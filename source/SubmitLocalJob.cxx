#include <EventLoop/DirectDriver.h>
#include <EventLoop/Job.h>
#include <HadronYieldsAnalysis/TreeMaker.h>
#include <HadronYieldsAnalysis/Useful.h>
#include <TSystem.h>
#include <SampleHandler/ScanDir.h>
#include <SampleHandler/ToolsDiscovery.h>



void InitializeLocalJob (const std::string submitDir, const std::string fileName, const std::string outFileName, const std::string dataType, const std::string collSys, const std::string triggerSet, const std::string saveObjects) 
{
  // Set up the job for xAOD access:
  xAOD::Init ().ignore ();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;
  
  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.
  const char* inputFilePath = gSystem->ExpandPathName ((std::getenv ("AOD_PATH") + std::string ("/") + fileName + "/").c_str ());
  SH::ScanDir ().filePattern ("*AOD*root*").scan (sh, inputFilePath);
   
  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  //job.options ()->setDouble (EL::Job::optMaxEvents, 500); // for testing purposes, limit to run over the first 500 events only!

  // define an output and an ntuple associated to that output
  EL::OutputStream output ("Tree");
  job.outputAdd (output);

  // add our algorithm to the job
  TreeMaker* alg = new TreeMaker ();


  if (collSys == ToString (CollisionSystem::PbPb18)) { 
    alg->m_collisionSystem = CollisionSystem::PbPb18;
  }
  else if (collSys == ToString (CollisionSystem::pp17)) {
    alg->m_collisionSystem = CollisionSystem::pp17;
  }
  else if (collSys == ToString (CollisionSystem::Pbp16)) {
    alg->m_collisionSystem = CollisionSystem::Pbp16;
  }
  else if (collSys == ToString (CollisionSystem::pPb16)) {
    alg->m_collisionSystem = CollisionSystem::pPb16;
  }
  else if (collSys == ToString (CollisionSystem::pPb16s5TeV)) {
    alg->m_collisionSystem = CollisionSystem::pPb16s5TeV;
  }
  else if (collSys == ToString (CollisionSystem::PbPb15)) {
    alg->m_collisionSystem = CollisionSystem::PbPb15;
  }
  else if (collSys == ToString (CollisionSystem::pp15)) {
    alg->m_collisionSystem = CollisionSystem::pp15;
  }
  else {
    std::cout << "Invalid collision system! Exiting." << std::endl;
    return;
  }
  std::cout << "Collision system = " << ToString (alg->m_collisionSystem) << std::endl;


  if (dataType == ToString (DataType::Collisions)) { 
    alg->m_dataType = DataType::Collisions;
  }
  else if (dataType == ToString (DataType::MCSignal)) {
    alg->m_dataType = DataType::MCSignal;
  }
  else if (dataType == ToString (DataType::MCDataOverlay)) { 
    alg->m_dataType = DataType::MCDataOverlay;
  }
  else if (dataType == ToString (DataType::MCHijing)) {
    alg->m_dataType = DataType::MCHijing;
  }
  else if (dataType == ToString (DataType::MCHijingOverlay)) {
    alg->m_dataType = DataType::MCHijingOverlay;
  }
  else {
    std::cout << "Invalid data type! Exiting" << std::endl;
    return;
  }
  std::cout << "Data type = " << ToString (alg->m_dataType) << std::flush;


  std::cout << "Trigger set = " << triggerSet << std::endl;
  if (triggerSet.find ("None") != std::string::npos) {
    alg->m_triggerSet.insert (EventTrigger::None);
    std::cout << "  --> not checking for triggers!" << std::endl;
  }
  if (triggerSet.find ("MinBias") != std::string::npos) {
    alg->m_triggerSet.insert (EventTrigger::MinBias);
    std::cout << "  --> checking for minimum bias triggers!" << std::endl;
  }
  if (triggerSet.find ("Photon") != std::string::npos) {
    alg->m_triggerSet.insert (EventTrigger::Photon);
    std::cout << "  --> checking for photon triggers!" << std::endl;
  }
  if (triggerSet.find ("Electron") != std::string::npos) {
    alg->m_triggerSet.insert (EventTrigger::Electron);
    std::cout << "  --> checking for electron triggers!" << std::endl;
  }
  if (triggerSet.find ("Muon") != std::string::npos) {
    alg->m_triggerSet.insert (EventTrigger::Muon);
    std::cout << "  --> checking for muon triggers!" << std::endl;
  }
  if (triggerSet.find ("Jet") != std::string::npos) {
    alg->m_triggerSet.insert (EventTrigger::Jet);
    std::cout << "  --> checking for jet triggers!" << std::endl;
  }
  if (triggerSet.find ("ZDC") != std::string::npos) {
    alg->m_triggerSet.insert (EventTrigger::ZDC);
    std::cout << "  --> checking for ZDC L1 triggers!" << std::endl;
  }


  std::cout << "Physics objects to save = " << saveObjects << std::endl;
  if (saveObjects.find ("None") != std::string::npos) {
    alg->m_saveObjects.insert (SaveObject::None);
    std::cout << "  --> saving... nothing? Okey dokey artichokey" << std::endl;
  }
  if (saveObjects.find ("Photons") != std::string::npos) {
    alg->m_saveObjects.insert (SaveObject::Photons);
    std::cout << "  --> saving photons!" << std::endl;
  }
  if (saveObjects.find ("Electrons") != std::string::npos) {
    alg->m_saveObjects.insert (SaveObject::Electrons);
    std::cout << "  --> saving electrons!" << std::endl;
  }
  if (saveObjects.find ("Clusters") != std::string::npos) {
    alg->m_saveObjects.insert (SaveObject::Clusters);
    std::cout << "  --> saving clusters!" << std::endl;
  }
  if (saveObjects.find ("Muons") != std::string::npos) {
    alg->m_saveObjects.insert (SaveObject::Muons);
    std::cout << "  --> saving muons!" << std::endl;
  }
  if (saveObjects.find ("Tracks") != std::string::npos) {
    alg->m_saveObjects.insert (SaveObject::Tracks);
    std::cout << "  --> saving tracks!" << std::endl;
  }
  if (saveObjects.find ("Jets") != std::string::npos) {
    alg->m_saveObjects.insert (SaveObject::Jets);
    std::cout << "  --> saving jets!" << std::endl;
  }


  alg->m_outputName = "Tree"; // give the name of the output to our algorithm

  // set the name of the algorithm (this is the name use with
  // messages)
  alg->SetName ("HadronYieldsAnalysis");

  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd (alg);
  job.options ()->setDouble (EL::Job::optRemoveSubmitDir, 1);

  // make the driver we want to use:
  EL::DirectDriver driver; // Driver to run a local job

  // process the job using the driver
  driver.submit (job, submitDir);

}



void replaceSubstring (std::string& str, const std::string substr, const std::string repl) {
  size_t index = 0;
  while (true) {
    index = str.find (substr, index);
    if (index == std::string::npos) break;
    str.replace (index, substr.length (), repl);
    index += repl.length ();
  }
  return;
}



void SubmitLocalJob (const std::string inFileName, const int submitNumber) {

  fstream inFile (inFileName.c_str ());

  std::string r_RELEASE, r_ALGORITHM, r_VERSION, r_TRIGGERSET, r_SAVEOBJECTS;

  std::getline (inFile, r_RELEASE);
  std::getline (inFile, r_ALGORITHM);
  std::getline (inFile, r_VERSION);
  std::getline (inFile, r_TRIGGERSET);
  std::getline (inFile, r_SAVEOBJECTS);

  std::cout << "RELEASE:       " << r_RELEASE      << std::endl;
  std::cout << "ALGORITHM:     " << r_ALGORITHM    << std::endl;
  std::cout << "VERSION:       " << r_VERSION      << std::endl;
  std::cout << "TRIGGERSET:    " << r_TRIGGERSET   << std::endl;
  std::cout << "SAVEOBJECTS:   " << r_SAVEOBJECTS  << std::endl;

  std::string input, submitDir, datasetName, outputName, dataType, collisionSystem, triggerSet, saveObjects;

  int iLine = 0; // goes to the correct line number
  while (inFile && iLine < submitNumber) {
    getline (inFile, input);
    iLine++;
  }

  getline (inFile, input);
  std::cout << "Reading input as: " << input << std::endl;
  std::stringstream ss (input);
  ss >> submitDir >> datasetName >> outputName >> dataType >> collisionSystem >> triggerSet >> saveObjects;

  // default to header values
  replaceSubstring (outputName, "RELEASE", r_RELEASE);
  replaceSubstring (outputName, "ALGORITHM", r_ALGORITHM);
  replaceSubstring (outputName, "VERSION", r_VERSION);
  std::cout << outputName << std::endl;
  triggerSet = r_TRIGGERSET;
  saveObjects = r_SAVEOBJECTS;

  std::cout << "Sending the following to InitializeLocalJob()..." << std::endl;
  std::cout << "  Submit dir:      "  << submitDir        << "(end)" << std::endl;
  std::cout << "  datasetName:     "  << datasetName      << "(end)" << std::endl;
  std::cout << "  outputName:      "  << outputName       << "(end)" << std::endl;
  std::cout << "  collisionSystem: "  << collisionSystem  << "(end)" << std::endl;
  std::cout << "  triggerSet:      "  << triggerSet       << "(end)" << std::endl;
  std::cout << "  saveObjects:     "  << saveObjects      << "(end)" << std::endl;

  InitializeLocalJob (submitDir, datasetName, outputName, dataType, collisionSystem, triggerSet, saveObjects);
}
