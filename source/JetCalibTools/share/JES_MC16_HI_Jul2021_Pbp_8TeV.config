#
#  Settings for Release 21 absolute EtaJES correction + relative and absolute insitu corrections + HI cross-calibration
#  Intended for use with run 2 Pb+p at 8.16 TeV reconstructed in R21
#    Beam config: proton going towards A-side (+eta) and ion going towards C-side (-eta)
#
#  Config file author: Jeff Ouellette, Jul. 2021
#
#####################

# --------------------------
# 1. Absolute JES

  # The file with the absolute JES factors
AbsoluteJES.CalibFile:	       JetCalibTools/CalibrationFactors/AntiKtHI_JES_constants_Pbp_8TeV_Jul21.config
AbsoluteJES.Description:       HI jet calibrations R = 0.4, 0.3, 0.2 for mc16 (2016 data at 8.16TeV)

EtaJESStartingScale: JetEMScaleMomentum

  # How low in ET do we extrapolate along the calibration curve ?
AntiKt4HI.MinPtForETAJES:  15
AntiKt2HI.MinPtForETAJES:  15

  # Which method should be used to extend the calibration curve past the minimum ET ?
  # 0 = calibation curve is frozen at minimum ET
  # 1 = slope of calibration curve is frozen at minimum ET (recommended)
  # 2 = order 2 polynomial is used to extend the calibration curve
LowPtJESExtrapolationMethod:   1
LowPtJESExtrapolationMinimumResponse: 0.25

######################

# 2. Time-Dependent Insitu Calibration
#
  TimeDependentInsituCalibration: true
  InsituTimeDependentConfigs: InsituConfig_1516_HI_Jan2021.config InsituConfig_17_HI_Jan2021.config  
  InsituRunBins: 251102 314199 999999

######################
