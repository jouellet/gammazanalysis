#include <HadronYieldsAnalysis/Useful.h>

#include <string>

#include <EventLoop/Algorithm.h>



std::string ToString (const CollisionSystem collSys) {
  switch (collSys) {
  case CollisionSystem::pp15:       return std::string ("pp15_5TeV");
  case CollisionSystem::PbPb15:     return std::string ("PbPb15_5TeV");
  case CollisionSystem::pPb16s5TeV: return std::string ("pPb16_5TeV");
  case CollisionSystem::pPb16:      return std::string ("pPb16_8TeV");
  case CollisionSystem::Pbp16:      return std::string ("Pbp16_8TeV");
  case CollisionSystem::pp17:       return std::string ("pp17_5TeV");
  case CollisionSystem::PbPb18:     return std::string ("PbPb18_5TeV");
  default:                          return std::string ("???");
  }
}



std::string ToString (const DataType dType) {
  switch (dType) {
  case DataType::Collisions:      return std::string ("Collisions");
  case DataType::MCSignal:        return std::string ("MCSignal");
  case DataType::MCDataOverlay:   return std::string ("MCDataOverlay");
  case DataType::MCHijing:        return std::string ("MCHijing");
  case DataType::MCHijingOverlay: return std::string ("MCHijingOverlay");
  default:                        return std::string ("???");
  }
}



std::string ToString (const EventTrigger evtTrig) {
  switch (evtTrig) {
  case EventTrigger::None:      return std::string ("None");
  case EventTrigger::MinBias:   return std::string ("MinBias");
  case EventTrigger::Photon:    return std::string ("Photon");
  case EventTrigger::Electron:  return std::string ("Electron");
  case EventTrigger::Muon:      return std::string ("Muon");
  case EventTrigger::Jet:       return std::string ("Jet");
  case EventTrigger::ZDC:       return std::string ("ZDC");
  default:                      return std::string ("???");
  }
}



bool IsIons (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::PbPb15 || collSys == CollisionSystem::pPb16s5TeV || collSys == CollisionSystem::pPb16 || collSys == CollisionSystem::Pbp16 || collSys == CollisionSystem::PbPb18);
}



bool IsPbPb (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::PbPb15 || collSys == CollisionSystem::PbPb18);
}



bool IsPbPb18 (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::PbPb18);
}



bool IsPbPb15 (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::PbPb15);
}



bool IsXeXe (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::XeXe17);
}



bool IspPb (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::pPb16s5TeV || collSys == CollisionSystem::pPb16 || collSys == CollisionSystem::Pbp16);
}



bool IspPb16 (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::pPb16s5TeV || collSys == CollisionSystem::pPb16 || collSys == CollisionSystem::Pbp16);
}



bool Ispp (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::pp15 || collSys == CollisionSystem::pp17);
}



bool Ispp15 (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::pp15);
}



bool Ispp17 (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::pp17);
}



bool IsPeriodA (const CollisionSystem collSys) {
  if (collSys == CollisionSystem::pPb16s5TeV || collSys == CollisionSystem::pPb16) {
    return true;
  }
  else if (collSys == CollisionSystem::Pbp16) {
    return false;
  }
  Info ("IsPeriodA (const CollisionSystem)", "Warning: collision system is symmetric, returning true by default.");
  return true;
}



bool Is5TeV (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::pp15 || collSys == CollisionSystem::PbPb15 || collSys == CollisionSystem::pPb16s5TeV || collSys == CollisionSystem::XeXe17 || collSys == CollisionSystem::pp17 || collSys == CollisionSystem::PbPb18);
}



bool Is8TeV (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::pPb16 || collSys == CollisionSystem::Pbp16);
}



bool Is2018 (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::PbPb18);
}



bool Is2017 (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::XeXe17 || collSys == CollisionSystem::pp17);
}



bool Is2016 (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::pPb16s5TeV || collSys == CollisionSystem::pPb16 || collSys == CollisionSystem::Pbp16);
}



bool Is2015 (const CollisionSystem collSys) {
  return (collSys == CollisionSystem::pp15 || collSys == CollisionSystem::PbPb15);
}



bool IsCollisions (const DataType dType) {
  return (dType == DataType::Collisions);
}



bool IsDataOverlay (const DataType dType) {
  return (dType == DataType::MCDataOverlay);
}


bool IsHijing (const DataType dType) {
  return (dType == DataType::MCHijing || dType == DataType::MCHijingOverlay);
}
