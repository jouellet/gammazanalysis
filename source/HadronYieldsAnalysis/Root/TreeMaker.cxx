#include <HadronYieldsAnalysis/TreeMaker.h>
#include <HadronYieldsAnalysis/Useful.h>

// EventLoop includes
#include <AsgTools/MessageCheck.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

// root includes
#include <TFile.h>
#include <TSystem.h>
#include <TLorentzVector.h>

// xAOD includes
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/Init.h"
#include "xAODTrigger/EmTauRoIContainer.h"

// ZDC includes
#include "xAODForward/ZdcModuleContainer.h"

// Tracking & vertex includes
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

// Truth includes
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/xAODTruthHelpers.h"

// EGamma includes
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/EgammaTruthxAODHelpers.h"
#include "xAODEgamma/ElectronxAODHelpers.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloCluster.h"

// Muon includes
#include <xAODMuon/MuonContainer.h>
#include <xAODMuon/Muon.h>

// Jet includes
#include "xAODJet/JetContainer.h"
#include "xAODJet/Jet.h"

// Path resolver tool
#include <PathResolver/PathResolver.h>


// this is needed to distribute the algorithm to the workers
ClassImp(TreeMaker)

typedef std::vector <ElementLink <xAOD::TrackParticleContainer>> TPELVec_t;


TreeMaker :: TreeMaker () {
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  m_goodRunsList = nullptr;
  m_goodRunsList_ignoreToroid = nullptr;

  m_trigDecisionTool = nullptr;
  m_trigConfigTool = nullptr;
  m_trigMatchingTool = nullptr;

  m_zdcAnalysisTool = nullptr;

  m_egammaCalibrationAndSmearingTool = nullptr;

  m_photonLooseIsEMSelector = nullptr;
  m_photonMediumIsEMSelector = nullptr;
  m_photonTightIsEMSelector = nullptr;

  m_electronLHTightSelectorTool = nullptr;
  m_electronLHMediumSelectorTool = nullptr;
  m_electronLHLooseSelectorTool = nullptr;
  m_electronLHMediumHISelectorTool = nullptr;
  m_electronLHLooseHISelectorTool = nullptr;

  m_muonCalibrationAndSmearingTool = nullptr;

  m_muonTightSelectorTool = nullptr;
  m_muonMediumSelectorTool = nullptr;
  m_muonLooseSelectorTool = nullptr;

  m_trackSelectionToolLoose = nullptr;
  m_trackSelectionToolLoosePrimary = nullptr;
  m_trackSelectionToolTightPrimary = nullptr;
  m_trackSelectionToolHITight = nullptr;
  m_trackSelectionToolHILoose = nullptr;

  m_mcTruthClassifier = nullptr;

  m_jetCleaningTool = nullptr;

  m_Akt2HI_EtaJES_CalibTool = nullptr;
  m_Akt2HI_EtaJES_Insitu_CalibTool = nullptr;

  m_Akt4HI_EtaJES_CalibTool = nullptr;
  m_Akt4HI_EtaJES_Insitu_CalibTool = nullptr;

  m_Akt2HI_UncertTool = nullptr;
  m_Akt4HI_UncertTool = nullptr;

  m_h204_JER = nullptr;
  m_h2CC_JES = nullptr;
  m_h2CC_JER = nullptr;
  m_rnd = nullptr;
  
  m_f_res_04 = nullptr;
  m_f_fra_04 = nullptr;
  m_f_res_02 = nullptr;
  m_f_fra_02 = nullptr;

  m_pt_mapping = nullptr;
  m_RS_JES = nullptr;
  m_RS_JER = nullptr;

}



EL::StatusCode TreeMaker :: setupJob (EL::Job& job) {
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  job.useXAOD (); 
 
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: histInitialize () {
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  TFile *outputFile = wk ()->getOutputFile (m_outputName);
  
  m_tree = new TTree ("bush", "bush");
  m_tree->SetDirectory (outputFile);


  // Event info 
  if (m_debug >= 1) {
    Info ("histInitialize ()", "Creating branches for event-level information...");
  }
  m_tree->Branch ("run_number",     &m_b_runNum,  "run_number/i");
  m_tree->Branch ("event_number",   &m_b_evtNum,  "event_number/i");
  m_tree->Branch ("lumi_block",     &m_b_lbn,     "lumi_block/i"); 
  m_tree->Branch ("passes_toroid",  &m_b_passesToroid,  "passes_toroid/O");
  m_tree->Branch ("isOOTPU",        &m_b_isOOTPU,       "isOOTPU/O");
  m_tree->Branch ("BlayerDesyn",    &m_b_BlayerDesyn,   "BlayerDesyn/O");
  if (m_debug >= 1) {
    Info ("histInitialize ()", "Event-level branches ready.");
  }


  // Collision info
  if (m_debug >= 1) {
    Info ("histInitialize ()", "Creating branches for bunch crossing information...");
  }
  m_tree->Branch ("actualInteractionsPerCrossing",  &m_b_actualInteractionsPerCrossing,   "actualInteractionsPerCrossing/F");
  m_tree->Branch ("averageInteractionsPerCrossing", &m_b_averageInteractionsPerCrossing,  "averageInteractionsPerCrossing/F");
  if (m_debug >= 1) {
    Info ("histInitialize ()", "Bunch crossing branches ready.");
  }


  // Truth event info
  if (!IsCollisions (m_dataType)) {
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Creating branches for truth event-level information...");
    }
    m_tree->Branch ("mcEventWeights",     &m_b_mcEventWeights);
    m_tree->Branch ("truth_event_n",      &m_b_truth_event_n,       "truth_event_n/I");
    m_tree->Branch ("nPart1",             &m_b_nPart1,              "nPart1[truth_event_n]/I");
    m_tree->Branch ("nPart2",             &m_b_nPart2,              "nPart2[truth_event_n]/I");
    m_tree->Branch ("impactParameter",    &m_b_impactParameter,     "impactParameter[truth_event_n]/F");
    m_tree->Branch ("nColl",              &m_b_nColl,               "nColl[truth_event_n]/I");
    m_tree->Branch ("nSpectatorNeutrons", &m_b_nSpectatorNeutrons,  "nSpectatorNeutrons[truth_event_n]/I");
    m_tree->Branch ("nSpectatorProtons",  &m_b_nSpectatorProtons,   "nSpectatorProtons[truth_event_n]/I");
    m_tree->Branch ("eccentricity",       &m_b_eccentricity,        "eccentricity[truth_event_n]/F");
    m_tree->Branch ("eventPlaneAngle",    &m_b_eventPlaneAngle,     "eventPlaneAngle[truth_event_n]/F");
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Truth event-level branches ready.");
    }
  }


  // Event trigger info
  if (m_triggerSet.size () == 0)
    m_triggerSet.insert (EventTrigger::None);
  
  if (m_triggerSet.count (EventTrigger::None) == 0) {
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Setting up trigger sets and branches...");
    }

    if (IsPbPb18 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> using PbPb18 trigger set.");
      }
      m_minbias_trig_n = m_minbias_trig_n_PbPb18;
      m_minbias_trig_name = m_minbias_trig_name_PbPb18;
      m_photon_trig_n = m_photon_trig_n_PbPb18;
      m_photon_trig_name = m_photon_trig_name_PbPb18;
      m_electron_trig_n = m_electron_trig_n_PbPb18;
      m_electron_trig_name = m_electron_trig_name_PbPb18;
      m_muon_trig_n = m_muon_trig_n_PbPb18;
      m_muon_trig_name = m_muon_trig_name_PbPb18;
      m_jet_trig_n = m_jet_trig_n_PbPb18;
      m_jet_trig_name = m_jet_trig_name_PbPb18;
    }
    else if (Ispp17 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> using pp17 trigger set.");
      }
      m_minbias_trig_n = m_minbias_trig_n_pp17;
      m_minbias_trig_name = m_minbias_trig_name_pp17;
      m_photon_trig_n = m_photon_trig_n_pp17;
      m_photon_trig_name = m_photon_trig_name_pp17;
      m_electron_trig_n = m_electron_trig_n_pp17;
      m_electron_trig_name = m_electron_trig_name_pp17;
      m_muon_trig_n = m_muon_trig_n_pp17;
      m_muon_trig_name = m_muon_trig_name_pp17;
      m_jet_trig_n = m_jet_trig_n_pp17;
      m_jet_trig_name = m_jet_trig_name_pp17;
    }
    else if (IspPb16 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> using pPb16 trigger set.");
      }
      m_minbias_trig_n = m_minbias_trig_n_pPb16;
      m_minbias_trig_name = m_minbias_trig_name_pPb16;
      m_photon_trig_n = m_photon_trig_n_pPb16;
      m_photon_trig_name = m_photon_trig_name_pPb16;
      m_electron_trig_n = m_electron_trig_n_pPb16;
      m_electron_trig_name = m_electron_trig_name_pPb16;
      m_muon_trig_n = m_muon_trig_n_pPb16;
      m_muon_trig_name = m_muon_trig_name_pPb16;

      if (Is8TeV (m_collisionSystem)) {
        if (m_debug >= 2) {
          Info ("histInitialize ()", "  --> with jet triggers for 8.16 TeV data");
        }
        if (IsPeriodA (m_collisionSystem)) {
          if (m_debug >= 2) {
            Info ("histInitialize ()", "  --> with period A collisions");
          }
          m_jet_trig_n = m_jet_trig_n_pPb16;
          m_jet_trig_name = m_jet_trig_name_pPb16;
        }
        else {
          if (m_debug >= 2) {
            Info ("histInitialize ()", "  --> with period B collisions");
          }
          m_jet_trig_n = m_jet_trig_n_Pbp16;
          m_jet_trig_name = m_jet_trig_name_Pbp16;
        }
      }
      else if (Is5TeV (m_collisionSystem)) {
        if (m_debug >= 2) {
          Info ("histInitialize ()", "  --> with jet triggers for 5.02 TeV data");
        }
        m_jet_trig_n = m_jet_trig_n_pPb16s5TeV;
        m_jet_trig_name = m_jet_trig_name_pPb16s5TeV;
      }
    }
    else if (IsPbPb15 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> using PbPb15 trigger set.");
      }
      m_minbias_trig_n = m_minbias_trig_n_PbPb15;
      m_minbias_trig_name = m_minbias_trig_name_PbPb15;
      m_photon_trig_n = m_photon_trig_n_PbPb15;
      m_photon_trig_name = m_photon_trig_name_PbPb15;
      m_electron_trig_n = m_electron_trig_n_PbPb15;
      m_electron_trig_name = m_electron_trig_name_PbPb15;
      m_muon_trig_n = m_muon_trig_n_PbPb15;
      m_muon_trig_name = m_muon_trig_name_PbPb15;
      m_jet_trig_n = m_jet_trig_n_PbPb15;
      m_jet_trig_name = m_jet_trig_name_PbPb15;
    }
    else if (Ispp15 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> using pp15 trigger set.");
      }
      m_minbias_trig_n = m_minbias_trig_n_pp15;
      m_minbias_trig_name = m_minbias_trig_name_pp15;
      m_photon_trig_n = m_photon_trig_n_pp15;
      m_photon_trig_name = m_photon_trig_name_pp15;
      m_electron_trig_n = m_electron_trig_n_pp15;
      m_electron_trig_name = m_electron_trig_name_pp15;
      m_muon_trig_n = m_muon_trig_n_pp15;
      m_muon_trig_name = m_muon_trig_name_pp15;
      m_jet_trig_n = m_jet_trig_n_pp15;
      m_jet_trig_name = m_jet_trig_name_pp15;
    }
    else {
      Error ("BranchTriggers ()", "Triggers not defined for collision system, exiting.");
      return EL::StatusCode::FAILURE;
    }

    if (m_triggerSet.count (EventTrigger::MinBias) > 0) {
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> adding minimum bias triggers...");
      }
      m_b_minbias_trig_decision = new bool [m_minbias_trig_n];
      m_b_minbias_trig_prescale = new float [m_minbias_trig_n];
      for (int i = 0; i < m_minbias_trig_n; i++) {
        if (m_debug >= 3) {
          Info ("histInitialize ()", Form ("  --> adding %s...", m_minbias_trig_name[i].c_str ()));
        }
        m_tree->Branch (Form ("%s_decision", m_minbias_trig_name[i].c_str ()), &(m_b_minbias_trig_decision[i]), Form ("%s_decision/O", m_minbias_trig_name[i].c_str ()));
        m_tree->Branch (Form ("%s_prescale", m_minbias_trig_name[i].c_str ()), &(m_b_minbias_trig_prescale[i]), Form ("%s_prescale/F", m_minbias_trig_name[i].c_str ()));
      }
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> minimum bias triggers ready.");
      }
    }
    if (m_triggerSet.count (EventTrigger::Photon) > 0) {
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> adding photon triggers...");
      }
      m_b_photon_trig_decision = new bool [m_photon_trig_n];
      m_b_photon_trig_prescale = new float [m_photon_trig_n];
      for (int i = 0; i < m_photon_trig_n; i++) {
        if (m_debug >= 3) {
          Info ("histInitialize ()", Form ("  --> adding %s...", m_photon_trig_name[i].c_str ()));
        }
        m_tree->Branch (Form ("%s_decision", m_photon_trig_name[i].c_str ()), &(m_b_photon_trig_decision[i]), Form ("%s_decision/O", m_photon_trig_name[i].c_str ()));
        m_tree->Branch (Form ("%s_prescale", m_photon_trig_name[i].c_str ()), &(m_b_photon_trig_prescale[i]), Form ("%s_prescale/F", m_photon_trig_name[i].c_str ()));
      }
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> photon triggers ready.");
      }
    }
    if (m_triggerSet.count (EventTrigger::Electron) > 0) {
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> adding electron triggers...");
      }
      m_b_electron_trig_decision = new bool [m_electron_trig_n];
      m_b_electron_trig_prescale = new float [m_electron_trig_n];
      for (int i = 0; i < m_electron_trig_n; i++) {
        if (m_debug >= 3) {
          Info ("histInitialize ()", Form ("  --> adding %s...", m_electron_trig_name[i].c_str ()));
        }
        m_tree->Branch (Form ("%s_decision", m_electron_trig_name[i].c_str ()), &(m_b_electron_trig_decision[i]), Form ("%s_decision/O", m_electron_trig_name[i].c_str ()));
        m_tree->Branch (Form ("%s_prescale", m_electron_trig_name[i].c_str ()), &(m_b_electron_trig_prescale[i]), Form ("%s_prescale/F", m_electron_trig_name[i].c_str ()));
      }
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> electron triggers ready.");
      }
    }
    if (m_triggerSet.count (EventTrigger::Muon) > 0) {
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> adding muon triggers...");
      }
      m_b_muon_trig_decision = new bool [m_muon_trig_n];
      m_b_muon_trig_prescale = new float [m_muon_trig_n];
      for (int i = 0; i < m_muon_trig_n; i++) {
        if (m_debug >= 3) {
          Info ("histInitialize ()", Form ("  --> adding %s...", m_muon_trig_name[i].c_str ()));
        }
        m_tree->Branch (Form ("%s_decision", m_muon_trig_name[i].c_str ()), &(m_b_muon_trig_decision[i]), Form ("%s_decision/O", m_muon_trig_name[i].c_str ()));
        m_tree->Branch (Form ("%s_prescale", m_muon_trig_name[i].c_str ()), &(m_b_muon_trig_prescale[i]), Form ("%s_prescale/F", m_muon_trig_name[i].c_str ()));
      }
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> muon triggers ready.");
      }
    }
    if (m_triggerSet.count (EventTrigger::Jet) > 0) {
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> adding jet triggers...");
      }
      m_b_jet_trig_decision = new bool [m_jet_trig_n];
      m_b_jet_trig_prescale = new float [m_jet_trig_n];
      for (int i = 0; i < m_jet_trig_n; i++) {
        if (m_debug >= 3) {
          Info ("histInitialize ()", Form ("  --> adding %s...", m_jet_trig_name[i].c_str ()));
        }
        m_tree->Branch (Form ("%s_decision", m_jet_trig_name[i].c_str ()), &(m_b_jet_trig_decision[i]), Form ("%s_decision/O", m_jet_trig_name[i].c_str ()));
        m_tree->Branch (Form ("%s_prescale", m_jet_trig_name[i].c_str ()), &(m_b_jet_trig_prescale[i]), Form ("%s_prescale/F", m_jet_trig_name[i].c_str ()));
      }
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> jet triggers ready.");
      }
    }
    if (m_triggerSet.count (EventTrigger::ZDC) > 0) {
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> adding ZDC L1 triggers...");
      }
      m_b_zdc_L1_trig_decision = new bool [m_zdc_L1_trig_n];
      m_b_zdc_L1_trig_tbp = new bool [m_zdc_L1_trig_n];
      m_b_zdc_L1_trig_tap = new bool [m_zdc_L1_trig_n];
      m_b_zdc_L1_trig_tav = new bool [m_zdc_L1_trig_n];
      m_b_zdc_L1_trig_prescale = new float [m_zdc_L1_trig_n];
      for (int i = 0; i < m_zdc_L1_trig_n; i++) {
        if (m_debug >= 3) {
          Info ("histInitialize ()", Form ("  --> adding %s...", m_zdc_L1_trig_name[i].c_str ()));
        }
        m_tree->Branch (Form ("%s_decision", m_zdc_L1_trig_name[i].c_str ()), &(m_b_zdc_L1_trig_decision[i]), Form ("%s_decision/O", m_zdc_L1_trig_name[i].c_str ()));
        m_tree->Branch (Form ("%s_tbp", m_zdc_L1_trig_name[i].c_str ()), &(m_b_zdc_L1_trig_tbp[i]), Form ("%s_tbp/O", m_zdc_L1_trig_name[i].c_str ()));
        m_tree->Branch (Form ("%s_tap", m_zdc_L1_trig_name[i].c_str ()), &(m_b_zdc_L1_trig_tap[i]), Form ("%s_tap/O", m_zdc_L1_trig_name[i].c_str ()));
        m_tree->Branch (Form ("%s_tav", m_zdc_L1_trig_name[i].c_str ()), &(m_b_zdc_L1_trig_tav[i]), Form ("%s_tav/O", m_zdc_L1_trig_name[i].c_str ()));
        m_tree->Branch (Form ("%s_prescale", m_zdc_L1_trig_name[i].c_str ()), &(m_b_zdc_L1_trig_prescale[i]), Form ("%s_prescale/F", m_zdc_L1_trig_name[i].c_str ()));
      }
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> ZDC L1 triggers ready.");
      }
    }
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Triggers set and branches ready.");
    }
  }


  // vertices
  if (m_debug >= 1) {
    Info ("histInitialize ()", "Creating branches for reconstructed vertex information...");
  }
  m_tree->Branch ("nvert",        &m_b_nvert,       "nvert/I");
  m_tree->Branch ("vert_x",       &m_b_vert_x,      "vert_x[nvert]/F");
  m_tree->Branch ("vert_y",       &m_b_vert_y,      "vert_y[nvert]/F");
  m_tree->Branch ("vert_z",       &m_b_vert_z,      "vert_z[nvert]/F");
  m_tree->Branch ("vert_ntrk",    &m_b_vert_ntrk,   "vert_ntrk[nvert]/I");
  m_tree->Branch ("vert_type",    &m_b_vert_type,   "vert_type[nvert]/I");
  m_tree->Branch ("vert_sumpt2",  &m_b_vert_sumpt2, "vert_sumpt2[nvert]/F");
  if (m_debug >= 1) {
    Info ("histInitialize ()", "Reconstructed vertex branches ready.");
  }


  if (!IsCollisions (m_dataType)) {
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Creating branches for truth vertex information...");
    }
    m_tree->Branch ("truth_vert_n",       &m_b_truth_vert_n,      "truth_vert_n/I");
    m_tree->Branch ("truth_vert_x",       &m_b_truth_vert_x,      "truth_vert_x[truth_vert_n]/F");
    m_tree->Branch ("truth_vert_y",       &m_b_truth_vert_y,      "truth_vert_y[truth_vert_n]/F");
    m_tree->Branch ("truth_vert_z",       &m_b_truth_vert_z,      "truth_vert_z[truth_vert_n]/F");
    m_tree->Branch ("truth_vert_ntrk",    &m_b_truth_vert_ntrk,   "truth_vert_ntrk[truth_vert_n]/I");
    m_tree->Branch ("truth_vert_sumpt2",  &m_b_truth_vert_sumpt2, "truth_vert_sumpt2[truth_vert_n]/F");
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Truth vertex branches ready.");
    }
  }


  // forward calorimeters
  if (m_debug >= 1) {
    Info ("histInitialize ()", "Creating branches for forward hadronic calorimeter energy sum information...");
  }
  m_tree->Branch ("fcalA_et",       &m_b_fcalA_et,      "fcalA_et/F");
  m_tree->Branch ("fcalC_et",       &m_b_fcalC_et,      "fcalC_et/F");
  m_tree->Branch ("fcalA_et_Cos2",  &m_b_fcalA_et_Cos2, "fcalA_et_Cos2/F");
  m_tree->Branch ("fcalC_et_Cos2",  &m_b_fcalC_et_Cos2, "fcalC_et_Cos2/F");
  m_tree->Branch ("fcalA_et_Sin2",  &m_b_fcalA_et_Sin2, "fcalA_et_Sin2/F");
  m_tree->Branch ("fcalC_et_Sin2",  &m_b_fcalC_et_Sin2, "fcalC_et_Sin2/F");
  m_tree->Branch ("fcalA_et_Cos3",  &m_b_fcalA_et_Cos3, "fcalA_et_Cos3/F");
  m_tree->Branch ("fcalC_et_Cos3",  &m_b_fcalC_et_Cos3, "fcalC_et_Cos3/F");
  m_tree->Branch ("fcalA_et_Sin3",  &m_b_fcalA_et_Sin3, "fcalA_et_Sin3/F");
  m_tree->Branch ("fcalC_et_Sin3",  &m_b_fcalC_et_Sin3, "fcalC_et_Sin3/F");
  m_tree->Branch ("fcalA_et_Cos4",  &m_b_fcalA_et_Cos4, "fcalA_et_Cos4/F");
  m_tree->Branch ("fcalC_et_Cos4",  &m_b_fcalC_et_Cos4, "fcalC_et_Cos4/F");
  m_tree->Branch ("fcalA_et_Sin4",  &m_b_fcalA_et_Sin4, "fcalA_et_Sin4/F");
  m_tree->Branch ("fcalC_et_Sin4",  &m_b_fcalC_et_Sin4, "fcalC_et_Sin4/F");
  if (m_debug >= 1) {
    Info ("histInitialize ()", "Forward hadronic calorimeter energy sum branches ready.");
  }


  // ZDC energies
  if (IsIons (m_collisionSystem) && (IsCollisions (m_dataType))){// || IsDataOverlay (m_dataType))) {
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Creating branches for ZDC energies...");
    }
    m_tree->Branch ("ZdcRawEnergy_A",     &m_b_ZdcRawEnergy_A,    "ZdcRawEnergy_A/F");
    m_tree->Branch ("ZdcRawEnergy_C",     &m_b_ZdcRawEnergy_C,    "ZdcRawEnergy_C/F");
    m_tree->Branch ("ZdcCalibEnergy_A",   &m_b_ZdcCalibEnergy_A,  "ZdcCalibEnergy_A/F");
    m_tree->Branch ("ZdcCalibEnergy_C",   &m_b_ZdcCalibEnergy_C,  "ZdcCalibEnergy_C/F");
    if (m_debug >= 1) {
      Info ("histInitialize ()", "ZDC energy branches ready.");
    }
  }


  // Sum of gaps and edge gaps
  {
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Creating branches for gap information...");
    }
    m_tree->Branch ("cluster_sumGap_A",  &m_b_cluster_sumGap_A,   "cluster_sumGap_A/F");
    m_tree->Branch ("cluster_sumGap_C",  &m_b_cluster_sumGap_C,   "cluster_sumGap_C/F");
    m_tree->Branch ("cluster_edgeGap_A", &m_b_cluster_edgeGap_A,  "cluster_edgeGap_A/F");
    m_tree->Branch ("cluster_edgeGap_C", &m_b_cluster_edgeGap_C,  "cluster_edgeGap_C/F");
    m_tree->Branch ("sumGap_A",          &m_b_sumGap_A,           "sumGap_A/F");
    m_tree->Branch ("sumGap_C",          &m_b_sumGap_C,           "sumGap_C/F");
    m_tree->Branch ("edgeGap_A",         &m_b_edgeGap_A,          "edgeGap_A/F");
    m_tree->Branch ("edgeGap_C",         &m_b_edgeGap_C,          "edgeGap_C/F");
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Gap branches ready.");
    }
  }


  // Save photon info
  if (m_saveObjects.count (SaveObject::Photons) > 0) {
    // Truth photon info
    if (!IsCollisions (m_dataType)) {
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Creating branches for truth photons...");
      }
      m_tree->Branch ("truth_photon_n",       &m_b_truth_photon_n,        "truth_photon_n/I");
      m_tree->Branch ("truth_photon_pt",      &m_b_truth_photon_pt,       "truth_photon_pt[truth_photon_n]/F");
      m_tree->Branch ("truth_photon_eta",     &m_b_truth_photon_eta,      "truth_photon_eta[truth_photon_n]/F");
      m_tree->Branch ("truth_photon_phi",     &m_b_truth_photon_phi,      "truth_photon_phi[truth_photon_n]/F");
      m_tree->Branch ("truth_photon_barcode", &m_b_truth_photon_barcode,  "truth_photon_barcode[truth_photon_n]/F");
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Truth photon branches ready.");
      }
    }

    // Reco photon info
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Creating branches for truth photons...");
    }
    m_tree->Branch ("photon_n",             &m_b_photon_n,            "photon_n/I");
    m_tree->Branch ("photon_pt_precalib",   &m_b_photon_pt_precalib,  "photon_pt_precalib[photon_n]/F");
    m_tree->Branch ("photon_pt",            &m_b_photon_pt,           "photon_pt[photon_n]/F");
    m_tree->Branch ("photon_eta",           &m_b_photon_eta,          "photon_eta[photon_n]/F");
    m_tree->Branch ("photon_etaBE",         &m_b_photon_etaBE,        "photon_etaBE[photon_n]/F");
    m_tree->Branch ("photon_phi",           &m_b_photon_phi,          "photon_phi[photon_n]/F");
    if (m_triggerSet.count (EventTrigger::Photon) > 0) {
      m_tree->Branch ("photon_matched",     &m_b_photon_matched,      Form ("photon_matched[%i][photon_n]/O", m_photon_trig_n));
    }
    m_tree->Branch ("photon_tight",         &m_b_photon_tight,        "photon_tight[photon_n]/O");
    m_tree->Branch ("photon_medium",        &m_b_photon_medium,       "photon_medium[photon_n]/O");
    m_tree->Branch ("photon_loose",         &m_b_photon_loose,        "photon_loose[photon_n]/O");
    m_tree->Branch ("photon_isem",          &m_b_photon_isem,         "photon_isem[photon_n]/i");
    m_tree->Branch ("photon_convFlag",      &m_b_photon_convFlag,     "photon_convFlag[photon_n]/I");
    m_tree->Branch ("photon_Rconv",         &m_b_photon_Rconv,        "photon_Rconv[photon_n]/F");
    m_tree->Branch ("photon_etcone20",      &m_b_photon_etcone20,     "photon_etcone20[photon_n]/F");
    m_tree->Branch ("photon_etcone30",      &m_b_photon_etcone30,     "photon_etcone30[photon_n]/F");
    m_tree->Branch ("photon_etcone40",      &m_b_photon_etcone40,     "photon_etcone40[photon_n]/F");
    m_tree->Branch ("photon_topoetcone20",  &m_b_photon_topoetcone20, "photon_topoetcone20[photon_n]/F");
    m_tree->Branch ("photon_topoetcone30",  &m_b_photon_topoetcone30, "photon_topoetcone30[photon_n]/F");
    m_tree->Branch ("photon_topoetcone40",  &m_b_photon_topoetcone40, "photon_topoetcone40[photon_n]/F");
    m_tree->Branch ("photon_Rhad1",         &m_b_photon_Rhad1,        "photon_Rhad1[photon_n]/F");
    m_tree->Branch ("photon_Rhad",          &m_b_photon_Rhad,         "photon_Rhad[photon_n]/F");
    m_tree->Branch ("photon_e277",          &m_b_photon_e277,         "photon_e277[photon_n]/F");
    m_tree->Branch ("photon_Reta",          &m_b_photon_Reta,         "photon_Reta[photon_n]/F");
    m_tree->Branch ("photon_Rphi",          &m_b_photon_Rphi,         "photon_Rphi[photon_n]/F");
    m_tree->Branch ("photon_weta1",         &m_b_photon_weta1,        "photon_weta1[photon_n]/F");
    m_tree->Branch ("photon_weta2",         &m_b_photon_weta2,        "photon_weta2[photon_n]/F");
    m_tree->Branch ("photon_wtots1",        &m_b_photon_wtots1,       "photon_wtots1[photon_n]/F");
    m_tree->Branch ("photon_f1",            &m_b_photon_f1,           "photon_f1[photon_n]/F");
    m_tree->Branch ("photon_f3",            &m_b_photon_f3,           "photon_f3[photon_n]/F");
    m_tree->Branch ("photon_fracs1",        &m_b_photon_fracs1,       "photon_fracs1[photon_n]/F");
    m_tree->Branch ("photon_DeltaE",        &m_b_photon_DeltaE,       "photon_DeltaE[photon_n]/F");
    m_tree->Branch ("photon_Eratio",        &m_b_photon_Eratio,       "photon_Eratio[photon_n]/F");
    m_tree->Branch ("photon_pt_sys",        &m_b_photon_pt_sys,       "photon_pt_sys[photon_n]/F");
    m_tree->Branch ("photon_eta_sys",       &m_b_photon_eta_sys,      "photon_eta_sys[photon_n]/F");
    m_tree->Branch ("photon_phi_sys",       &m_b_photon_phi_sys,      "photon_phi_sys[photon_n]/F");
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Reconstructed photon branches ready.");
    }
  }


  // Save electron info
  if (m_saveObjects.count (SaveObject::Electrons) > 0) {
    // Truth electrons info
    if (!IsCollisions (m_dataType)) {
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Creating branches for truth electrons...");
      }
      m_tree->Branch ("truth_electron_n",       &m_b_truth_electron_n,        "truth_electron_n/I");
      m_tree->Branch ("truth_electron_pt",      &m_b_truth_electron_pt,       "truth_electron_pt[truth_electron_n]/F");
      m_tree->Branch ("truth_electron_eta",     &m_b_truth_electron_eta,      "truth_electron_eta[truth_electron_n]/F");
      m_tree->Branch ("truth_electron_phi",     &m_b_truth_electron_phi,      "truth_electron_phi[truth_electron_n]/F");
      m_tree->Branch ("truth_electron_charge",  &m_b_truth_electron_charge,   "truth_electron_charge[truth_electron_n]/I");
      m_tree->Branch ("truth_electron_barcode", &m_b_truth_electron_barcode,  "truth_electron_barcode[truth_electron_n]/I");
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Truth electron branches ready.");
      }
    }

    // Reco electrons info
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Creating branches for reconstructed electrons...");
    }
    m_tree->Branch ("electron_n",                     &m_b_electron_n,                      "electron_n/I");
    m_tree->Branch ("electron_pt_precalib",           &m_b_electron_pt_precalib,            "electron_pt_precalib[electron_n]/F");
    m_tree->Branch ("electron_pt",                    &m_b_electron_pt,                     "electron_pt[electron_n]/F");
    m_tree->Branch ("electron_eta",                   &m_b_electron_eta,                    "electron_eta[electron_n]/F");
    m_tree->Branch ("electron_etaBE",                 &m_b_electron_etaBE,                  "electron_etaBE[electron_n]/F");
    m_tree->Branch ("electron_phi",                   &m_b_electron_phi,                    "electron_phi[electron_n]/F");
    m_tree->Branch ("electron_charge",                &m_b_electron_charge,                 "electron_charge[electron_n]/I");
    m_tree->Branch ("electron_lhtight",               &m_b_electron_lhtight,                "electron_lhtight[electron_n]/O");
    m_tree->Branch ("electron_lhmedium",              &m_b_electron_lhmedium,               "electron_lhmedium[electron_n]/O");
    m_tree->Branch ("electron_lhloose",               &m_b_electron_lhloose,                "electron_lhloose[electron_n]/O");
    m_tree->Branch ("electron_lhmedium_hi",           &m_b_electron_lhmedium_hi,            "electron_lhmedium_hi[electron_n]/O");
    m_tree->Branch ("electron_lhloose_hi",            &m_b_electron_lhloose_hi,             "electron_lhloose_hi[electron_n]/O");
    if (m_triggerSet.count (EventTrigger::Electron) > 0) {
      m_tree->Branch ("electron_matched",             &m_b_electron_matched,                Form ("electron_matched[%i][electron_n]/O", m_electron_trig_n));
    }
    m_tree->Branch ("electron_etcone20",              &m_b_electron_etcone20,               "electron_etcone20[electron_n]/F");
    m_tree->Branch ("electron_etcone30",              &m_b_electron_etcone30,               "electron_etcone30[electron_n]/F");
    m_tree->Branch ("electron_etcone40",              &m_b_electron_etcone40,               "electron_etcone40[electron_n]/F");
    m_tree->Branch ("electron_topoetcone20",          &m_b_electron_topoetcone20,           "electron_topoetcone20[electron_n]/F");
    m_tree->Branch ("electron_topoetcone30",          &m_b_electron_topoetcone30,           "electron_topoetcone30[electron_n]/F");
    m_tree->Branch ("electron_topoetcone40",          &m_b_electron_topoetcone40,           "electron_topoetcone30[electron_n]/F");
    m_tree->Branch ("electron_id_track_pt",           &m_b_electron_id_track_pt,            "electron_id_track_pt[electron_n]/F");
    m_tree->Branch ("electron_id_track_eta",          &m_b_electron_id_track_eta,           "electron_id_track_eta[electron_n]/F");
    m_tree->Branch ("electron_id_track_phi",          &m_b_electron_id_track_phi,           "electron_id_track_phi[electron_n]/F");
    m_tree->Branch ("electron_id_track_charge",       &m_b_electron_id_track_charge,        "electron_id_track_charge[electron_n]/F");
    m_tree->Branch ("electron_id_track_d0",           &m_b_electron_id_track_d0,            "electron_id_track_d0[electron_n]/F");
    m_tree->Branch ("electron_id_track_d0sig",        &m_b_electron_id_track_d0sig,         "electron_id_track_d0sig[electron_n]/F");
    m_tree->Branch ("electron_id_track_z0",           &m_b_electron_id_track_z0,            "electron_id_track_z0[electron_n]/F");
    m_tree->Branch ("electron_id_track_z0sig",        &m_b_electron_id_track_z0sig,         "electron_id_track_z0sig[electron_n]/F");
    m_tree->Branch ("electron_id_track_theta",        &m_b_electron_id_track_theta,         "electron_id_track_theta[electron_n]/F");
    m_tree->Branch ("electron_id_track_vz",           &m_b_electron_id_track_vz,            "electron_id_track_vz[electron_n]/F");
    m_tree->Branch ("electron_id_track_Loose",        &m_b_electron_id_track_Loose,         "electron_id_track_Loose[electron_n]/O");
    m_tree->Branch ("electron_id_track_LoosePrimary", &m_b_electron_id_track_LoosePrimary,  "electron_id_track_LoosePrimary[electron_n]/O");
    m_tree->Branch ("electron_id_track_TightPrimary", &m_b_electron_id_track_TightPrimary,  "electron_id_track_TightPrimary[electron_n]/O");
    m_tree->Branch ("electron_id_track_HITight",      &m_b_electron_id_track_HITight,       "electron_id_track_HITight[electron_n]/O");
    m_tree->Branch ("electron_id_track_HILoose",      &m_b_electron_id_track_HILoose,       "electron_id_track_HILoose[electron_n]/O");
    m_tree->Branch ("electron_Rhad1",                 &m_b_electron_Rhad1,                  "electron_Rhad1[electron_n]/F");
    m_tree->Branch ("electron_Rhad",                  &m_b_electron_Rhad,                   "electron_Rhad[electron_n]/F");
    m_tree->Branch ("electron_e277",                  &m_b_electron_e277,                   "electron_e277[electron_n]/F");
    m_tree->Branch ("electron_Reta",                  &m_b_electron_Reta,                   "electron_Reta[electron_n]/F");
    m_tree->Branch ("electron_Rphi",                  &m_b_electron_Rphi,                   "electron_Rphi[electron_n]/F");
    m_tree->Branch ("electron_weta1",                 &m_b_electron_weta1,                  "electron_weta1[electron_n]/F");
    m_tree->Branch ("electron_weta2",                 &m_b_electron_weta2,                  "electron_weta2[electron_n]/F");
    m_tree->Branch ("electron_wtots1",                &m_b_electron_wtots1,                 "electron_wtots1[electron_n]/F");
    m_tree->Branch ("electron_f1",                    &m_b_electron_f1,                     "electron_f1[electron_n]/F");
    m_tree->Branch ("electron_f3",                    &m_b_electron_f3,                     "electron_f3[electron_n]/F");
    m_tree->Branch ("electron_fracs1",                &m_b_electron_fracs1,                 "electron_fracs1[electron_n]/F");
    m_tree->Branch ("electron_DeltaE",                &m_b_electron_DeltaE,                 "electron_DeltaE[electron_n]/F");
    m_tree->Branch ("electron_Eratio",                &m_b_electron_Eratio,                 "electron_Eratio[electron_n]/F");
    m_tree->Branch ("electron_pt_sys",                &m_b_electron_pt_sys);//,                 "electron_pt_sys[electron_n]/F");
    m_tree->Branch ("electron_eta_sys",               &m_b_electron_eta_sys);//,                "electron_eta_sys[electron_n]/F");
    m_tree->Branch ("electron_phi_sys",               &m_b_electron_phi_sys);//,                "electron_phi_sys[electron_n]/F");
    //for (int i = 0; i < nElectronSys; i++) {
    //  m_b_electron_pt_sys_ALL[i]  = new float [m_max_electron_n];
    //  m_b_electron_eta_sys_ALL[i] = new float [m_max_electron_n];
    //  m_b_electron_phi_sys_ALL[i] = new float [m_max_electron_n];
    //  m_tree->Branch (Form ("electron_pt_sys_%i",   i), m_b_electron_pt_sys_ALL[i],   Form ("electron_pt_sys_%i[electron_n]/F",   i));
    //  m_tree->Branch (Form ("electron_eta_sys_%i",  i), m_b_electron_eta_sys_ALL[i],  Form ("electron_eta_sys_%i[electron_n]/F",  i));
    //  m_tree->Branch (Form ("electron_phi_sys_%i",  i), m_b_electron_phi_sys_ALL[i],  Form ("electron_phi_sys_%i[electron_n]/F",  i));
    //}
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Reconstructed electron branches ready.");
    }
  }


  // Save clusters info
  if (m_saveObjects.count (SaveObject::Photons) > 0 || m_saveObjects.count (SaveObject::Electrons) > 0) {
    // EM Calorimeter cluster info
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Creating branches for electromagnetic calorimeter clusters...");
    }
    m_tree->Branch ("em_cluster_n",         &m_b_em_cluster_n,        "em_cluster_n/I");
    m_tree->Branch ("em_cluster_pt",        &m_b_em_cluster_pt,       "em_cluster_pt[em_cluster_n]/F");
    m_tree->Branch ("em_cluster_et",        &m_b_em_cluster_et,       "em_cluster_et[em_cluster_n]/F");
    m_tree->Branch ("em_cluster_eta",       &m_b_em_cluster_eta,      "em_cluster_eta[em_cluster_n]/F");
    m_tree->Branch ("em_cluster_phi",       &m_b_em_cluster_phi,      "em_cluster_phi[em_cluster_n]/F");
    m_tree->Branch ("em_cluster_energyBE",  &m_b_em_cluster_energyBE, "em_cluster_energyBE[em_cluster_n]/F");
    m_tree->Branch ("em_cluster_etaBE",     &m_b_em_cluster_etaBE,    "em_cluster_etaBE[em_cluster_n]/F");
    m_tree->Branch ("em_cluster_phiBE",     &m_b_em_cluster_phiBE,    "em_cluster_phiBE[em_cluster_n]/F");
    m_tree->Branch ("em_cluster_calE",      &m_b_em_cluster_calE,     "em_cluster_calE[em_cluster_n]/F");
    m_tree->Branch ("em_cluster_calEta",    &m_b_em_cluster_calEta,   "em_cluster_calEta[em_cluster_n]/F");
    m_tree->Branch ("em_cluster_calPhi",    &m_b_em_cluster_calPhi,   "em_cluster_calPhi[em_cluster_n]/F");
    m_tree->Branch ("em_cluster_size",      &m_b_em_cluster_size,     "em_cluster_size[em_cluster_n]/I");
    m_tree->Branch ("em_cluster_status",    &m_b_em_cluster_status,   "em_cluster_status[em_cluster_n]/I");
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Electromagnetic calorimeter cluster branches ready.");
    }
  }


  // Save muon info
  if (m_saveObjects.count (SaveObject::Muons) > 0) {
    // Truth muons info
    if (!IsCollisions (m_dataType)) {
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Creating branches for truth muons...");
      }
      m_tree->Branch ("truth_muon_n",       &m_b_truth_muon_n,        "truth_muon_n/I");
      m_tree->Branch ("truth_muon_pt",      &m_b_truth_muon_pt,       "truth_muon_pt[truth_muon_n]/F");
      m_tree->Branch ("truth_muon_eta",     &m_b_truth_muon_eta,      "truth_muon_eta[truth_muon_n]/F");
      m_tree->Branch ("truth_muon_phi",     &m_b_truth_muon_phi,      "truth_muon_phi[truth_muon_n]/F");
      m_tree->Branch ("truth_muon_charge",  &m_b_truth_muon_charge,   "truth_muon_charge[truth_muon_n]/I");
      m_tree->Branch ("truth_muon_barcode", &m_b_truth_muon_barcode,  "truth_muon_barcode[truth_muon_n]/I");
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Truth muon branches ready.");
      }
    }

    // Reco muons info
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Creating branches for reconstructed muons...");
    }
    m_tree->Branch ("muon_n",                     &m_b_muon_n,                      "muon_n/I");
    m_tree->Branch ("muon_pt_precalib",           &m_b_muon_pt_precalib,            "muon_pt_precalib[muon_n]/F");
    m_tree->Branch ("muon_pt",                    &m_b_muon_pt,                     "muon_pt[muon_n]/F");
    m_tree->Branch ("muon_ms_pt_precalib",        &m_b_muon_ms_pt_precalib,         "muon_ms_pt_precalib[muon_n]/F");
    m_tree->Branch ("muon_ms_pt",                 &m_b_muon_ms_pt,                  "muon_ms_pt[muon_n]/F");
    m_tree->Branch ("muon_eta",                   &m_b_muon_eta,                    "muon_eta[muon_n]/F");
    m_tree->Branch ("muon_phi",                   &m_b_muon_phi,                    "muon_phi[muon_n]/F");
    m_tree->Branch ("muon_charge",                &m_b_muon_charge,                 "muon_charge[muon_n]/I");
    m_tree->Branch ("muon_tight",                 &m_b_muon_tight,                  "muon_tight[muon_n]/O");
    m_tree->Branch ("muon_medium",                &m_b_muon_medium,                 "muon_medium[muon_n]/O");
    m_tree->Branch ("muon_loose",                 &m_b_muon_loose,                  "muon_loose[muon_n]/O");
    if (m_triggerSet.count (EventTrigger::Muon) > 0) {
      m_tree->Branch ("muon_matched",             &m_b_muon_matched,                Form ("muon_matched[%i][muon_n]/O", m_muon_trig_n));
    }
    m_tree->Branch ("muon_etcone20",              &m_b_muon_etcone20,               "muon_etcone20[muon_n]/F");
    m_tree->Branch ("muon_etcone30",              &m_b_muon_etcone30,               "muon_etcone30[muon_n]/F");
    m_tree->Branch ("muon_etcone40",              &m_b_muon_etcone40,               "muon_etcone40[muon_n]/F");
    m_tree->Branch ("muon_topoetcone20",          &m_b_muon_topoetcone20,           "muon_topoetcone20[muon_n]/F");
    m_tree->Branch ("muon_topoetcone30",          &m_b_muon_topoetcone30,           "muon_topoetcone30[muon_n]/F");
    m_tree->Branch ("muon_topoetcone40",          &m_b_muon_topoetcone40,           "muon_topoetcone40[muon_n]/F");
    m_tree->Branch ("muon_id_track_pt",           &m_b_muon_id_track_pt,            "muon_id_track_pt[muon_n]/F");
    m_tree->Branch ("muon_id_track_eta",          &m_b_muon_id_track_eta,           "muon_id_track_eta[muon_n]/F");
    m_tree->Branch ("muon_id_track_phi",          &m_b_muon_id_track_phi,           "muon_id_track_phi[muon_n]/F");
    m_tree->Branch ("muon_id_track_charge",       &m_b_muon_id_track_charge,        "muon_id_track_charge[muon_n]/F");
    m_tree->Branch ("muon_id_track_d0",           &m_b_muon_id_track_d0,            "muon_id_track_d0[muon_n]/F");
    m_tree->Branch ("muon_id_track_d0sig",        &m_b_muon_id_track_d0sig,         "muon_id_track_d0sig[muon_n]/F");
    m_tree->Branch ("muon_id_track_z0",           &m_b_muon_id_track_z0,            "muon_id_track_z0[muon_n]/F");
    m_tree->Branch ("muon_id_track_z0sig",        &m_b_muon_id_track_z0sig,         "muon_id_track_z0sig[muon_n]/F");
    m_tree->Branch ("muon_id_track_theta",        &m_b_muon_id_track_theta,         "muon_id_track_theta[muon_n]/F");
    m_tree->Branch ("muon_id_track_vz",           &m_b_muon_id_track_vz,            "muon_id_track_vz[muon_n]/F");
    m_tree->Branch ("muon_id_track_Loose",        &m_b_muon_id_track_Loose,         "muon_id_track_Loose[muon_n]/O");
    m_tree->Branch ("muon_id_track_LoosePrimary", &m_b_muon_id_track_LoosePrimary,  "muon_id_track_LoosePrimary[muon_n]/O");
    m_tree->Branch ("muon_id_track_TightPrimary", &m_b_muon_id_track_TightPrimary,  "muon_id_track_TightPrimary[muon_n]/O");
    m_tree->Branch ("muon_id_track_HITight",      &m_b_muon_id_track_HITight,       "muon_id_track_HITight[muon_n]/O");
    m_tree->Branch ("muon_id_track_HILoose",      &m_b_muon_id_track_HILoose,       "muon_id_track_HILoose[muon_n]/O");
    m_tree->Branch ("muon_ms_track_pt",           &m_b_muon_ms_track_pt,            "muon_ms_track_pt[muon_n]/F");
    m_tree->Branch ("muon_ms_track_eta",          &m_b_muon_ms_track_eta,           "muon_ms_track_eta[muon_n]/F");
    m_tree->Branch ("muon_ms_track_phi",          &m_b_muon_ms_track_phi,           "muon_ms_track_phi[muon_n]/F");
    m_tree->Branch ("muon_ms_track_charge",       &m_b_muon_ms_track_charge,        "muon_ms_track_charge[muon_n]/F");
    m_tree->Branch ("muon_ms_track_d0",           &m_b_muon_ms_track_d0,            "muon_ms_track_d0[muon_n]/F");
    m_tree->Branch ("muon_ms_track_d0sig",        &m_b_muon_ms_track_d0sig,         "muon_ms_track_d0sig[muon_n]/F");
    m_tree->Branch ("muon_ms_track_z0",           &m_b_muon_ms_track_z0,            "muon_ms_track_z0[muon_n]/F");
    m_tree->Branch ("muon_ms_track_z0sig",        &m_b_muon_ms_track_z0sig,         "muon_ms_track_z0sig[muon_n]/F");
    m_tree->Branch ("muon_ms_track_theta",        &m_b_muon_ms_track_theta,         "muon_ms_track_theta[muon_n]/F");
    m_tree->Branch ("muon_ms_track_vz",           &m_b_muon_ms_track_vz,            "muon_ms_track_vz[muon_n]/F");
    m_tree->Branch ("muon_pt_sys",                &m_b_muon_pt_sys);//,                 "muon_pt_sys[muon_n]/F");
    m_tree->Branch ("muon_eta_sys",               &m_b_muon_eta_sys);//,                "muon_eta_sys[muon_n]/F");
    m_tree->Branch ("muon_phi_sys",               &m_b_muon_phi_sys);//,                "muon_phi_sys[muon_n]/F");
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Reconstructed muon branches ready.");
    }
  }


  // Save tracks info
  if (m_saveObjects.count (SaveObject::Tracks) > 0) {
    // Truth tracks info
    if (!IsCollisions (m_dataType)) {
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Creating branches for truth charged particles...");
      }
      m_tree->Branch ("truth_trk_n",        &m_b_truth_trk_n,         "truth_trk_n/I");
      m_tree->Branch ("truth_trk_pt",       &m_b_truth_trk_pt,        "truth_trk_pt[truth_trk_n]/F");
      m_tree->Branch ("truth_trk_eta",      &m_b_truth_trk_eta,       "truth_trk_eta[truth_trk_n]/F");
      m_tree->Branch ("truth_trk_phi",      &m_b_truth_trk_phi,       "truth_trk_phi[truth_trk_n]/F");
      m_tree->Branch ("truth_trk_charge",   &m_b_truth_trk_charge,    "truth_trk_charge[truth_trk_n]/F");
      m_tree->Branch ("truth_trk_pdgid",    &m_b_truth_trk_pdgid,     "truth_trk_pdgid[truth_trk_n]/I");
      m_tree->Branch ("truth_trk_barcode",  &m_b_truth_trk_barcode,   "truth_trk_barcode[truth_trk_n]/I");
      m_tree->Branch ("truth_trk_isHadron", &m_b_truth_trk_isHadron,  "truth_trk_isHadron[truth_trk_n]/O");
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Truth charged particle branches ready.");
      }
    }


    // Reco tracks info
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Creating branches for inner detector tracks...");
    }
    m_tree->Branch ("ntrk",                   &m_b_ntrk,                  "ntrk/I");
    m_tree->Branch ("trk_pt",                 &m_b_trk_pt,                "trk_pt[ntrk]/F");
    m_tree->Branch ("trk_eta",                &m_b_trk_eta,               "trk_eta[ntrk]/F");
    m_tree->Branch ("trk_phi",                &m_b_trk_phi,               "trk_phi[ntrk]/F");
    m_tree->Branch ("trk_charge",             &m_b_trk_charge,            "trk_charge[ntrk]/F");
    m_tree->Branch ("trk_Loose",              &m_b_trk_Loose,             "trk_Loose[ntrk]/O");
    m_tree->Branch ("trk_LoosePrimary",       &m_b_trk_LoosePrimary,      "trk_LoosePrimary[ntrk]/O");
    m_tree->Branch ("trk_TightPrimary",       &m_b_trk_TightPrimary,      "trk_TightPrimary[ntrk]/O");
    m_tree->Branch ("trk_HITight",            &m_b_trk_HITight,           "trk_HITight[ntrk]/O");
    m_tree->Branch ("trk_HILoose",            &m_b_trk_HILoose,           "trk_HILoose[ntrk]/O");
    m_tree->Branch ("trk_d0",                 &m_b_trk_d0,                "trk_d0[ntrk]/F");
    m_tree->Branch ("trk_d0sig",              &m_b_trk_d0sig,             "trk_d0sig[ntrk]/F");
    m_tree->Branch ("trk_z0",                 &m_b_trk_z0,                "trk_z0[ntrk]/F");
    m_tree->Branch ("trk_z0sig",              &m_b_trk_z0sig,             "trk_z0sig[ntrk]/F");
    m_tree->Branch ("trk_theta",              &m_b_trk_theta,             "trk_theta[ntrk]/F");
    m_tree->Branch ("trk_vz",                 &m_b_trk_vz,                "trk_vz[ntrk]/F");
    m_tree->Branch ("trk_nBLayerHits",        &m_b_trk_nBLayerHits,       "trk_nBLayerHits[ntrk]/C");
    m_tree->Branch ("trk_nBLayerSharedHits",  &m_b_trk_nBLayerSharedHits, "trk_nBLayerSharedHits[ntrk]/C");
    m_tree->Branch ("trk_nPixelHits",         &m_b_trk_nPixelHits,        "trk_nPixelHits[ntrk]/C");
    m_tree->Branch ("trk_nPixelHoles",        &m_b_trk_nPixelHoles,       "trk_nPixelHoles[ntrk]/C");
    m_tree->Branch ("trk_nPixelSharedHits",   &m_b_trk_nPixelSharedHits,  "trk_nPixelSharedHits[ntrk]/C");
    m_tree->Branch ("trk_nPixelDeadSensors",  &m_b_trk_nPixelDeadSensors, "trk_nPixelDeadSensors[ntrk]/C");
    m_tree->Branch ("trk_nSCTHits",           &m_b_trk_nSCTHits,          "trk_nSCTHits[ntrk]/C");
    m_tree->Branch ("trk_nSCTHoles",          &m_b_trk_nSCTHoles,         "trk_nSCTHoles[ntrk]/C");
    m_tree->Branch ("trk_nSCTDoubleHoles",    &m_b_trk_nSCTDoubleHoles,   "trk_nSCTDoubleHoles[ntrk]/C");
    m_tree->Branch ("trk_nSCTSharedHits",     &m_b_trk_nSCTSharedHits,    "trk_nSCTSharedHits[ntrk]/C");
    m_tree->Branch ("trk_nSCTDeadSensors",    &m_b_trk_nSCTDeadSensors,   "trk_nSCTDeadSensors[ntrk]/C");
    m_tree->Branch ("trk_pixeldEdx",          &m_b_trk_pixeldEdx,         "trk_pixeldEdx[ntrk]/F");
    if (!IsCollisions (m_dataType)) {
      if (m_debug >= 2) {
        Info ("histInitialize ()", "  --> creating branches for inner detector tracks truth-matched information...");
      }
      m_tree->Branch ("trk_prob_truth",     &m_b_trk_prob_truth,      "trk_prob_truth[ntrk]/F");
      m_tree->Branch ("trk_truth_pt",       &m_b_trk_truth_pt,        "trk_truth_pt[ntrk]/F");
      m_tree->Branch ("trk_truth_eta",      &m_b_trk_truth_eta,       "trk_truth_eta[ntrk]/F");
      m_tree->Branch ("trk_truth_phi",      &m_b_trk_truth_phi,       "trk_truth_phi[ntrk]/F");
      m_tree->Branch ("trk_truth_charge",   &m_b_trk_truth_charge,    "trk_truth_charge[ntrk]/F");
      m_tree->Branch ("trk_truth_type",     &m_b_trk_truth_type,      "trk_truth_type[ntrk]/I");
      m_tree->Branch ("trk_truth_orig",     &m_b_trk_truth_orig,      "trk_truth_orig[ntrk]/I");
      m_tree->Branch ("trk_truth_barcode",  &m_b_trk_truth_barcode,   "trk_truth_barcode[ntrk]/I");
      m_tree->Branch ("trk_truth_pdgid",    &m_b_trk_truth_pdgid,     "trk_truth_pdgid[ntrk]/I");
      m_tree->Branch ("trk_truth_vz",       &m_b_trk_truth_vz,        "trk_truth_vz[ntrk]/F");
      m_tree->Branch ("trk_truth_nIn",      &m_b_trk_truth_nIn,       "trk_truth_nIn[ntrk]/I");
      m_tree->Branch ("trk_truth_isHadron", &m_b_trk_truth_isHadron,  "trk_truth_isHadron[ntrk]/O");
    }
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Inner detector track branches ready.");
    }
  }


  // Save clusters info
  if (m_saveObjects.count (SaveObject::Clusters) > 0) {
    // HI Calorimeter cluster info
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Creating branches for HI calorimeter clusters...");
    }
    m_tree->Branch ("hi_cluster_n",        &m_b_hi_cluster_n,        "hi_cluster_n/I");
    m_tree->Branch ("hi_cluster_pt",       &m_b_hi_cluster_pt,       "hi_cluster_pt[hi_cluster_n]/F");
    m_tree->Branch ("hi_cluster_et",       &m_b_hi_cluster_et,       "hi_cluster_et[hi_cluster_n]/F");
    m_tree->Branch ("hi_cluster_eta",      &m_b_hi_cluster_eta,      "hi_cluster_eta[hi_cluster_n]/F");
    m_tree->Branch ("hi_cluster_phi",      &m_b_hi_cluster_phi,      "hi_cluster_phi[hi_cluster_n]/F");
    m_tree->Branch ("hi_cluster_energyBE", &m_b_hi_cluster_energyBE, "hi_cluster_energyBE[hi_cluster_n]/F");
    m_tree->Branch ("hi_cluster_etaBE",    &m_b_hi_cluster_etaBE,    "hi_cluster_etaBE[hi_cluster_n]/F");
    m_tree->Branch ("hi_cluster_phiBE",    &m_b_hi_cluster_phiBE,    "hi_cluster_phiBE[hi_cluster_n]/F");
    m_tree->Branch ("hi_cluster_calE",     &m_b_hi_cluster_calE,     "hi_cluster_calE[hi_cluster_n]/F");
    m_tree->Branch ("hi_cluster_calEta",   &m_b_hi_cluster_calEta,   "hi_cluster_calEta[hi_cluster_n]/F");
    m_tree->Branch ("hi_cluster_calPhi",   &m_b_hi_cluster_calPhi,   "hi_cluster_calPhi[hi_cluster_n]/F");
    m_tree->Branch ("hi_cluster_size",     &m_b_hi_cluster_size,     "hi_cluster_size[hi_cluster_n]/I");
    m_tree->Branch ("hi_cluster_status",   &m_b_hi_cluster_status,   "hi_cluster_status[hi_cluster_n]/I");
    if (m_debug >= 1) {
      Info ("histInitialize ()", "HI cluster branches ready.");
    }
  }


  // Save jets info
  if (m_saveObjects.count (SaveObject::Jets) > 0) {
    // Truth jet info
    if (!IsCollisions (m_dataType)) {
      // Anti-kT R=0.2 Truth Jet info
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Creating branches for anti-kT R=0.2 truth jets...");
      }
      m_tree->Branch ("akt2_truth_jet_n",   &m_b_akt2_truth_jet_n,    "akt2_truth_jet_n/I");
      m_tree->Branch ("akt2_truth_jet_pt",  &m_b_akt2_truth_jet_pt,   "akt2_truth_jet_pt[akt2_truth_jet_n]/F");
      m_tree->Branch ("akt2_truth_jet_eta", &m_b_akt2_truth_jet_eta,  "akt2_truth_jet_eta[akt2_truth_jet_n]/F");
      m_tree->Branch ("akt2_truth_jet_phi", &m_b_akt2_truth_jet_phi,  "akt2_truth_jet_phi[akt2_truth_jet_n]/F");
      m_tree->Branch ("akt2_truth_jet_e",   &m_b_akt2_truth_jet_e,    "akt2_truth_jet_e[akt2_truth_jet_n]/F");
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Anti-kT R=0.2 truth jets branches ready.");
      }

      // Anti-kT R=0.4 Truth Jet info
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Creating branches for anti-kT R=0.4 truth jets...");
      }
      m_tree->Branch ("akt4_truth_jet_n",   &m_b_akt4_truth_jet_n,    "akt4_truth_jet_n/I");
      m_tree->Branch ("akt4_truth_jet_pt",  &m_b_akt4_truth_jet_pt,   "akt4_truth_jet_pt[akt4_truth_jet_n]/F");
      m_tree->Branch ("akt4_truth_jet_eta", &m_b_akt4_truth_jet_eta,  "akt4_truth_jet_eta[akt4_truth_jet_n]/F");
      m_tree->Branch ("akt4_truth_jet_phi", &m_b_akt4_truth_jet_phi,  "akt4_truth_jet_phi[akt4_truth_jet_n]/F");
      m_tree->Branch ("akt4_truth_jet_e",   &m_b_akt4_truth_jet_e,    "akt4_truth_jet_e[akt4_truth_jet_n]/F");
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Anti-kT R=0.4 truth jets branches ready.");
      }
    }

    // Anti-kT R=0.4 EMTopo Jet info
    if (Ispp (m_collisionSystem)) {
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Creating branches for anti-kT R=0.4 EMtopo jets...");
      }
      m_tree->Branch ("akt4_emtopo_jet_n",         &m_b_akt4_emtopo_jet_n,        "akt4_emtopo_jet_n/I");
      m_tree->Branch ("akt4_emtopo_jet_pt",        &m_b_akt4_emtopo_jet_pt,       "akt4_emtopo_jet_pt[akt4_emtopo_jet_n]/F");
      m_tree->Branch ("akt4_emtopo_jet_eta",       &m_b_akt4_emtopo_jet_eta,      "akt4_emtopo_jet_eta[akt4_emtopo_jet_n]/F");
      m_tree->Branch ("akt4_emtopo_jet_phi",       &m_b_akt4_emtopo_jet_phi,      "akt4_emtopo_jet_phi[akt4_emtopo_jet_n]/F");
      m_tree->Branch ("akt4_emtopo_jet_e",         &m_b_akt4_emtopo_jet_e,        "akt4_emtopo_jet_e[akt4_emtopo_jet_n]/F");
      m_tree->Branch ("akt4_emtopo_jet_LooseBad",  &m_b_akt4_emtopo_jet_LooseBad, "akt4_emtopo_jet_LooseBad[akt4_emtopo_jet_n]/O");
      if (m_debug >= 1) {
        Info ("histInitialize ()", "Anti-kT R=0.4 EMtopo jet branches ready.");
      }
    }


    // Anti-kT R=0.2 HI Jet info
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Creating branches for anti-kT R=0.2 HI jets...");
    }
    m_tree->Branch ("akt2_hi_jet_n",            &m_b_akt2_hi_jet_n,             "akt2_hi_jet_n/I");
    m_tree->Branch ("akt2_hi_jet_pt_precalib",  &m_b_akt2_hi_jet_pt_precalib,   "akt2_hi_jet_pt_precalib[akt2_hi_jet_n]/F");
    m_tree->Branch ("akt2_hi_jet_eta_precalib", &m_b_akt2_hi_jet_eta_precalib,  "akt2_hi_jet_eta_precalib[akt2_hi_jet_n]/F");
    m_tree->Branch ("akt2_hi_jet_e_precalib",   &m_b_akt2_hi_jet_e_precalib,    "akt2_hi_jet_e_precalib[akt2_hi_jet_n]/F");
    m_tree->Branch ("akt2_hi_jet_pt_etajes",    &m_b_akt2_hi_jet_pt_etajes,     "akt2_hi_jet_pt_etajes[akt2_hi_jet_n]/F");
    m_tree->Branch ("akt2_hi_jet_eta_etajes",   &m_b_akt2_hi_jet_eta_etajes,    "akt2_hi_jet_eta_etajes[akt2_hi_jet_n]/F");
    m_tree->Branch ("akt2_hi_jet_e_etajes",     &m_b_akt2_hi_jet_e_etajes,      "akt2_hi_jet_e_etajes[akt2_hi_jet_n]/F");
    m_tree->Branch ("akt2_hi_jet_pt_xcalib",    &m_b_akt2_hi_jet_pt_xcalib,     "akt2_hi_jet_pt_xcalib[akt2_hi_jet_n]/F");
    m_tree->Branch ("akt2_hi_jet_eta_xcalib",   &m_b_akt2_hi_jet_eta_xcalib,    "akt2_hi_jet_eta_xcalib[akt2_hi_jet_n]/F");
    m_tree->Branch ("akt2_hi_jet_e_xcalib",     &m_b_akt2_hi_jet_e_xcalib,      "akt2_hi_jet_e_xcalib[akt2_hi_jet_n]/F");
    m_tree->Branch ("akt2_hi_jet_phi",          &m_b_akt2_hi_jet_phi,           "akt2_hi_jet_phi[akt2_hi_jet_n]/F");
    m_tree->Branch ("akt2_hi_jet_sub_et",       &m_b_akt2_hi_jet_sub_et,        "akt2_hi_jet_sub_et[akt2_hi_jet_n]/F");
    m_tree->Branch ("akt2_hi_jet_sub_e",        &m_b_akt2_hi_jet_sub_e,         "akt2_hi_jet_sub_e[akt2_hi_jet_n]/F");
    m_tree->Branch ("akt2_hi_jet_LooseBad",     &m_b_akt2_hi_jet_LooseBad,      "akt2_hi_jet_LooseBad[akt2_hi_jet_n]/O");
    //if (!IsCollisions (m_dataType)) {
    for (int i = 0; i < nJESSys; i++) {
      m_b_akt2_hi_jet_pt_sys_JES_ALL[i] = new float[m_max_akt2_hi_jet_n];
      m_tree->Branch (Form ("akt2_hi_jet_pt_sys_JES_%i", i), m_b_akt2_hi_jet_pt_sys_JES_ALL[i], Form ("akt2_hi_jet_pt_sys_JES_%i[akt2_hi_jet_n]/F", i));
    }
    for (int i = 0; i < nJERSys; i++) {
      m_b_akt2_hi_jet_pt_sys_JER_ALL[i] = new float[m_max_akt2_hi_jet_n];
      m_tree->Branch (Form ("akt2_hi_jet_pt_sys_JER_%i", i), m_b_akt2_hi_jet_pt_sys_JER_ALL[i], Form ("akt2_hi_jet_pt_sys_JER_%i[akt2_hi_jet_n]/F", i));
    }
    //}
    m_tree->Branch ("akt2_hi_jet_timing",       &m_b_akt2_hi_jet_timing,        "akt2_hi_jet_timing[akt2_hi_jet_n]/F");
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Anti-kT R=0.2 HI jet branches ready.");
    }


    // Anti-kT R=0.4 HI Jet info
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Creating branches for anti-kT R=0.4 HI jets...");
    }
    m_tree->Branch ("akt4_hi_jet_n",            &m_b_akt4_hi_jet_n,             "akt4_hi_jet_n/I");
    m_tree->Branch ("akt4_hi_jet_pt_precalib",  &m_b_akt4_hi_jet_pt_precalib,   "akt4_hi_jet_pt_precalib[akt4_hi_jet_n]/F");
    m_tree->Branch ("akt4_hi_jet_eta_precalib", &m_b_akt4_hi_jet_eta_precalib,  "akt4_hi_jet_eta_precalib[akt4_hi_jet_n]/F");
    m_tree->Branch ("akt4_hi_jet_e_precalib",   &m_b_akt4_hi_jet_e_precalib,    "akt4_hi_jet_e_precalib[akt4_hi_jet_n]/F");
    m_tree->Branch ("akt4_hi_jet_pt_etajes",    &m_b_akt4_hi_jet_pt_etajes,     "akt4_hi_jet_pt_etajes[akt4_hi_jet_n]/F");
    m_tree->Branch ("akt4_hi_jet_eta_etajes",   &m_b_akt4_hi_jet_eta_etajes,    "akt4_hi_jet_eta_etajes[akt4_hi_jet_n]/F");
    m_tree->Branch ("akt4_hi_jet_e_etajes",     &m_b_akt4_hi_jet_e_etajes,      "akt4_hi_jet_e_etajes[akt4_hi_jet_n]/F");
    m_tree->Branch ("akt4_hi_jet_pt_xcalib",    &m_b_akt4_hi_jet_pt_xcalib,     "akt4_hi_jet_pt_xcalib[akt4_hi_jet_n]/F");
    m_tree->Branch ("akt4_hi_jet_eta_xcalib",   &m_b_akt4_hi_jet_eta_xcalib,    "akt4_hi_jet_eta_xcalib[akt4_hi_jet_n]/F");
    m_tree->Branch ("akt4_hi_jet_e_xcalib",     &m_b_akt4_hi_jet_e_xcalib,      "akt4_hi_jet_e_xcalib[akt4_hi_jet_n]/F");
    m_tree->Branch ("akt4_hi_jet_phi",          &m_b_akt4_hi_jet_phi,           "akt4_hi_jet_phi[akt4_hi_jet_n]/F");
    m_tree->Branch ("akt4_hi_jet_sub_et",       &m_b_akt4_hi_jet_sub_et,        "akt4_hi_jet_sub_et[akt4_hi_jet_n]/F");
    m_tree->Branch ("akt4_hi_jet_sub_e",        &m_b_akt4_hi_jet_sub_e,         "akt4_hi_jet_sub_e[akt4_hi_jet_n]/F");
    m_tree->Branch ("akt4_hi_jet_LooseBad",     &m_b_akt4_hi_jet_LooseBad,      "akt4_hi_jet_LooseBad[akt4_hi_jet_n]/O");
    //if (!IsCollisions (m_dataType)) {
    for (int i = 0; i < nJESSys; i++) {
      m_b_akt4_hi_jet_pt_sys_JES_ALL[i] = new float[m_max_akt4_hi_jet_n];
      m_tree->Branch (Form ("akt4_hi_jet_pt_sys_JES_%i", i), m_b_akt4_hi_jet_pt_sys_JES_ALL[i], Form ("akt4_hi_jet_pt_sys_JES_%i[akt4_hi_jet_n]/F", i));
    }
    for (int i = 0; i < nJERSys; i++) {
      m_b_akt4_hi_jet_pt_sys_JER_ALL[i] = new float[m_max_akt4_hi_jet_n];
      m_tree->Branch (Form ("akt4_hi_jet_pt_sys_JER_%i", i), m_b_akt4_hi_jet_pt_sys_JER_ALL[i], Form ("akt4_hi_jet_pt_sys_JER_%i[akt4_hi_jet_n]/F", i));
    }
    //}
    m_tree->Branch ("akt4_hi_jet_timing",       &m_b_akt4_hi_jet_timing,        "akt4_hi_jet_timing[akt4_hi_jet_n]/F");
    if (m_debug >= 1) {
      Info ("histInitialize ()", "Anti-kT R=0.4 HI jet branches ready.");
    }
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: fileExecute () {
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: changeInput (bool /*firstFile*/) {
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: initialize () {
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  ANA_CHECK_SET_TYPE (EL::StatusCode);


  //----------------------------------------------------------------------
  // Trigger initialization
  //----------------------------------------------------------------------
  if (IsCollisions (m_dataType)) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing trigger selection tools...");
    }

    // Initialize trigger configuration tool
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up trigger configuration tool...");
    }
    m_trigConfigTool = new TrigConf::xAODConfigTool ("xAODConfigTool"); // gives us access to the meta-data
    ANA_CHECK (m_trigConfigTool->initialize ());
    ToolHandle<TrigConf::ITrigConfigTool> trigConfigHandle (m_trigConfigTool);
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> trigger configuration tool ready.");
    }

    // Initialize trigger decision tool and connect to trigger configuration tool (NOTE: must be after trigger configuration tool is initialized!!!)
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up trigger decision tool...");
    }
    m_trigDecisionTool = new Trig::TrigDecisionTool ("TrigDecisionTool");
    ANA_CHECK (m_trigDecisionTool->setProperty ("ConfigTool", trigConfigHandle)); // connect the TrigDecisionTool to the ConfigTool
    ANA_CHECK (m_trigDecisionTool->setProperty ("TrigDecisionKey", "xTrigDecision"));
    ANA_CHECK (m_trigDecisionTool->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> trigger decision tool ready.");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up trigger matching tool...");
    }
    m_trigMatchingTool = new Trig::MatchingTool ("xAODMatchingTool");
    //ANA_CHECK (m_trigMatchingTool->setProperty ("OutputLevel", MSG::DEBUG));
    ANA_CHECK (m_trigMatchingTool->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> trigger matching tool ready.");
    }

    if (m_debug >= 1) {
      Info ("initialize ()", "Trigger selection tools ready.");
    }
  }

 
  //----------------------------------------------------------------------
  // GRL
  //----------------------------------------------------------------------
  if (IsCollisions (m_dataType) || IsDataOverlay (m_dataType)) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing good runs list tool...");
    }

    std::vector <std::string> vecStringGRL;
    if (m_debug >= 2) {
      Info ("initialize ()", Form ("  --> adding %s/%s...", filePath.c_str (), m_pp15_grl_name.c_str ()));
    }
    vecStringGRL.push_back (gSystem->ExpandPathName (Form ("%s/%s", filePath.c_str (), m_pp15_grl_name.c_str ())));
    if (m_debug >= 2) {
      Info ("initialize ()", Form ("  --> adding %s/%s...", filePath.c_str (), m_PbPb15_grl_name.c_str ()));
    }
    vecStringGRL.push_back (gSystem->ExpandPathName (Form ("%s/%s", filePath.c_str (), m_PbPb15_grl_name.c_str ())));
    if (m_debug >= 2) {
      Info ("initialize ()", Form ("  --> adding %s/%s...", filePath.c_str (), m_pPb16_grl_name.c_str ()));
    }
    vecStringGRL.push_back (gSystem->ExpandPathName (Form ("%s/%s", filePath.c_str (), m_pPb16_grl_name.c_str ())));
    if (m_debug >= 2) {
      Info ("initialize ()", Form ("  --> adding %s/%s...", filePath.c_str (), m_pPb16s5TeV_grl_name.c_str ()));
    }
    vecStringGRL.push_back (gSystem->ExpandPathName (Form ("%s/%s", filePath.c_str (), m_pPb16s5TeV_grl_name.c_str ())));
    if (m_debug >= 2) {
      Info ("initialize ()", Form ("  --> adding %s/%s...", filePath.c_str (), m_pp17_grl_name.c_str ()));
    }
    vecStringGRL.push_back (gSystem->ExpandPathName (Form ("%s/%s", filePath.c_str (), m_pp17_grl_name.c_str ())));
    if (m_debug >= 2) {
      Info ("initialize ()", Form ("  --> adding %s/%s...", filePath.c_str (), m_PbPb18_grl_name.c_str ()));
    }
    vecStringGRL.push_back (gSystem->ExpandPathName (Form ("%s/%s", filePath.c_str (), m_PbPb18_grl_name.c_str ())));
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> all dataset GRLs added.");
    }

    m_goodRunsList = new GoodRunsListSelectionTool ("GoodRunsListSelectionTool");
    ANA_CHECK (m_goodRunsList->setProperty ("GoodRunsListVec", vecStringGRL));
    ANA_CHECK (m_goodRunsList->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    ANA_CHECK (m_goodRunsList->initialize ());
    if (m_debug >= 1) {
      Info ("initialize ()", "Good runs list tool ready.");
    }


    if (IsPbPb18 (m_collisionSystem)) {
      if (m_debug >= 1) {
        Info ("initialize ()", "Initializing good runs list tool (ignoring toroid status)...");
      }

      vecStringGRL.clear ();
      if (m_debug >= 2) {
        Info ("initialize ()", Form ("  --> adding %s/%s...", filePath.c_str (), m_PbPb18_grl_ignoreToroid_name.c_str ()));
      }
      vecStringGRL.push_back (gSystem->ExpandPathName (Form ("%s/%s", filePath.c_str (), m_PbPb18_grl_ignoreToroid_name.c_str ())));

      m_goodRunsList_ignoreToroid = new GoodRunsListSelectionTool ("GoodRunsListSelectionTool_ignoreToroid");
      ANA_CHECK (m_goodRunsList_ignoreToroid->setProperty ("GoodRunsListVec", vecStringGRL));
      ANA_CHECK (m_goodRunsList_ignoreToroid->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
      ANA_CHECK (m_goodRunsList_ignoreToroid->initialize ());

      if (m_debug >= 1) {
        Info ("initialize ()", "Good runs list tool (ignoring toroid status) ready.");
      }
    }
  }


  //----------------------------------------------------------------------
  // OOTPU tool
  //----------------------------------------------------------------------
  {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing out-of-time pile-up calibration...");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", Form ("  --> searching for configuration file at %s", PathResolverFindCalibFile (Form ("HadronYieldsAnalysis/%s", m_oop_fname.c_str ())).c_str ()));
    }
    TFile* f_oop_In = TFile::Open (PathResolverFindCalibFile (Form ("HadronYieldsAnalysis/%s", m_oop_fname.c_str ())).c_str (), "READ");
    if (!f_oop_In) {
      Error ("initialize ()", Form ("Could not find input out-of-time pile-up calibration file %s!", m_oop_fname.c_str ()));
      return EL::StatusCode::FAILURE;
    }
    if (m_debug >= 2) {
      Info ("initialize ()", Form (" --> read out-of-time pileup cuts from %s", m_oop_fname.c_str ()));
    }

    m_oop_hMean  = (TH1D*) ((TH1D*) f_oop_In->Get ("hMeanTotal"))->Clone ("hMeanTotal_HIPileTool");
    m_oop_hMean->SetDirectory (0);
    m_oop_hSigma = (TH1D*) ((TH1D*) f_oop_In->Get ("hSigmaTotal"))->Clone ("hSigmaTotal_HIPileTool");
    m_oop_hSigma->SetDirectory (0);

    if (m_debug >= 1) {
      Info ("initialize ()", "Out-of-time pile-up calibration ready.");
    }
  }


  //----------------------------------------------------------------------
  // ZDC calibration tool
  //----------------------------------------------------------------------
  if (IsIons (m_collisionSystem) && (IsCollisions (m_dataType))){// || IsDataOverlay (m_dataType))) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing Zdc analysis tool...");
    }

    m_zdcAnalysisTool = new ZDC::ZdcAnalysisTool ("ZdcAnalysisTool");

    ANA_CHECK (m_zdcAnalysisTool->setProperty ("FlipEMDelay", false)); // false
    ANA_CHECK (m_zdcAnalysisTool->setProperty ("LowGainOnly", false)); // false
    ANA_CHECK (m_zdcAnalysisTool->setProperty ("DoCalib", IsPbPb (m_collisionSystem) || (Is5TeV (m_collisionSystem) && IspPb (m_collisionSystem)))); // true
    if (IsPbPb18 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("initialize ()", "  --> using low-mu 2018 Pb+Pb calibration config");
      }
      ANA_CHECK (m_zdcAnalysisTool->setProperty ("Configuration", "PbPb2018")); // "PbPb2018"
    }
    else if (IspPb16 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("initialize ()", "  --> using low-mu 2016 p+Pb calibration config");
      }
      ANA_CHECK (m_zdcAnalysisTool->setProperty ("Configuration", "pPb2016")); // "pPb2016"
    }
    else if (IsPbPb15 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("initialize ()", "  --> using low-mu 2015 Pb+Pb calibration config");
      }
      ANA_CHECK (m_zdcAnalysisTool->setProperty ("Configuration", "PbPb2015")); // "PbPb2015"
    }
    ANA_CHECK (m_zdcAnalysisTool->setProperty ("AuxSuffix", "_RP")); // "RP"
    ANA_CHECK (m_zdcAnalysisTool->setProperty ("ForceCalibRun", -1)); 

    if (IsPbPb18 (m_collisionSystem) || (Is5TeV (m_collisionSystem) && IspPb16 (m_collisionSystem))) {
      ANA_CHECK (m_zdcAnalysisTool->setProperty ("DoTrigEff", false)); // for now
      ANA_CHECK (m_zdcAnalysisTool->setProperty ("DoTimeCalib", false)); // for now
    }
    ANA_CHECK (m_zdcAnalysisTool->initialize ());

    if (m_debug >= 1) {
      Info ("initialize ()", "Zdc analysis tool ready.");
    }
  }


  //----------------------------------------------------------------------
  // Egamma calibration tool
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Photons) > 0 || m_saveObjects.count (SaveObject::Electrons) > 0) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing egamma calibration and smearing tool...");
    }

    m_egammaCalibrationAndSmearingTool = new CP::EgammaCalibrationAndSmearingTool ("EgammaCalibrationAndSmearingTool");
    if (IsIons (m_collisionSystem) || Ispp15 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("initialize ()", "  --> using low-mu settings for HI data");
      }
      ANA_CHECK (m_egammaCalibrationAndSmearingTool->setProperty ("ESModel", "es2017_R21_ofc0_v1"));
    }
    else if (Ispp17 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("initialize ()", "  --> using high-mu settings for non-HI data");
      }
      ANA_CHECK (m_egammaCalibrationAndSmearingTool->setProperty ("ESModel", "es2018_R21_v0"));
    }
    else {
      Error ("initialize ()", "Failed to recognize collision system for egamma calibration and smearing tool! Quitting.");
      return EL::StatusCode::FAILURE;
    }
    // ANA_CHECK (m_egammaCalibrationAndSmearingTool->setProperty ("isOverlayMC", IsDataOverlay (m_dataType))); // deprecated; from Qipeng's egamma hack
    ANA_CHECK (m_egammaCalibrationAndSmearingTool->setProperty ("decorrelationModel", "1NP_v1")); // 1NP_v1 (2 on scale + 1 on resolution) or FULL_v1 (69 on scale + 10 on resolution)
    ANA_CHECK (m_egammaCalibrationAndSmearingTool->setProperty ("useAFII", 0));
    if (!IsCollisions (m_dataType)) {
      ANA_CHECK (m_egammaCalibrationAndSmearingTool->setProperty ("randomRunNumber", EgammaCalibPeriodRunNumbersExample::run_2016));
    }
    ANA_CHECK (m_egammaCalibrationAndSmearingTool->initialize ());

    if (m_debug >= 1) {
      Info ("initialize ()", "Egamma calibration and smearing tool ready.");
    }
  }


  //----------------------------------------------------------------------
  // Initialize photon recommended systematics list
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Photons) > 0) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing photon recommended CP systematic set...");
    }
    CP::SystematicSet recommendedPhotonSystematics;
    recommendedPhotonSystematics.insert (m_egammaCalibrationAndSmearingTool->recommendedSystematics ());
    for (auto sys : recommendedPhotonSystematics) {
      TString syst_name = TString (sys.name ());
      if (!syst_name.BeginsWith ("PH") && !syst_name.BeginsWith ("EG")) {
        continue;
      }
      if (m_debug >= 2) {
        Info ("initialize ()", Form ("  --> adding photon systematic %s", sys.name ().c_str ()));
      }
      photonSysList.push_back (CP::SystematicSet ({sys}));
    }
    if (m_debug >= 1) {
      Info ("initialize ()", "Photon recommended CP systematic set ready.");
    }
  }


  //----------------------------------------------------------------------
  // Initialize electron recommended systematics list
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Electrons) > 0) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing electron recommended CP systematic set...");
    }
    CP::SystematicSet recommendedElectronSystematics;
    recommendedElectronSystematics.insert (m_egammaCalibrationAndSmearingTool->recommendedSystematics ());
    for (auto sys : recommendedElectronSystematics) {
      TString syst_name = TString (sys.name ());
      if (!syst_name.BeginsWith ("EL") && !syst_name.BeginsWith ("EG")) {
        continue;
      }
      if (m_debug >= 2) {
        Info ("initialize ()", Form ("  --> adding electron systematic %s", sys.name ().c_str ()));
      }
      electronSysList.push_back (CP::SystematicSet ({sys}));
    }
    if (m_debug >= 1) {
      Info ("initialize ()", "Electron recommended CP systematic set ready.");
    }
  }


  //----------------------------------------------------------------------
  // Initialize photon selection tools
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Photons) > 0) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing photon selector tools...");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up LooseIsEM selector tool...");
    }
    m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector  ("PhotonLooseIsEMSelector");
    ANA_CHECK (m_photonLooseIsEMSelector->setProperty ("isEMMask", egammaPID::PhotonLoose));
    ANA_CHECK (m_photonLooseIsEMSelector->setProperty ("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf"));
    ANA_CHECK (m_photonLooseIsEMSelector->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> LooseIsEM selector tool ready.");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up MediumIsEM selector tool...");
    }
    m_photonMediumIsEMSelector = new AsgPhotonIsEMSelector  ("PhotonMediumIsEMSelector");
    ANA_CHECK (m_photonMediumIsEMSelector->setProperty ("isEMMask", egammaPID::PhotonMedium));
    ANA_CHECK (m_photonMediumIsEMSelector->setProperty ("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMMediumSelectorCutDefs.conf"));
    ANA_CHECK (m_photonMediumIsEMSelector->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> MediumIsEM selector tool ready.");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up TightIsEM selector tool...");
    }
    m_photonTightIsEMSelector = new AsgPhotonIsEMSelector  ("PhotonTightIsEMSelector");
    ANA_CHECK (m_photonTightIsEMSelector->setProperty ("isEMMask", egammaPID::PhotonTight));
    ANA_CHECK (m_photonTightIsEMSelector->setProperty ("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMTightSelectorCutDefs.conf"));
    ANA_CHECK (m_photonTightIsEMSelector->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> TightIsEM selector tool ready.");
    }

    if (m_debug >= 1) {
      Info ("initialize ()", "Photon selector tools ready.");
    }
  }


  //----------------------------------------------------------------------
  // Electron selector tools
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Electrons) > 0) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing electron selector tools...");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up LHLoose selector tool...");
    }
    m_electronLHLooseSelectorTool = new AsgElectronLikelihoodTool ("ElectronLHLooseSelectorTool");
    ANA_CHECK (m_electronLHLooseSelectorTool->setProperty ("WorkingPoint", "LooseLHElectron"));
    ANA_CHECK (m_electronLHLooseSelectorTool->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> LHLoose selector tool ready.");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up LHMedium selector tool...");
    }
    m_electronLHMediumSelectorTool = new AsgElectronLikelihoodTool ("ElectronLHMediumSelectorTool");
    ANA_CHECK (m_electronLHMediumSelectorTool->setProperty ("WorkingPoint", "MediumLHElectron"));
    ANA_CHECK (m_electronLHMediumSelectorTool->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> LHMedium selector tool ready.");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up LHTight selector tool...");
    }
    m_electronLHTightSelectorTool = new AsgElectronLikelihoodTool ("ElectronLHTightSelectorTool");
    ANA_CHECK (m_electronLHTightSelectorTool->setProperty ("WorkingPoint", "TightLHElectron"));
    ANA_CHECK (m_electronLHTightSelectorTool->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> LHTight selector tool ready.");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up LHLooseHI selector tool...");
    }
    m_electronLHLooseHISelectorTool = new AsgElectronLikelihoodTool ("ElectronLHLooseHISelectorTool");
    ANA_CHECK (m_electronLHLooseHISelectorTool->setProperty ("ConfigFile", "ElectronPhotonSelectorTools/offline/mc15_20160907_HI/ElectronLikelihoodLooseOfflineConfig2016_HI.conf"));
    ANA_CHECK (m_electronLHLooseHISelectorTool->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> LHLooseHI selector tool ready.");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up LHMediumHI selector tool...");
    }
    m_electronLHMediumHISelectorTool = new AsgElectronLikelihoodTool ("ElectronLHMediumHISelectorTool");
    ANA_CHECK (m_electronLHMediumHISelectorTool->setProperty ("ConfigFile", "ElectronPhotonSelectorTools/offline/mc15_20160907_HI/ElectronLikelihoodMediumOfflineConfig2016_HI.conf"));
    ANA_CHECK (m_electronLHMediumHISelectorTool->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> LHMediumHI selector tool ready.");
    }

    if (m_debug >= 1) {
      Info ("initialize ()", "Electron selector tools ready.");
    }
  }


  //----------------------------------------------------------------------
  // Initialize muon corrector tool
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Muons) > 0) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing muon calibration and smearing tool...");
    }

    m_muonCalibrationAndSmearingTool = new CP::MuonCalibrationAndSmearingTool ("MuonCalibrationAndSmearingTool");
    if (Is2018 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("initialize ()", "  --> setting config to Data18, Recs2019_05_30, sagittaBiasDataAll_03_02_19_Data18");
      }
      ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("Year", "Data18"));
      ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("Release", "Recs2019_05_30"));
      ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("SagittaRelease", "sagittaBiasDataAll_03_02_19_Data18"));
    }
    else if (Is2017 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("initialize ()", "  --> setting config to Data17, Recs2019_05_30, sagittaBiasDataAll_03_02_19_Data17");
      }
      ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("Year", "Data17"));
      ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("Release", "Recs2019_05_30"));
      ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("SagittaRelease", "sagittaBiasDataAll_03_02_19_Data17"));
    }
    else if (Is2016 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("initialize ()", "  --> setting config to Data16, Recs2019_05_30, sagittaBiasDataAll_03_02_19_Data16");
      }
      ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("Year", "Data16"));
      ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("Release", "Recs2019_05_30"));
      ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("SagittaRelease", "sagittaBiasDataAll_03_02_19_Data16"));
    }
    else if (Is2015 (m_collisionSystem)) {
      if (m_debug >= 2) {
        Info ("initialize ()", "  --> setting config to Data15, Recs2019_05_30, sagittaBiasDataAll_03_02_19_Data16");
      }
      ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("Year", "Data15"));
      ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("Release", "Recs2019_05_30"));
      ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("SagittaRelease", "sagittaBiasDataAll_03_02_19_Data16"));
    }
    ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("StatComb", false));
    ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("SagittaCorr", true));
    ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("doSagittaMCDistortion", false));
    ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("SagittaCorrPhaseSpace", true));
    ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("isOverlayMC", IsDataOverlay (m_dataType)));
    //ANA_CHECK (m_muonCalibrationAndSmearingTool->setProperty ("OutputLevel", MSG::VERBOSE));
    ANA_CHECK (m_muonCalibrationAndSmearingTool->initialize ());

    if (m_debug >= 1) {
      Info ("initialize ()", "Muon calibration and smearing tool ready.");
    }
  }


  //----------------------------------------------------------------------
  // Initialize muon recommended systematics list
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Muons) > 0) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing muon recommended CP systematic set...");
    }
    CP::SystematicSet recommendedMuonSystematics;
    recommendedMuonSystematics.insert (m_muonCalibrationAndSmearingTool->recommendedSystematics ());
    for (auto sys : recommendedMuonSystematics) {
      if (m_debug >= 2) {
        Info ("initialize ()", Form ("  --> adding muon systematic %s", sys.name ().c_str ()));
      }
      muonSysList.push_back (CP::SystematicSet ({sys}));
    }
    if (m_debug >= 1) {
      Info ("initialize ()", "Muon recommended CP systematic set ready.");
    }
  }


  //----------------------------------------------------------------------
  // Muon selector tools
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Muons) > 0) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing muon selection tools...");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up Loose selection tool...");
    }
    m_muonLooseSelectorTool = new CP::MuonSelectionTool ("MuonLooseSelection");
    ANA_CHECK (m_muonLooseSelectorTool->setProperty ("MaxEta", 2.5));
    ANA_CHECK (m_muonLooseSelectorTool->setProperty ("MuQuality", 2));
    if (IsPbPb (m_collisionSystem)) {
      ANA_CHECK (m_muonLooseSelectorTool->setProperty ("TrtCutOff", true));
    }
    ANA_CHECK (m_muonLooseSelectorTool->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> Loose selection tool ready.");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up Medium selection tool...");
    }
    m_muonMediumSelectorTool = new CP::MuonSelectionTool ("MuonMediumSelection");
    ANA_CHECK (m_muonMediumSelectorTool->setProperty ("MaxEta", 2.5));
    ANA_CHECK (m_muonMediumSelectorTool->setProperty ("MuQuality", 1));
    if (IsPbPb (m_collisionSystem)) {
      ANA_CHECK (m_muonMediumSelectorTool->setProperty ("TrtCutOff", true));
    }
    ANA_CHECK (m_muonMediumSelectorTool->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> Medium selection tool ready.");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up Tight selection tool...");
    }
    m_muonTightSelectorTool = new CP::MuonSelectionTool ("MuonTightSelection");
    ANA_CHECK (m_muonTightSelectorTool->setProperty ("MaxEta", 2.5));
    ANA_CHECK (m_muonTightSelectorTool->setProperty ("MuQuality", 0));
    if (IsPbPb (m_collisionSystem)) {
      ANA_CHECK (m_muonTightSelectorTool->setProperty ("TrtCutOff", true));
    }
    ANA_CHECK (m_muonTightSelectorTool->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> Tight selection tool ready.");
    }

    if (m_debug >= 1) {
      Info ("initialize ()", "Muon selection tools ready.");
    }
  }

  
  //----------------------------------------------------------------------
  // Track selection tools
  // Used for offline selection and sum pT, OOTPU calculations
  //----------------------------------------------------------------------
  {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing track selection tools...");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up Loose selection tool...");
    }
    m_trackSelectionToolLoose = new InDet::InDetTrackSelectionTool ("TrackSelectionToolLoose");
    ANA_CHECK (m_trackSelectionToolLoose->setProperty ("CutLevel", "Loose"));
    ANA_CHECK (m_trackSelectionToolLoose->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> Loose selection tool ready.");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up LoosePrimary selection tool...");
    }
    m_trackSelectionToolLoosePrimary = new InDet::InDetTrackSelectionTool ("TrackSelectionToolLoosePrimary");
    ANA_CHECK (m_trackSelectionToolLoosePrimary->setProperty ("CutLevel", "LoosePrimary"));
    ANA_CHECK (m_trackSelectionToolLoosePrimary->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> LoosePrimary selection tool ready.");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up TightPrimary selection tool...");
    }
    m_trackSelectionToolTightPrimary = new InDet::InDetTrackSelectionTool ("TrackSelectionToolTightPrimary");
    ANA_CHECK (m_trackSelectionToolTightPrimary->setProperty ("CutLevel", "TightPrimary"));
    ANA_CHECK (m_trackSelectionToolTightPrimary->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> TightPrimary selection tool ready.");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up HITight selection tool...");
    }
    m_trackSelectionToolHITight = new InDet::InDetTrackSelectionTool ("TrackSelectionToolHITight");
    ANA_CHECK (m_trackSelectionToolHITight->setProperty ("CutLevel", "HITight"));
    ANA_CHECK (m_trackSelectionToolHITight->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> HITight selection tool ready.");
    }

    if (m_debug >= 2) {
      Info ("initialize ()", "  --> setting up HILoose selection tool...");
    }
    m_trackSelectionToolHILoose = new InDet::InDetTrackSelectionTool ("TrackSelectionToolHILoose");
    ANA_CHECK (m_trackSelectionToolHILoose->setProperty ("CutLevel", "HILoose"));
    ANA_CHECK (m_trackSelectionToolHILoose->initialize ());
    if (m_debug >= 2) {
      Info ("initialize ()", "  --> HILoose selection tool ready.");
    }

    if (m_debug >= 1) {
      Info ("initialize ()", "Track selection tools ready.");
    }
  }


  //----------------------------------------------------------------------
  // Initialize MC truth tool
  //----------------------------------------------------------------------
  if ((m_saveObjects.count (SaveObject::Tracks) > 0 || m_saveObjects.count (SaveObject::Electrons) > 0 || m_saveObjects.count (SaveObject::Muons) > 0) && !IsCollisions (m_dataType)) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing MC truth classifier tool...");
    }

    m_mcTruthClassifier = new MCTruthClassifier ("MCTruthClassifier");
    ANA_CHECK (m_mcTruthClassifier->initialize ());

    if (m_debug >= 1) {
      Info ("initialize ()", "MC truth classifier tool ready.");
    }
  }


  //----------------------------------------------------------------------
  // Initialize jet cleaning tool
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Jets) > 0) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing jet cleaning tools...");
    }

    m_jetCleaningTool = new JetCleaningTool ("JetCleaningTool");
    ANA_CHECK (m_jetCleaningTool->setProperty ("CutLevel","LooseBad"));
    ANA_CHECK (m_jetCleaningTool->setProperty ("DoUgly", false));
    ANA_CHECK (m_jetCleaningTool->initialize ());

    if (m_debug >= 1) {
      Info ("initialize ()", "Jet cleaning tools ready.");
    }
  }


  //----------------------------------------------------------------------
  // Initialize jet calibration tools
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Jets) > 0) {
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing jet calibration tools...");
    }

    std::string m_configFile;
    if (Is5TeV (m_collisionSystem) && IspPb (m_collisionSystem) && IsPeriodA (m_collisionSystem)) {
      m_configFile = "JES_MC16_HI_Jul2021_pPb_5TeV.config";
    }
    else if (Is5TeV (m_collisionSystem) && !IspPb (m_collisionSystem)) {
      m_configFile = "JES_MC16_HI_Jan2021_5TeV.config";
    }
    else if (Is8TeV (m_collisionSystem) && IspPb (m_collisionSystem)) {
      if (IsPeriodA (m_collisionSystem)) {
        m_configFile = "JES_MC16_HI_Jul2021_pPb_8TeV.config";
        //m_configFile = "JES_MC15c_HI_Oct2018_pPb.config"; // old R20.7 file
      }
      else {
        m_configFile = "JES_MC16_HI_Jul2021_Pbp_8TeV.config";
        //m_configFile = "JES_MC15c_HI_Oct2018_Pbp.config"; // old R20.7 file
      }
    }
    else {
      Error ("initialize ()", "Failed to recognize jet calibration sequence for data set. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    if (m_debug >= 1) {
      Info ("initialize ()", Form ("  --> jet calibration config file set to %s", m_configFile.c_str ()));
    }


    // R=0.2
    {
      if (m_debug >= 2) {
        Info ("initialize ()", "Initializing Anti-kT HI R=0.2 jet calibration tools...");
      }

      // EtaJES JetCalibTool
      if (m_debug >= 2) {
        Info ("initialize ()", "  --> setting up EtaJES correction tool...");
      }

      m_Akt2HI_EtaJES_CalibTool = new JetCalibrationTool ("Akt2HI_EtaJES_CalibTool");
      ANA_CHECK (m_Akt2HI_EtaJES_CalibTool->setProperty ("JetCollection", "AntiKt2HI"));
      ANA_CHECK (m_Akt2HI_EtaJES_CalibTool->setProperty ("ConfigFile", m_configFile.c_str ()));
      ANA_CHECK (m_Akt2HI_EtaJES_CalibTool->setProperty ("DEVmode", true));
      ANA_CHECK (m_Akt2HI_EtaJES_CalibTool->setProperty ("CalibSequence", "EtaJES"));
      ANA_CHECK (m_Akt2HI_EtaJES_CalibTool->setProperty ("IsData", IsCollisions (m_dataType)));
      m_Akt2HI_EtaJES_CalibTool->initializeTool ("Akt2HI_EtaJES_CalibTool");

      if (m_debug >= 2) {
        Info ("initialize ()", "  --> EtaJES correction tool ready.");
      }

      // xCalib/In situ JetCalibTool
      if (IsCollisions (m_dataType) || IsDataOverlay (m_dataType)) {
        if (m_debug >= 2) {
          Info ("initialize ()", "  --> setting up in situ / cross-calibration tool...");
        }

        m_Akt2HI_EtaJES_Insitu_CalibTool = new JetCalibrationTool ("Akt2HI_EtaJES_Insitu_CalibTool");
        ANA_CHECK (m_Akt2HI_EtaJES_Insitu_CalibTool->setProperty ("JetCollection", "AntiKt2HI"));
        ANA_CHECK (m_Akt2HI_EtaJES_Insitu_CalibTool->setProperty ("ConfigFile", m_configFile.c_str ()));
        ANA_CHECK (m_Akt2HI_EtaJES_Insitu_CalibTool->setProperty ("DEVmode", true));
        ANA_CHECK (m_Akt2HI_EtaJES_Insitu_CalibTool->setProperty ("CalibSequence", "EtaJES_Insitu"));
        ANA_CHECK (m_Akt2HI_EtaJES_Insitu_CalibTool->setProperty ("IsData", IsCollisions (m_dataType) || IsDataOverlay (m_dataType)));
        m_Akt2HI_EtaJES_Insitu_CalibTool->initializeTool ("Akt2HI_EtaJES_Insitu_CalibTool");

        if (m_debug >= 2) {
          Info ("initialize ()", "  --> in situ / cross-calibration tool ready.");
        }
      }

      if (m_debug >= 2) {
        Info ("initialize ()", "Anti-kT HI R=0.2 jet calibration tools ready.");
      }
    }

    // R=0.4
    {
      if (m_debug >= 2) {
        Info ("initialize ()", "Initializing Anti-kT HI R=0.4 jet calibration tools...");
      }

      // EtaJES JetCalibTool
      if (m_debug >= 2) {
        Info ("initialize ()", "  --> setting up EtaJES correction tool...");
      }

      m_Akt4HI_EtaJES_CalibTool = new JetCalibrationTool ("Akt4HI_EtaJES_CalibTool");
      ANA_CHECK (m_Akt4HI_EtaJES_CalibTool->setProperty ("JetCollection", "AntiKt4HI"));
      ANA_CHECK (m_Akt4HI_EtaJES_CalibTool->setProperty ("ConfigFile", m_configFile.c_str ()));
      ANA_CHECK (m_Akt4HI_EtaJES_CalibTool->setProperty ("DEVmode", true));
      ANA_CHECK (m_Akt4HI_EtaJES_CalibTool->setProperty ("CalibSequence", "EtaJES"));
      ANA_CHECK (m_Akt4HI_EtaJES_CalibTool->setProperty ("IsData", IsCollisions (m_dataType)));
      m_Akt4HI_EtaJES_CalibTool->initializeTool ("Akt4HI_EtaJES_CalibTool");

      if (m_debug >= 2) {
        Info ("initialize ()", "  --> EtaJES correction tool ready.");
      }

      // xCalib/In situ JetCalibTool
      if (IsCollisions (m_dataType) || IsDataOverlay (m_dataType)) {
        if (m_debug >= 2) {
          Info ("initialize ()", "  --> setting up in situ / cross-calibration tool...");
        }

        m_Akt4HI_EtaJES_Insitu_CalibTool = new JetCalibrationTool ("Akt4HI_EtaJES_Insitu_CalibTool");
        ANA_CHECK (m_Akt4HI_EtaJES_Insitu_CalibTool->setProperty ("JetCollection", "AntiKt4HI"));
        ANA_CHECK (m_Akt4HI_EtaJES_Insitu_CalibTool->setProperty ("ConfigFile", m_configFile.c_str ()));
        ANA_CHECK (m_Akt4HI_EtaJES_Insitu_CalibTool->setProperty ("DEVmode", true));
        ANA_CHECK (m_Akt4HI_EtaJES_Insitu_CalibTool->setProperty ("CalibSequence", "EtaJES_Insitu"));
        ANA_CHECK (m_Akt4HI_EtaJES_Insitu_CalibTool->setProperty ("IsData", IsCollisions (m_dataType) || IsDataOverlay (m_dataType)));
        m_Akt4HI_EtaJES_Insitu_CalibTool->initializeTool ("Akt4HI_EtaJES_Insitu_CalibTool");

        if (m_debug >= 2) {
          Info ("initialize ()", "  --> in situ / cross-calibration tool ready.");
        }
      }

      if (m_debug >= 2) {
        Info ("initialize ()", "Anti-kT HI R=0.4 jet calibration tools ready.");
      }
    }

    if (m_debug >= 1) {
      Info ("initialize ()", "Jet calibration tools ready.");
    }
  }



  //----------------------------------------------------------------------
  // Initialize jet systematic uncertainty tools
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Jets) > 0) {
    //if (!IsCollisions (m_dataType)){
    if (m_debug >= 1) {
      Info ("initialize ()", "Initializing jet systematic uncertainty tools...");
    }

    if (Is5TeV (m_collisionSystem)) {
      m_configFile_uncer02 = (std::string) gSystem->ExpandPathName (Form ("%s/HIJetUncertainties/HIR2_GlobalReduction_SimpleJER.config", filePath.c_str ()));
      m_configFile_uncer04 = (std::string) gSystem->ExpandPathName (Form ("%s/HIJetUncertainties/HIR4_GlobalReduction_SimpleJER.config", filePath.c_str ()));
      m_CC_fname = (std::string) gSystem->ExpandPathName (Form ("%s/HIJetUncertainties/CrossCalibUncertainty.root", filePath.c_str ()));
      if (IspPb16 (m_collisionSystem) && Is5TeV (m_collisionSystem)) {
        m_FU_02_fname = (std::string) gSystem->ExpandPathName (Form ("%s/HIJetUncertainties/FlavorJESUncertainty_R0p2_pPb.root", filePath.c_str ()));
        m_FU_04_fname = (std::string) gSystem->ExpandPathName (Form ("%s/HIJetUncertainties/FlavorJESUncertainty_R0p4_pPb.root", filePath.c_str ()));
      }
      else {
        m_FU_02_fname = (std::string) gSystem->ExpandPathName (Form ("%s/HIJetUncertainties/FlavorJESUncertainty_R0p2.root", filePath.c_str ()));
        m_FU_04_fname = (std::string) gSystem->ExpandPathName (Form ("%s/HIJetUncertainties/FlavorJESUncertainty_R0p4.root", filePath.c_str ()));
      }
      m_RS_fname = (std::string) gSystem->ExpandPathName (Form ("%s/HIJetUncertainties/additional_jesjeruc_0p4.root", filePath.c_str ()));
    }
    else {
      Error ("initialize ()", "Failed to recognize jet uncertainty configuration file. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    {
      if (m_debug >= 2) {
        Info ("initialize ()", "--> Setting up anti-kT HI R=0.2 jet uncertainty tool...");
      }

      m_Akt2HI_UncertTool = new JetUncertaintiesTool ("Akt2HI_UncertTool");
      ANA_CHECK (m_Akt2HI_UncertTool->setProperty ("JetDefinition", "AntiKt2HI"));
      ANA_CHECK (m_Akt2HI_UncertTool->setProperty ("MCType", "MC16"));
      ANA_CHECK (m_Akt2HI_UncertTool->setProperty ("ConfigFile", m_configFile_uncer02.c_str ()));
      ANA_CHECK (m_Akt2HI_UncertTool->setProperty ("IsData", IsCollisions (m_dataType)));
      ANA_CHECK (m_Akt2HI_UncertTool->initialize ());

      if (m_debug >= 2) {
        Info ("initialize ()", "--> Anti-kT HI R=0.2 jet uncertainty tool ready.");
      }
    }

    {
      if (m_debug >= 2) {
        Info ("initialize ()", "--> Setting up anti-kT HI R=0.4 jet uncertainty tool...");
      }
      
      m_Akt4HI_UncertTool = new JetUncertaintiesTool ("Akt4HI_UncertTool");
      ANA_CHECK (m_Akt4HI_UncertTool->setProperty ("JetDefinition", "AntiKt4HI"));
      ANA_CHECK (m_Akt4HI_UncertTool->setProperty ("MCType", "MC16"));
      ANA_CHECK (m_Akt4HI_UncertTool->setProperty ("ConfigFile", m_configFile_uncer04.c_str ()));
      ANA_CHECK (m_Akt4HI_UncertTool->setProperty ("IsData", IsCollisions (m_dataType)));
      ANA_CHECK (m_Akt4HI_UncertTool->initialize ());


      for (int iComp = 0; iComp <= 16; iComp++) {
        Info ("initialize ()", Form ("--> Anti-kT HI R=0.4 jet uncertainty tool component %i name: %s", iComp, m_Akt4HI_UncertTool->getComponentName (iComp).c_str ()));
      }

      if (m_debug >= 2) {
        Info ("initialize ()", "--> Anti-kT HI R=0.4 jet uncertainty tool ready.");
      }
    }

    {
      if (m_debug >= 2) {
        Info ("initialize ()", "--> Setting up cross-calibration uncertainty tools...");
      }

      m_rnd = new TRandom3 ();
      m_rnd->SetSeed (07062014);
      std::unique_ptr <TFile> f_CC_In (TFile::Open (PathResolverFindCalibFile (m_CC_fname).c_str (), "READ"));
      m_h204_JER = (TH2D*) ((TH2D*) f_CC_In->Get ("JER_Nominal_MC16_AntiKt4HI"))->Clone ("h204_JER"); m_h204_JER->SetDirectory (0);
      m_h2CC_JES = (TH2D*) ((TH2D*) f_CC_In->Get ("h2_uncertainty_CC_JES"))->Clone ("h2_CC_JES"); m_h2CC_JES->SetDirectory (0);
      m_h2CC_JER = (TH2D*) ((TH2D*) f_CC_In->Get ("h2_uncertainty_CC_JER"))->Clone ("h2_CC_JER"); m_h2CC_JER->SetDirectory (0);

      if (m_debug >= 2) {
        Info ("initialize ()", "--> Cross-calibration uncertainty tools ready.");
      }
    }

    {
      if (m_debug >= 2) {
        Info ("initialize ()", "--> Setting up flavour uncertainty tools...");
      }

      // Flavour uncertainty for R=0.2 jets
      std::unique_ptr <TFile> f_FU_02_In (TFile::Open (PathResolverFindCalibFile (m_FU_02_fname).c_str (), "READ"));

      // Flavour uncertainty for R=0.4 jets
      std::unique_ptr <TFile> f_FU_04_In (TFile::Open (PathResolverFindCalibFile (m_FU_04_fname).c_str (), "READ"));

      // bootstrapped p+Pb flavour uncertainty accounting for boost of -0.465
      if (IspPb16 (m_collisionSystem)) {
        m_f_res_02 = (TH2D*) ((TH2D*) f_FU_02_In->Get ("term1"))->Clone ("m_f_res_02"); m_f_res_02->SetDirectory (0);
        m_f_fra_02 = (TH2D*) ((TH2D*) f_FU_02_In->Get ("term2"))->Clone ("m_f_fra_02"); m_f_fra_02->SetDirectory (0);

        m_f_res_04 = (TH2D*) ((TH2D*) f_FU_04_In->Get ("term1"))->Clone ("m_f_res_04"); m_f_res_04->SetDirectory (0);
        m_f_fra_04 = (TH2D*) ((TH2D*) f_FU_04_In->Get ("term2"))->Clone ("m_f_fra_04"); m_f_fra_04->SetDirectory (0);
      }
      // otherwise use default flavour uncertainty
      else {
        m_f_res_02 = (TH2D*) ((TH2D*) f_FU_02_In->Get ("termAbs1"))->Clone ("m_f_res_02"); m_f_res_02->SetDirectory (0);
        m_f_fra_02 = (TH2D*) ((TH2D*) f_FU_02_In->Get ("termAbs2"))->Clone ("m_f_fra_02"); m_f_fra_02->SetDirectory (0);

        m_f_res_04 = (TH2D*) ((TH2D*) f_FU_04_In->Get ("termAbs1"))->Clone ("m_f_res_04"); m_f_res_04->SetDirectory (0);
        m_f_fra_04 = (TH2D*) ((TH2D*) f_FU_04_In->Get ("termAbs2"))->Clone ("m_f_fra_04"); m_f_fra_04->SetDirectory (0);
      }

      if (m_debug >= 2) {
        Info ("initialize ()", "--> Flavour uncertainty tools ready.");
      }
    }

    {
      if (m_debug >= 2) {
        Info ("initialize ()", "--> Setting up R-scan uncertainty tools...");
      }

      std::unique_ptr <TFile> f_RS_In (TFile::Open (PathResolverFindCalibFile (m_RS_fname).c_str (), "READ"));
      m_pt_mapping = (TF1*) ((TF1*) f_RS_In->Get ("p2_to_0p4mapping"))->Clone ("pt_mapping");
      m_RS_JES = (TF1*) ((TF1*) f_RS_In->Get ("r0p2_jes"))->Clone ("RS_JES");
      m_RS_JER = (TF1*) ((TF1*) f_RS_In->Get ("f_jer_smearing_r0p2"))->Clone ("RS_JER");

      if (m_debug >= 2) {
        Info ("initialize ()", "--> R-scan uncertainty tools ready.");
      }
    }

    if (m_debug >= 1) {
      Info ("initialize ()", "Jet systematic uncertainty tools ready.");
    }
    //}
    else if (m_debug >= 1) {
      Info ("initialize ()", "Running over data, don't need to set up any systematic uncertainty tools.");
    }
  }


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: execute () {
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // set type of return code you are expecting
  // (add to top of each function once)

  ANA_CHECK_SET_TYPE (EL::StatusCode);

  if (m_debug >= 1) {
    Info ("execute ()", "Now processing next event...");
  }

  //----------------------------------------------------------------------
  // Event information needed throughout
  //----------------------------------------------------------------------
  const xAOD::EventInfo* eventInfo = 0;
  if (!evtStore ()->retrieve (eventInfo, "EventInfo").isSuccess ()) {
    Error ("GetEventInfo ()", "Failed to retrieve EventInfo container. Exiting.");
    return EL::StatusCode::FAILURE;
  }
  else if (m_debug >= 2) {
    Info ("GetEventInfo ()", "Successfully retrieved EventInfo.");
  }


  const xAOD::Vertex* priVtx = nullptr;
  std::vector <float> vec_trk_eta;
  vec_trk_eta.clear ();
  std::vector <const xAOD::IParticle*> triggerParticles;
  triggerParticles.clear ();



  //----------------------------------------------------------------------
  // Store event-level information and apply GRL cut
  //----------------------------------------------------------------------
  m_b_runNum = eventInfo->runNumber ();
  m_b_lbn = eventInfo->lumiBlock ();
  m_b_evtNum = eventInfo->eventNumber ();
  m_b_actualInteractionsPerCrossing = eventInfo->actualInteractionsPerCrossing ();
  m_b_averageInteractionsPerCrossing = eventInfo->averageInteractionsPerCrossing ();
  m_b_BlayerDesyn = isDesynEvent (eventInfo->runNumber (), eventInfo->lumiBlock ());

  if (m_debug >= 3) {
    Info ("CheckEventStatus ()", Form ("Gathered basic info: (runNum, lumiBlock, evtNum) = (%i, %i, %i)", m_b_runNum, m_b_lbn, m_b_evtNum));
  }
 
  // if data or MC+data overlay, check if event passes GRL. Then check if event passes event cleaning
  if (IsCollisions (m_dataType) || IsDataOverlay (m_dataType)) {
    if (m_debug >= 1) {
      Info ("CheckEventStatus ()", "Checking GRLs and event cleaning criteria...");
    }

    // First check against nominal GRL
    if (!(m_goodRunsList->passRunLB (*eventInfo))) {
      if (m_debug >= 2) {
        Info ("CheckGRL ()", "Event did not pass GRL, continuing to the next event.");
      }
      return EL::StatusCode::SUCCESS;
    }

    // Now check against GRL ignoring toroid status (if applicable)
    if (m_goodRunsList_ignoreToroid != nullptr && !(m_goodRunsList_ignoreToroid->passRunLB (*eventInfo))) {
      m_b_passesToroid = false;
    }
    else if (m_debug >= 2) {
      Info ("CheckGRL ()", "Event did not pass GRL ignoring toroid status.");
    }

    // Now check if events passes event cleaning
    if (!IsDataOverlay (m_dataType) || !IspPb (m_collisionSystem) || !Is8TeV (m_collisionSystem)) {
      if (eventInfo->errorState (xAOD::EventInfo::LAr) == xAOD::EventInfo::Error ||  
          eventInfo->errorState (xAOD::EventInfo::Tile) == xAOD::EventInfo::Error ||
          eventInfo->errorState (xAOD::EventInfo::SCT) == xAOD::EventInfo::Error ||
          eventInfo->isEventFlagBitSet (xAOD::EventInfo::Core, 18)) {
        if (m_debug >= 3) {
          if (eventInfo->errorState (xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) {
            Info ("CheckEventStatus ()", "  --> eventInfo->errorState (xAOD::EventInfo::LAr) == xAOD::EventInfo::Error");
          }
          if (eventInfo->errorState (xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) {
            Info ("CheckEventStatus ()", "  --> eventInfo->errorState (xAOD::EventInfo::Tile) == xAOD::EventInfo::Error");
          }
          if (eventInfo->errorState (xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) {
            Info ("CheckEventStatus ()", "  --> eventInfo->errorState (xAOD::EventInfo::SCT) == xAOD::EventInfo::Error");
          }
          if (eventInfo->isEventFlagBitSet (xAOD::EventInfo::Core, 18)) {
            Info ("CheckEventStatus ()", "  --> eventInfo->isEventFlagBitSet (xAOD::EventInfo::Core, 18)");
          }
        }
        if (m_debug >= 2) Info ("CheckEventStatus ()", "Event did not pass event cleaning check, continuing to the next event.");
        return EL::StatusCode::SUCCESS;
      }
    }

    if (m_debug >= 1) {
      Info ("CheckEventStatus ()", "Finished checking GRLs and event cleaning criteria.");
    }
  } // end GRL scope


  
  //----------------------------------------------------------------------
  // Truth event information
  //----------------------------------------------------------------------
  if (!IsCollisions (m_dataType)) {
    if (m_debug >= 1) {
      Info ("GetTruthEventInfo ()", "Retrieving MC event weights...");
    }
    m_b_mcEventWeights = eventInfo->mcEventWeights ();
    if (m_debug >= 1) {
      Info ("GetTruthEventInfo ()", "Finished retrieving MC event weights.");
    }
  }

  if (IsHijing (m_dataType)) {
    if (m_debug >= 1) {
      Info ("GetTruthEventInfo ()", "Retrieving truth events...");
    }

    const xAOD::TruthEventContainer* truthEvents = 0;
    if (!evtStore ()->retrieve (truthEvents, "TruthEvents").isSuccess ()) {
      Error ("GetTruthEventInfo ()", "Failed to retrieve TruthEvents container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetTruthEvents ()", "Successfully retrieved TruthEvents container.");
    }

    m_b_truth_event_n = 0;
    for (const auto* truthEvent : *truthEvents) {
      if (m_b_truth_event_n >= m_max_truth_event_n) {
        Error ("GetTruthEventInfo ()", "Tried to overflow truth event arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetTruthEventInfo ()", Form ("Checked truth event array length, found %i truth events so far.", m_b_truth_event_n));
      }
      truthEvent->heavyIonParameter (m_b_nPart1[m_b_truth_event_n], xAOD::TruthEvent::HIParam::NPARTPROJ);
      truthEvent->heavyIonParameter (m_b_nPart2[m_b_truth_event_n], xAOD::TruthEvent::HIParam::NPARTTARG);
      truthEvent->heavyIonParameter (m_b_impactParameter[m_b_truth_event_n], xAOD::TruthEvent::HIParam::IMPACTPARAMETER);
      truthEvent->heavyIonParameter (m_b_nColl[m_b_truth_event_n], xAOD::TruthEvent::HIParam::NCOLL);
      truthEvent->heavyIonParameter (m_b_nSpectatorNeutrons[m_b_truth_event_n], xAOD::TruthEvent::HIParam::SPECTATORNEUTRONS);
      truthEvent->heavyIonParameter (m_b_nSpectatorProtons[m_b_truth_event_n], xAOD::TruthEvent::HIParam::SPECTATORPROTONS);
      truthEvent->heavyIonParameter (m_b_eccentricity[m_b_truth_event_n], xAOD::TruthEvent::HIParam::ECCENTRICITY);
      truthEvent->heavyIonParameter (m_b_eventPlaneAngle[m_b_truth_event_n], xAOD::TruthEvent::HIParam::EVENTPLANEANGLE);
      m_b_truth_event_n++;
    }
    if (m_debug >= 1) {
      Info ("GetTruthEventInfo ()", "Finished retrieving truth events.");
    }
  } // end truth events scope



  //----------------------------------------------------------------------
  // Event trigger info
  //----------------------------------------------------------------------
  if (m_triggerSet.size () > 0 && m_triggerSet.count (EventTrigger::None) == 0) {
    if (m_debug >= 1) {
      Info ("GetTriggers ()", "Checking triggers...");
    }

    bool minbiasDidFire = false;
    bool photonDidFire = false;
    bool electronDidFire = false;
    bool muonDidFire = false;
    bool jetDidFire = false;
    bool zdcDidFire = false;

    // Gather minbias triggers
    if (m_triggerSet.count (EventTrigger::MinBias) > 0) {
      for (int iTrigger = 0; iTrigger < m_minbias_trig_n; iTrigger++) {
        auto trigger = m_trigDecisionTool->getChainGroup (m_minbias_trig_name[iTrigger].c_str ());
        m_b_minbias_trig_decision[iTrigger] = trigger->isPassed ();
        m_b_minbias_trig_prescale[iTrigger] = trigger->getPrescale ();

        minbiasDidFire = minbiasDidFire || trigger->isPassed ();
      } // end loop over iTrigger
    }

    // Gather photon triggers
    if (m_triggerSet.count (EventTrigger::Photon) > 0) {
      for (int iTrigger = 0; iTrigger < m_photon_trig_n; iTrigger++) {
        auto trigger = m_trigDecisionTool->getChainGroup (m_photon_trig_name[iTrigger].c_str ());
        m_b_photon_trig_decision[iTrigger] = trigger->isPassed ();
        m_b_photon_trig_prescale[iTrigger] = trigger->getPrescale ();

        photonDidFire = photonDidFire || trigger->isPassed ();
      } // end loop over iTrigger
    }

    // Gather electron triggers
    if (m_triggerSet.count (EventTrigger::Electron) > 0) {
      for (int iTrigger = 0; iTrigger < m_electron_trig_n; iTrigger++) {
        auto trigger = m_trigDecisionTool->getChainGroup (m_electron_trig_name[iTrigger].c_str ());
        m_b_electron_trig_decision[iTrigger] = trigger->isPassed ();
        m_b_electron_trig_prescale[iTrigger] = trigger->getPrescale ();

        electronDidFire = electronDidFire || trigger->isPassed ();
      } // end loop over iTrigger
    }

    // Gather muon triggers
    if (m_triggerSet.count (EventTrigger::Muon) > 0) {
      for (int iTrigger = 0; iTrigger < m_muon_trig_n; iTrigger++) {
        auto trigger = m_trigDecisionTool->getChainGroup (m_muon_trig_name[iTrigger].c_str ());
        m_b_muon_trig_decision[iTrigger] = trigger->isPassed ();
        m_b_muon_trig_prescale[iTrigger] = trigger->getPrescale ();

        muonDidFire = muonDidFire || trigger->isPassed ();
      } // end loop over iTrigger
    }

    // Gather jet triggers
    if (m_triggerSet.count (EventTrigger::Jet) > 0) {
      for (int iTrigger = 0; iTrigger < m_jet_trig_n; iTrigger++) {
        auto trigger = m_trigDecisionTool->getChainGroup (m_jet_trig_name[iTrigger].c_str ());
        m_b_jet_trig_decision[iTrigger] = trigger->isPassed ();
        m_b_jet_trig_prescale[iTrigger] = trigger->getPrescale ();

        jetDidFire = jetDidFire || trigger->isPassed ();
      } // end loop over iTrigger
    }

    // Gather ZDC L1 triggers
    if (m_triggerSet.count (EventTrigger::ZDC) > 0) {
      // Zdc block with online/offline info
      // online L1 ZDC trigger information
      for (int iTrigger = 0; iTrigger < m_zdc_L1_trig_n; iTrigger++) {
        const unsigned int bits = m_trigDecisionTool->isPassedBits (m_zdc_L1_trig_name[iTrigger].c_str ());
        auto trigger = m_trigDecisionTool->getChainGroup (m_zdc_L1_trig_name[iTrigger].c_str ());
        m_b_zdc_L1_trig_decision[iTrigger] = trigger->isPassed ();
        m_b_zdc_L1_trig_prescale[iTrigger] = trigger->getPrescale ();
        m_b_zdc_L1_trig_tbp[iTrigger]      = bits & TrigDefs::L1_isPassedBeforePrescale;
        m_b_zdc_L1_trig_tap[iTrigger]      = bits & TrigDefs::L1_isPassedAfterPrescale;
        m_b_zdc_L1_trig_tav[iTrigger]      = bits & TrigDefs::L1_isPassedAfterVeto;

        zdcDidFire = zdcDidFire || trigger->isPassed ();
      } // end loop over iTrigger
    }

    if (m_debug >= 3) {
      if (m_triggerSet.count (EventTrigger::MinBias) > 0 && minbiasDidFire) {
        Info ("GetTriggers ()", "  --> m_triggerSet.count (EventTrigger::MinBias) > 0 && minbiasDidFire");
      }
      if (m_triggerSet.count (EventTrigger::Photon) > 0 && photonDidFire) {
        Info ("GetTriggers ()", "  --> m_triggerSet.count (EventTrigger::Photon) > 0 && photonDidFire");
      }
      if (m_triggerSet.count (EventTrigger::Electron) > 0 && electronDidFire) {
        Info ("GetTriggers ()", "  --> m_triggerSet.count (EventTrigger::Electron) > 0 && electronDidFire");
      }
      if (m_triggerSet.count (EventTrigger::Muon) > 0 && muonDidFire) {
        Info ("GetTriggers ()", "  --> m_triggerSet.count (EventTrigger::Muon) > 0 && muonDidFire");
      }
      if (m_triggerSet.count (EventTrigger::Jet) > 0 && jetDidFire) {
        Info ("GetTriggers ()", "  --> m_triggerSet.count (EventTrigger::Jet) > 0 && jetDidFire");
      }
      if (m_triggerSet.count (EventTrigger::ZDC) > 0 && zdcDidFire) {
        Info ("GetTriggers ()", "  --> m_triggerSet.count (EventTrigger::ZDC) > 0 && zdcDidFire");
      }
    }

    // Make sure one of the triggers fired
    const bool keepEvent = ((m_triggerSet.count (EventTrigger::MinBias) > 0 && minbiasDidFire)
                            || (m_triggerSet.count (EventTrigger::Photon) > 0 && photonDidFire)
                            || (m_triggerSet.count (EventTrigger::Electron) > 0 && electronDidFire)
                            || (m_triggerSet.count (EventTrigger::Muon) > 0 && muonDidFire)
                            || (m_triggerSet.count (EventTrigger::Jet) > 0 && jetDidFire)
                            //|| (m_triggerSet.count (EventTrigger::ZDC) > 0 && zdcDidFire)
                           );
            
    if (!keepEvent) {
      if (m_debug >= 2) {
        Info ("GetTriggers ()", "No trigger fired, continuing to the next event.");
      }
      return EL::StatusCode::SUCCESS;
    }

    if (m_debug >= 1) {
      Info ("GetTriggers ()", "Finished checking triggers.");
    }
  } // end trigger scope



  //----------------------------------------------------------------------
  // Get truth vertices information
  //----------------------------------------------------------------------
  if (!IsCollisions (m_dataType)) {
    if (m_debug >= 1) {
      Info ("GetTruthVertices ()", "Retrieving truth vertices...");
    }

    m_b_truth_vert_n = 0;

    const xAOD::TruthEventContainer* truthEvents = nullptr;
    if (!evtStore ()->retrieve (truthEvents, "TruthEvents") .isSuccess ())  {
      Error ("GetTruthVertices ()", "Failed to retrieve TruthEvents container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetTruthVertices ()", "Successfully retrieved TruthEvents container.");
    }

    for (const auto* event : *truthEvents) {
      if (m_b_truth_vert_n >= m_max_truth_vert_n) {
        Error ("GetTruthVertices ()", "Tried to overflow vertex arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetTruthVertices ()", Form ("Checked truth vertices array length, found %i vertices so far.", m_b_truth_vert_n));
      }

      const xAOD::TruthVertex* vert = event->signalProcessVertex ();

      //if (m_b_truth_vert_n == 0) {
      //  std::cout << "vertex type is ";
      //  switch (vert->vertexType ()) {
      //    case xAOD::VxType::NoVtx:         std::cout << "NoVtx";        break;
      //    case xAOD::VxType::PriVtx:        std::cout << "PriVtx";       break;
      //    case xAOD::VxType::SecVtx:        std::cout << "SecVtx";       break;
      //    case xAOD::VxType::PileUp:        std::cout << "PileUp";       break;
      //    case xAOD::VxType::ConvVtx:       std::cout << "ConvVtx";      break;
      //    case xAOD::VxType::V0Vtx:         std::cout << "V0Vtx";        break;
      //    case xAOD::VxType::KinkVtx:       std::cout << "KinkVtx";      break;
      //    case xAOD::VxType::NotSpecified:  std::cout << "NotSpecified"; break;
      //  }
      //  std::cout << std::endl;
      //}

      m_b_truth_vert_x[m_b_truth_vert_n] = vert->x ();
      m_b_truth_vert_y[m_b_truth_vert_n] = vert->y ();
      m_b_truth_vert_z[m_b_truth_vert_n] = vert->z ();
      m_b_truth_vert_ntrk[m_b_truth_vert_n] = vert->nOutgoingParticles ();

      float sumpt2 = 0;
      for (const auto part : vert->outgoingParticleLinks ()) {
        if (!part.isValid ()) {
          continue;
        }

        if (!((*part)->isCharged ())) {
          continue;
        }

        sumpt2 += std::pow ((*part)->pt () * 1e-3, 2);
      } // end loop over tracks
      m_b_truth_vert_sumpt2[m_b_truth_vert_n] = sumpt2;
      m_b_truth_vert_n++;
    }

    if (m_debug >= 1) {
      Info ("GetTruthVertices ()", "Finished retrieving truth vertices.");
    }

    if (m_b_truth_vert_n == 0) {
      if (m_debug >= 1) {
        Info ("GetTruthVertices ()", "Did not find a truth vertex in this event.");
        Info ("GetTruthVertices ()", "I require a truth vertex in MC so I am continuing to the next event.");
      }
      return EL::StatusCode::SUCCESS;
    }
  } // end truth vertex scope




  //----------------------------------------------------------------------
  // Get reconstructed vertices information
  //----------------------------------------------------------------------
  {
    if (m_debug >= 1) {
      Info ("GetPrimaryVertices ()", "Retrieving primary vertices...");
    }

    m_b_nvert = 0;

    const xAOD::VertexContainer* primaryVertices = 0;
    if (!evtStore ()->retrieve (primaryVertices, "PrimaryVertices") .isSuccess ())  {
      Error ("GetPrimaryVertices ()", "Failed to retrieve PrimaryVertices container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetPrimaryVertices ()", "Successfully retrieved PrimaryVertices container.");
    }

    for (const auto* vert : *primaryVertices) {
      if (m_b_nvert >= m_max_nvert) {
        Error ("GetPrimaryVertices ()", "Tried to overflow vertex arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetPrimaryVertices ()", Form ("Checked primary vertices array length, found %i vertices so far.", m_b_nvert));
      }

      //if (m_b_nvert == 0) {
      //  std::cout << "vertex type is ";
      //  switch (vert->vertexType ()) {
      //    case xAOD::VxType::NoVtx:         std::cout << "NoVtx";        break;
      //    case xAOD::VxType::PriVtx:        std::cout << "PriVtx";       break;
      //    case xAOD::VxType::SecVtx:        std::cout << "SecVtx";       break;
      //    case xAOD::VxType::PileUp:        std::cout << "PileUp";       break;
      //    case xAOD::VxType::ConvVtx:       std::cout << "ConvVtx";      break;
      //    case xAOD::VxType::V0Vtx:         std::cout << "V0Vtx";        break;
      //    case xAOD::VxType::KinkVtx:       std::cout << "KinkVtx";      break;
      //    case xAOD::VxType::NotSpecified:  std::cout << "NotSpecified"; break;
      //  }
      //  std::cout << std::endl;
      //}

      if (m_b_nvert == 0 && vert) {
        priVtx = vert;
      }
      m_b_vert_x[m_b_nvert] = vert->x ();
      m_b_vert_y[m_b_nvert] = vert->y ();
      m_b_vert_z[m_b_nvert] = vert->z ();
      m_b_vert_ntrk[m_b_nvert] = vert->nTrackParticles ();
      m_b_vert_type[m_b_nvert] = vert->vertexType ();

      float sumpt2 = 0;
      for (const auto track : vert->trackParticleLinks ()) {
        if (!track.isValid ()) {
          continue;
        }
        if (!m_trackSelectionToolHILoose->accept (**track, vert)) {
          continue;
        }

        sumpt2 += std::pow ((*track)->pt () * 1e-3, 2);
      } // end loop over tracks
      m_b_vert_sumpt2[m_b_nvert] = sumpt2;
      m_b_nvert++;
    }

    if (m_debug >= 1) {
      Info ("GetPrimaryVertices ()", "Finished retrieving primary vertices.");
    }

    if (m_b_nvert == 0 || priVtx == nullptr) {
      if (m_debug >= 1) {
        Info ("GetPrimaryVertices ()", "Did not find a primary vertex in this event.");
      }
      if (IsCollisions (m_dataType)) {
        if (m_debug >= 1) {
          Info ("GetPrimaryVertices ()", "I require a primary vertex in data so I am continuing to the next event.");
        }
        return EL::StatusCode::SUCCESS;
      }
    }
    else {
      assert (priVtx->vertexType () == xAOD::VxType::PriVtx);
    }
  } // end vertex scope



  //----------------------------------------------------------------------
  // Check for out of time pile up (OOTPU)
  //----------------------------------------------------------------------
  {
    if (m_debug >= 1) Info ("GetOutOfTimePileUp ()", "Checking for out-of-time pileup...");

    if (IsPbPb18 (m_collisionSystem)) {
      const xAOD::TrackParticleContainer* trackContainer = 0;
      if (!evtStore ()->retrieve (trackContainer, "InDetTrackParticles").isSuccess ()) {
        Error ("GetOutOfTimePileUp ()", "Failed to retrieve InDetTrackParticles container. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 2) {
        Info ("GetOutOfTimePileUp ()", "Successfully retrieved InDetTrackParticles container.");
      }

      const xAOD::HIEventShapeContainer* caloSums = 0;  
      if (!evtStore ()->retrieve (caloSums, "CaloSums").isSuccess ()) {
        Error ("GetOutOfTimePileUp ()", "Failed to retrieve CaloSums container. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 2) {
        Info ("GetOutOfTimePileUp ()", "Successfully retrieved CaloSums container.");
      }

      int nTracks = 0;
      for (const auto* track : *trackContainer) {
        if (track->pt () * 1e-3 < 0.5) { // cut at 500 MeV
          continue;
        }
        if (m_trackSelectionToolHITight->accept (*track, priVtx)) {
          nTracks++;
        }
      } 

      m_b_isOOTPU = is_Outpileup (*caloSums, nTracks);
    }
    else {
      m_b_isOOTPU = false;
    }
    if (m_debug >= 1) {
      Info ("GetOutOfTimePileUp ()", "Finished checking for out-of-time pileup.");
    }
  } // end OOTPU scope



  //----------------------------------------------------------------------
  // Calculate ZDC quantities
  //----------------------------------------------------------------------
  if (IsIons (m_collisionSystem) && (IsCollisions (m_dataType))){// || IsDataOverlay (m_dataType))) {
    if (m_debug >= 1) {
      Info ("GetZdcSums ()", "Retrieving ZDC sums...");
    }

    // Zdc block with online/offline info
    // offline calibration from Peter's Zdc-dev-3
    m_b_ZdcRawEnergy_A = 0;
    m_b_ZdcRawEnergy_C = 0;
    m_b_ZdcCalibEnergy_A = 0;
    m_b_ZdcCalibEnergy_C = 0;

    // run ZDC Calibration
    ANA_CHECK (m_zdcAnalysisTool->reprocessZdc ());

    const xAOD::ZdcModuleContainer* m_zdcSums = 0;
    if (!evtStore ()->retrieve (m_zdcSums, "ZdcSums_RP").isSuccess ()) {
      Error ("GetZdcSums ()", "Failed to retrieve ZdcSums container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetZdcSums ()", "Successfully retrieved ZdcSums_RP container.");
    }

    for (const auto zdcSum : *m_zdcSums) {
      const float rawEnergy = zdcSum->auxdecor <float> ("UncalibSum") * 1e-3;
      const float calibEnergy = zdcSum->auxdecor <float> ("CalibEnergy") * 1e-3;
      const int side = zdcSum->side ();
      const int type = zdcSum->type ();
      if (type != 0)
        continue;
      if (side == +1) {
        m_b_ZdcRawEnergy_A += rawEnergy;
        m_b_ZdcCalibEnergy_A += calibEnergy;
      }
      if (side == -1) {
        m_b_ZdcRawEnergy_C += rawEnergy;
        m_b_ZdcCalibEnergy_C += calibEnergy;
      }
      
      if (m_debug >= 2) {
        Info ("GetZdcSums ()", Form ("ZDC SIDE / TYPE / MOD = %i / %i / %i, amp = %g, rawEnergy = %g, calibEnergy = %g", side, type, zdcSum->zdcModule (), zdcSum->amplitude (), rawEnergy, calibEnergy));
      }
    }
    if (m_debug >= 1) {
      Info ("GetZdcSums ()", "Finished retrieving ZDC sums.");
    }
  } // end ZDC scope



  //----------------------------------------------------------------------
  // Store total FCal energies and angular integrals
  //----------------------------------------------------------------------
  {
    if (m_debug >= 1) {
      Info ("GetHIEventShape ()", "Retrieving HIEventShape...");
    }

    m_b_fcalA_et = 0;
    m_b_fcalC_et = 0;
    m_b_fcalA_et_Cos2 = 0; 
    m_b_fcalA_et_Sin2 = 0; 
    m_b_fcalC_et_Cos2 = 0; 
    m_b_fcalC_et_Sin2 = 0; 
    m_b_fcalA_et_Cos3 = 0; 
    m_b_fcalA_et_Sin3 = 0; 
    m_b_fcalC_et_Cos3 = 0; 
    m_b_fcalC_et_Sin3 = 0; 
    m_b_fcalA_et_Cos4 = 0; 
    m_b_fcalA_et_Sin4 = 0; 
    m_b_fcalC_et_Cos4 = 0; 
    m_b_fcalC_et_Sin4 = 0; 

    const xAOD::HIEventShapeContainer* hiueContainer = 0;  
    if (!evtStore ()->retrieve (hiueContainer, "HIEventShape").isSuccess ()) {
      Error ("GetHIEventShape ()", "Failed to retrieve HIEventShape container. Exiting." );
      return EL::StatusCode::FAILURE; 
    }
    else if (m_debug >= 2) {
      Info ("GetHIEventShape ()", "Successfully retrieved HIEventShape container.");
    }

    for (const auto* hiue : *hiueContainer) {
      int layer = hiue->layer ();

      if (layer != 21 && layer != 22 && layer != 23) {
        continue;
      }

      double et = hiue->et ();
      double eta = hiue->etaMin ();
      const std::vector <float>& c1 = hiue->etCos ();
      const std::vector <float>& s1 = hiue->etSin ();

      if (eta > 0) {
        m_b_fcalA_et += et * 1e-3;
        m_b_fcalA_et_Cos2 += c1.at (1) * 1e-3;
        m_b_fcalA_et_Sin2 += s1.at (1) * 1e-3;
        m_b_fcalA_et_Cos3 += c1.at (2) * 1e-3;
        m_b_fcalA_et_Sin3 += s1.at (2) * 1e-3;
        m_b_fcalA_et_Cos4 += c1.at (3) * 1e-3;
        m_b_fcalA_et_Sin4 += s1.at (3) * 1e-3;
      }
      else {
        m_b_fcalC_et += et * 1e-3;
        m_b_fcalC_et_Cos2 += c1.at (1) * 1e-3;
        m_b_fcalC_et_Sin2 += s1.at (1) * 1e-3;
        m_b_fcalC_et_Cos3 += c1.at (2) * 1e-3;
        m_b_fcalC_et_Sin3 += s1.at (2) * 1e-3;
        m_b_fcalC_et_Cos4 += c1.at (3) * 1e-3;
        m_b_fcalC_et_Sin4 += s1.at (3) * 1e-3;
      }
    }
    if (m_debug >= 1) {
      Info ("GetHIEventShape ()", "Finished retrieving HIEventShape.");
    }
  } // end FCals scope



  //----------------------------------------------------------------------
  // Get truth photons if MC
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Photons) > 0 && !IsCollisions (m_dataType)) {
    if (m_debug >= 1) {
      Info ("GetTruthPhotons ()", "Retrieving truth photons...");
    }

    m_b_truth_photon_n = 0;

    const xAOD::TruthParticleContainer* truthParticleContainer = 0;
    if (!evtStore ()->retrieve (truthParticleContainer, "TruthParticles").isSuccess ()) {
      Error ("GetTruthPhotons ()", "Failed to retrieve TruthParticles container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetTruthPhotons ()", "Successfully retrieved TruthParticles container.");
    }

    for (const auto* truthParticle : *truthParticleContainer) {
    
      if (truthParticle->status() != 1) {
        continue; // if not final state continue
      }
      if (truthParticle->absPdgId () != 22) {
        continue;  // check if photon
      }
      if (truthParticle->pt () * 1e-3 < m_min_truth_photon_pt) {
        continue; // pT cut
      }
      if (std::fabs (truthParticle->eta()) > 3) {
       continue; // require photons inside EMCal
      }

      // Check for array overflow
      if (m_b_truth_photon_n >= m_max_truth_photon_n) {
        Error ("GetTruthPhotons ()", "Tried to overflow truth photon arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetTruthPhotons ()", Form ("Checked truth photon array length, found %i truth photons so far.", m_b_truth_photon_n));
      }

      // Store truth photon info
      m_b_truth_photon_pt[m_b_truth_photon_n] = truthParticle->pt () * 1e-3;
      m_b_truth_photon_eta[m_b_truth_photon_n] = truthParticle->eta ();
      m_b_truth_photon_phi[m_b_truth_photon_n] = truthParticle->phi ();
      m_b_truth_photon_barcode[m_b_truth_photon_n] = truthParticle->barcode ();
      m_b_truth_photon_n++;
    } // end truth particles loop
    if (m_debug >= 1) {
      Info ("GetTruthPhotons ()", "Finished retrieving truth photons.");
    }
  } // end truth photons scope



  //----------------------------------------------------------------------
  // Get photon container
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Photons) > 0) {
    if (m_debug >= 1) {
      Info ("GetPhotons ()", "Retrieving photons...");
    }

    m_b_photon_n = 0;

    const xAOD::PhotonContainer* photons = 0;
    if (!evtStore ()->retrieve (photons, "Photons").isSuccess ())  {
      Error ("GetPhotons ()", "Failed to retrieve Photons container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetPhotons ()", "Successfully retrieved Photons container.");
    }

    for (const auto* init_photon : *photons) {
      // Check for array overflow
      if (m_b_photon_n >= m_max_photon_n) {
        Error ("GetPhotons ()", "Tried to overflow photon arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetPhotons ()", Form ("Checked photon array length, found %i photons so far.", m_b_photon_n));
      }
    
      // Create calibrated copy of photon
      xAOD::Photon* photon = NULL;
      if (m_egammaCalibrationAndSmearingTool->correctedCopy (*init_photon, photon) != CP::CorrectionCode::Ok || !photon) {
        Error ("GetPhotons ()", "Photon unsuccessfully calibrated. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 2) {
        Info ("GetPhotons ()", "Successfully calibrated photon.");
      }

      // Photon kinematic info
      const float photon_pt_precalib = init_photon->pt () * 1e-3;
      const float photon_pt = photon->pt () * 1e-3;
      const float photon_eta = photon->eta ();
      const float photon_etaBE = photon->caloCluster ()->etaBE (2);
      const float photon_phi = photon->phi ();

      // Photon kinematic cut
      if (photon_pt < m_min_photon_pt) {
        if (photon) {
          delete photon;
        }
        continue;
      }

      // Store photon kinematic info
      m_b_photon_pt_precalib[m_b_photon_n] = photon_pt_precalib;
      m_b_photon_pt[m_b_photon_n] = photon_pt;
      m_b_photon_eta[m_b_photon_n] = photon_eta;
      m_b_photon_etaBE[m_b_photon_n] = photon_etaBE;
      m_b_photon_phi[m_b_photon_n] = photon_phi;

      // Store photon trigger matching info
      if (IsCollisions (m_dataType) && m_triggerSet.count (EventTrigger::Photon)) {
        triggerParticles.clear ();
        triggerParticles.push_back ((const xAOD::IParticle*) photon);
        for (int iTrigger = 0; iTrigger < m_photon_trig_n; iTrigger++) {
          bool matched = m_trigMatchingTool->match (triggerParticles, m_photon_trig_name[iTrigger], 0.07, false);
          m_b_photon_matched[iTrigger][m_b_photon_n] = matched;
        }
      }

      // Store photon identification info
      m_b_photon_tight[m_b_photon_n] = m_photonTightIsEMSelector->accept (photon);
      m_b_photon_medium[m_b_photon_n] = m_photonMediumIsEMSelector->accept (photon);
      m_b_photon_loose[m_b_photon_n] = m_photonLooseIsEMSelector->accept (photon);
      m_b_photon_isem[m_b_photon_n] = m_photonTightIsEMSelector->IsemValue ();
      m_b_photon_convFlag[m_b_photon_n] = xAOD::EgammaHelpers::conversionType (photon);
      m_b_photon_Rconv[m_b_photon_n] = xAOD::EgammaHelpers::conversionRadius (photon);

      // Store photon isolation info
      m_b_photon_etcone20[m_b_photon_n] = photon->auxdata<float> ("etcone20") * 1e-3;
      m_b_photon_etcone30[m_b_photon_n] = photon->auxdata<float> ("etcone30") * 1e-3;
      m_b_photon_etcone40[m_b_photon_n] = photon->auxdata<float> ("etcone40") * 1e-3;
      m_b_photon_topoetcone20[m_b_photon_n] = photon->auxdata<float> ("topoetcone20") * 1e-3;
      m_b_photon_topoetcone30[m_b_photon_n] = photon->auxdata<float> ("topoetcone30") * 1e-3;
      m_b_photon_topoetcone40[m_b_photon_n] = photon->auxdata<float> ("topoetcone40") * 1e-3;

      // Store photon shower shapes info
      m_b_photon_Rhad1[m_b_photon_n] = photon->auxdata<float> ("Rhad1");
      m_b_photon_Rhad[m_b_photon_n] = photon->auxdata<float> ("Rhad");
      m_b_photon_e277[m_b_photon_n] = photon->auxdata<float> ("e277");
      m_b_photon_Reta[m_b_photon_n] = photon->auxdata<float> ("Reta");
      m_b_photon_Rphi[m_b_photon_n] = photon->auxdata<float> ("Rphi");
      m_b_photon_weta1[m_b_photon_n] = photon->auxdata<float> ("weta1");
      m_b_photon_weta2[m_b_photon_n] = photon->auxdata<float> ("weta2");
      m_b_photon_wtots1[m_b_photon_n] = photon->auxdata<float> ("wtots1");
      m_b_photon_f1[m_b_photon_n] = photon->auxdata<float> ("f1");
      m_b_photon_f3[m_b_photon_n] = photon->auxdata<float> ("f3");
      m_b_photon_fracs1[m_b_photon_n] = photon->auxdata<float> ("fracs1");
      m_b_photon_DeltaE[m_b_photon_n] = photon->auxdata<float> ("DeltaE");
      m_b_photon_Eratio[m_b_photon_n] = photon->auxdata<float> ("Eratio");

      // Find largest photon systematic 
      float max_dpt = 0, max_deta = 0, max_dphi = 0;
      for (auto& systematic : photonSysList) {
        m_egammaCalibrationAndSmearingTool->applySystematicVariation (systematic);

        xAOD::Photon* photonSysVar = nullptr;
        if (m_egammaCalibrationAndSmearingTool->correctedCopy (*init_photon, photonSysVar)  != CP::CorrectionCode::Ok) {
          Info ("GetPhotons ()", "Cannot apply systematic variation to photon energy scale. Continuing.");
          continue;
        }

        max_dpt = std::fmax (max_dpt, std::fabs (photonSysVar->pt () * 1e-3 - photon_pt));
        max_deta = std::fmax (max_deta, std::fabs (photonSysVar->eta () - photon_eta));
        max_dphi = std::fmax (max_dphi, std::fabs (photonSysVar->phi () - photon_phi));

        if (photonSysVar) {
          delete photonSysVar;
        }
      }

      // Store largest photon systematic info
      m_b_photon_pt_sys[m_b_photon_n] = max_dpt;
      m_b_photon_eta_sys[m_b_photon_n] = max_deta;
      m_b_photon_phi_sys[m_b_photon_n] = max_dphi;

      if (photon) {
        delete photon;
      }
      m_b_photon_n++;
    } // end photon loop
    if (m_debug >= 1) {
      Info ("GetPhotons ()", "Finished retrieving photons.");
    }
  } // end photon scope



  //----------------------------------------------------------------------
  // Get truth electrons if MC 
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Electrons) > 0 && !IsCollisions (m_dataType)) {
    if (m_debug >= 1) {
      Info ("GetTruthElectrons ()", "Retrieving truth electrons...");
    }

    m_b_truth_electron_n = 0;

    const xAOD::TruthParticleContainer* truthParticleContainer = 0;
    if (!evtStore ()->retrieve (truthParticleContainer, "TruthParticles").isSuccess ()) {
      Error ("GetTruthElectrons()", "Failed to retrieve TruthParticles container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetTruthElectrons ()", "Successfully retrieved TruthParticles container.");
    }

    for (const auto* truthParticle : *truthParticleContainer) {
    
      if (truthParticle->status() != 1) {
        continue; // if not final state continue
      }
      if (truthParticle->absPdgId () != 11) {
        continue;  // check if electron or positron
      }
      if (std::fabs (truthParticle->eta()) > 3) {
        continue; // require particles inside tracker
      }
      if (truthParticle->pt () * 1e-3 < m_min_truth_electron_pt) {
        continue;
      }

      // Check for array overflow
      if (m_b_truth_electron_n >= m_max_truth_electron_n) {
        Error ("GetTruthElectrons ()", "Tried to overflow truth electron arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetTruthElectrons ()", Form ("Checked truth electron array length, found %i truth electrons so far.", m_b_truth_electron_n));
      }

      // Store truth electron info
      m_b_truth_electron_pt[m_b_truth_electron_n] = truthParticle->pt () * 1e-3;
      m_b_truth_electron_eta[m_b_truth_electron_n] = truthParticle->eta ();
      m_b_truth_electron_phi[m_b_truth_electron_n] = truthParticle->phi ();
      m_b_truth_electron_charge[m_b_truth_electron_n] = truthParticle->charge ();
      m_b_truth_electron_barcode[m_b_truth_electron_n] = truthParticle->barcode ();
      m_b_truth_electron_n++;
      
    } // end truth particles loop
    if (m_debug >= 1) {
      Info ("GetTruthElectrons ()", "Finished retrieving truth electrons.");
    }
  } // end truth electrons scope



  //----------------------------------------------------------------------
  // Get electron container
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Electrons) > 0) {
    if (m_debug >= 1) {
      Info ("GetElectrons ()", "Retrieving electrons...");
    }

    m_b_electron_n = 0;
    m_b_electron_pt_sys.clear ();
    m_b_electron_eta_sys.clear ();
    m_b_electron_phi_sys.clear ();

    const xAOD::ElectronContainer* electrons = 0;
    if (!evtStore ()->retrieve (electrons, "Electrons").isSuccess()) {
      Error ("GetElectrons ()", "Failed to retrieve Electrons container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetElectrons ()", "Successfully retrieved Electrons container.");
    }

    for (const auto init_electron : *electrons) { 
      // Check for array overflow
      if (m_b_electron_n >= m_max_electron_n) {
        Error ("GetElectrons ()", "Tried to overflow electron arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetElectrons ()", Form ("Checked electron array length, found %i electrons so far.", m_b_electron_n));
      }

      // Create calibrated copy of electron
      xAOD::Electron* electron = nullptr;
      if (m_egammaCalibrationAndSmearingTool->correctedCopy (*init_electron, electron) != CP::CorrectionCode::Ok || !electron) {
        Error ("GetElectrons ()", "Electron unsuccessfully calibrated. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 2) {
        Info ("GetElectrons ()", "Successfully calibrated electron.");
      }

      // Electron kinematic info
      const float electron_pt_precalib = init_electron->pt () * 1e-3;
      const float electron_pt = electron->pt () * 1e-3;
      const float electron_eta = electron->eta ();
      const float electron_etaBE = electron->caloCluster ()->etaBE (2);
      const float electron_phi = electron->phi ();
      
      // Electron kinematic cut
      if (electron_pt < m_min_electron_pt) {
        if (electron) {
          delete electron;
        }
        continue;
      }

      // Store electron kinematic info
      m_b_electron_pt_precalib[m_b_electron_n] = electron_pt_precalib;
      m_b_electron_pt[m_b_electron_n] = electron_pt;
      m_b_electron_eta[m_b_electron_n] = electron_eta;
      m_b_electron_etaBE[m_b_electron_n] = electron_etaBE;
      m_b_electron_phi[m_b_electron_n] = electron_phi;
      m_b_electron_charge[m_b_electron_n] = electron->charge ();

      // Store electron trigger matching info
      if (IsCollisions (m_dataType) && m_triggerSet.count (EventTrigger::Electron)) {
        triggerParticles.clear ();
        triggerParticles.push_back ((const xAOD::IParticle*) electron);
        for (int iTrigger = 0; iTrigger < m_electron_trig_n; iTrigger++) {
          bool matched = m_trigMatchingTool->match (triggerParticles, m_electron_trig_name[iTrigger], 0.07, false);
          m_b_electron_matched[iTrigger][m_b_electron_n] = matched;
        }
      }

      // Store electron identification info
      m_b_electron_lhtight[m_b_electron_n] = m_electronLHTightSelectorTool->accept (electron);
      m_b_electron_lhmedium[m_b_electron_n] = m_electronLHMediumSelectorTool->accept (electron);
      m_b_electron_lhloose[m_b_electron_n] = m_electronLHLooseSelectorTool->accept (electron);
      m_b_electron_lhmedium_hi[m_b_electron_n] = m_electronLHMediumHISelectorTool->accept (electron);
      m_b_electron_lhloose_hi[m_b_electron_n] = m_electronLHLooseHISelectorTool->accept (electron);

      // Store electron isolation info
      m_b_electron_etcone20[m_b_electron_n] = electron->auxdata<float> ("etcone20") * 1e-3;
      m_b_electron_etcone30[m_b_electron_n] = electron->auxdata<float> ("etcone30") * 1e-3;
      m_b_electron_etcone40[m_b_electron_n] = electron->auxdata<float> ("etcone40") * 1e-3;
      m_b_electron_topoetcone20[m_b_electron_n] = electron->auxdata<float> ("topoetcone20") * 1e-3;
      m_b_electron_topoetcone30[m_b_electron_n] = electron->auxdata<float> ("topoetcone30") * 1e-3;
      m_b_electron_topoetcone40[m_b_electron_n] = electron->auxdata<float> ("topoetcone40") * 1e-3;

      // Store electron inner detector track particle info
      const xAOD::TrackParticle* electronTrack = xAOD::EgammaHelpers::getOriginalTrackParticle (electron);
      m_b_electron_id_track_pt[m_b_electron_n] = electronTrack->pt () * 1e-3;
      m_b_electron_id_track_eta[m_b_electron_n] = electronTrack->eta ();
      m_b_electron_id_track_phi[m_b_electron_n] = electronTrack->phi ();
      m_b_electron_id_track_charge[m_b_electron_n] = electronTrack->charge ();
      m_b_electron_id_track_d0[m_b_electron_n] = electronTrack->d0 ();
      m_b_electron_id_track_d0sig[m_b_electron_n] = xAOD::TrackingHelpers::d0significance (electronTrack, eventInfo->beamPosSigmaX (), eventInfo->beamPosSigmaY (), eventInfo->beamPosSigmaXY());
      m_b_electron_id_track_z0[m_b_electron_n] = electronTrack->z0 ();
      try {
        m_b_electron_id_track_z0sig[m_b_electron_n] = xAOD::TrackingHelpers::z0significance (electronTrack);
      }
      catch (const std::runtime_error& error) {
        if (m_debug >= 1) {
          Info ("GetElectrons ()", "Recovered from error in z0 significance calculation.");
        }
        return EL::StatusCode::SUCCESS;
      }
      m_b_electron_id_track_theta[m_b_electron_n] = electronTrack->theta ();
      m_b_electron_id_track_vz[m_b_electron_n] = electronTrack->vz ();
      m_b_electron_id_track_Loose[m_b_electron_n] = m_trackSelectionToolLoose->accept (*electronTrack, priVtx);
      m_b_electron_id_track_LoosePrimary[m_b_electron_n] = m_trackSelectionToolLoosePrimary->accept (*electronTrack, priVtx);
      m_b_electron_id_track_TightPrimary[m_b_electron_n] = m_trackSelectionToolTightPrimary->accept (*electronTrack, priVtx);
      m_b_electron_id_track_HILoose[m_b_electron_n] = m_trackSelectionToolHILoose->accept (*electronTrack, priVtx);
      m_b_electron_id_track_HITight[m_b_electron_n] = m_trackSelectionToolHITight->accept (*electronTrack, priVtx);

      // Store electron shower shapes info
      m_b_electron_Rhad1[m_b_electron_n] = electron->auxdata<float> ("Rhad1");
      m_b_electron_Rhad[m_b_electron_n] = electron->auxdata<float> ("Rhad");
      m_b_electron_e277[m_b_electron_n] = electron->auxdata<float> ("e277");
      m_b_electron_Reta[m_b_electron_n] = electron->auxdata<float> ("Reta");
      m_b_electron_Rphi[m_b_electron_n] = electron->auxdata<float> ("Rphi");
      m_b_electron_weta1[m_b_electron_n] = electron->auxdata<float> ("weta1");
      m_b_electron_weta2[m_b_electron_n] = electron->auxdata<float> ("weta2");
      m_b_electron_wtots1[m_b_electron_n] = electron->auxdata<float> ("wtots1");
      m_b_electron_f1[m_b_electron_n] = electron->auxdata<float> ("f1");
      m_b_electron_f3[m_b_electron_n] = electron->auxdata<float> ("f3");
      m_b_electron_fracs1[m_b_electron_n] = electron->auxdata<float> ("fracs1");
      m_b_electron_DeltaE[m_b_electron_n] = electron->auxdata<float> ("DeltaE");
      m_b_electron_Eratio[m_b_electron_n] = electron->auxdata<float> ("Eratio");

      // Store electron systematic info
      m_b_electron_pt_sys.push_back (std::vector <float> (0));
      m_b_electron_eta_sys.push_back (std::vector <float> (0));
      m_b_electron_phi_sys.push_back (std::vector <float> (0));
      for (auto& systematic : electronSysList) {
        m_egammaCalibrationAndSmearingTool->applySystematicVariation (systematic);

        xAOD::Electron* electronSysVar = nullptr;
        if (m_egammaCalibrationAndSmearingTool->correctedCopy (*init_electron, electronSysVar)  != CP::CorrectionCode::Ok) {
          Info ("GetElectrons ()", "Cannot apply systematic variation to electron energy scale. Continuing.");
          continue;
        }

        m_b_electron_pt_sys[m_b_electron_n].push_back (electronSysVar->pt () * 1e-3);
        m_b_electron_eta_sys[m_b_electron_n].push_back (electronSysVar->eta ());
        m_b_electron_phi_sys[m_b_electron_n].push_back (electronSysVar->phi ());

        if (electronSysVar) {
          delete electronSysVar;
        }
      }

      m_b_electron_n++;
      if (electron) {
        delete electron;
      }
    } // end electron loop

    if (m_debug >= 1) {
      Info ("GetElectrons ()", "Finished retrieving electrons.");
    }
  } // end electron scope



  //----------------------------------------------------------------------
  // Get egamma clusters
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Photons) > 0 || m_saveObjects.count (SaveObject::Electrons) > 0) {
    if (m_debug >= 1) {
      Info ("GetEgammaClusters ()", "Retrieving egamma clusters...");
    }

    m_b_em_cluster_n = 0;

    const xAOD::CaloClusterContainer* clusters = 0;
    if (!evtStore ()->retrieve (clusters, "egammaClusters") .isSuccess ()) {
      Error ("GetEgammaClusters()", "Failed to retrieve EgammaClusters container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetEgammaClusters ()", "Successfully retrieved EgammaClusters container.");
    }

    for (auto cluster : *clusters) {
      // Check for array overflow
      if (m_b_em_cluster_n >= m_max_em_cluster_n) {
        Error ("GetEgammaClusters ()", "Tried to overflow egamma cluster arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetEgammaClusters ()", Form ("Checked egamma cluster array length, found %i clusters so far.", m_b_em_cluster_n));
      }

      if (cluster->pt() * 1e-3 < m_min_em_cluster_pt) {
        continue;
      }

      // Store cluster info
      m_b_em_cluster_pt[m_b_em_cluster_n] = cluster->pt ()*1e-3;
      m_b_em_cluster_et[m_b_em_cluster_n] = cluster->et ()*1e-3;
      m_b_em_cluster_eta[m_b_em_cluster_n] = cluster->eta ();
      m_b_em_cluster_phi[m_b_em_cluster_n] = cluster->phi ();

      m_b_em_cluster_energyBE[m_b_em_cluster_n] = cluster->energyBE (2)*1e-3;
      m_b_em_cluster_etaBE[m_b_em_cluster_n] = cluster->etaBE (2);
      m_b_em_cluster_phiBE[m_b_em_cluster_n] = cluster->phiBE (2);
      m_b_em_cluster_calE[m_b_em_cluster_n] = cluster->calE ()*1e-3;
      m_b_em_cluster_calEta[m_b_em_cluster_n] = cluster->calEta ();
      m_b_em_cluster_calPhi[m_b_em_cluster_n] = cluster->calPhi ();

      m_b_em_cluster_size[m_b_em_cluster_n] = cluster->clusterSize ();
      m_b_em_cluster_status[m_b_em_cluster_n] = cluster->signalState ();
      m_b_em_cluster_n++;
    } // end egamma cluster loop
    if (m_debug >= 1) {
      Info ("GetEgammaClusters ()", "Finished retrieving egamma clusters.");
    }
  } // end egamma cluster scope



  //----------------------------------------------------------------------
  // Get truth muons if MC 
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Muons) > 0 && !IsCollisions (m_dataType)) {
    if (m_debug >= 1) {
      Info ("GetTruthMuons ()", "Retrieving truth muons...");
    }

    m_b_truth_muon_n = 0;

    const xAOD::TruthParticleContainer* truthParticleContainer = 0;
    if (!evtStore ()->retrieve (truthParticleContainer, "TruthParticles").isSuccess ()) {
      Error ("GetTruthMuons()", "Failed to retrieve TruthParticles container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetTruthMuons ()", "Successfully retrieved TruthParticles container.");
    }

    for (const auto* truthParticle : *truthParticleContainer) {
    
      if (truthParticle->status() != 1) {
        continue; // if not final state continue
      }
      if (truthParticle->absPdgId () != 13) {
        continue;  // check if muon or anti-muon
      }
      if (std::fabs (truthParticle->eta()) > 3) {
        continue; // require particles inside tracker
      }
      if (truthParticle->pt () * 1e-3 < m_min_truth_muon_pt) {
        continue;
      }

      // Check for array overflow
      if (m_b_truth_muon_n >= m_max_truth_muon_n) {
        Error ("GetTruthMuons ()", "Tried to overflow truth muon arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetTruthMuons ()", Form ("Checked truth muon array length, found %i truth muons so far.", m_b_truth_muon_n));
      }

      // Store truth muon info
      m_b_truth_muon_pt[m_b_truth_muon_n] = truthParticle->pt () * 1e-3;
      m_b_truth_muon_eta[m_b_truth_muon_n] = truthParticle->eta ();
      m_b_truth_muon_phi[m_b_truth_muon_n] = truthParticle->phi ();
      m_b_truth_muon_charge[m_b_truth_muon_n] = truthParticle->charge ();
      m_b_truth_muon_barcode[m_b_truth_muon_n] = truthParticle->barcode ();
      m_b_truth_muon_n++;
    } // end truth particles loop
    if (m_debug >= 1) {
      Info ("GetTruthMuons ()", "Finished retrieving truth muons.");
    }
  } // end truth muons scope



  //----------------------------------------------------------------------
  // Get muon container
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Muons) > 0) {
    if (m_debug >= 1) {
      Info ("GetMuons ()", "Retrieving muons...");
    }

    m_b_muon_n = 0;
    m_b_muon_pt_sys.clear ();
    m_b_muon_eta_sys.clear ();
    m_b_muon_phi_sys.clear ();

    const xAOD::MuonContainer* muons = 0;
    if (!evtStore ()->retrieve (muons, "Muons").isSuccess()) {
      Error ("GetMuons ()", "Failed to retrieve Muons container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetMuons ()", "Successfully retrieved Muons container.");
    }

    for (const auto init_muon : *muons) { 
      // Check for array overflow
      if (m_b_muon_n >= m_max_muon_n) {
        Error ("GetMuons ()", "Tried to overflow muon arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetMuons ()", Form ("Checked muon array length, found %i muons so far.", m_b_muon_n));
      }

      // Create calibrated copy of muon
      xAOD::Muon* muon = nullptr;
      if (m_muonCalibrationAndSmearingTool->correctedCopy (*init_muon, muon) != CP::CorrectionCode::Ok || !muon) {
        Error ("GetMuons ()", "Muon unsuccessfully calibrated. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 2) {
        Info ("GetMuons ()", "Successfully calibrated muon.");
      }

      // Muon kinematic info 
      auto* muon_ms = muon->trackParticle (xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle);
      const float muon_pt_precalib = init_muon->pt () * 1e-3;
      const float muon_pt = muon->pt () * 1e-3;
      const float muon_ms_pt_precalib = (muon_ms ? muon_ms->pt () * 1e-3 : -999);
      const float muon_ms_pt = (muon_ms ? muon->auxdata<float> ("MuonSpectrometerPt") * 1e-3 : -999);
      const float muon_eta = muon->eta ();
      const float muon_phi = muon->phi ();

      // Muon kinematic cut
      if (muon_pt < m_min_muon_pt && muon_ms_pt < m_min_muon_pt) {
        if (muon) {
          delete muon;
        }
        continue;
      }

      // Store muon kinematic info
      m_b_muon_pt_precalib[m_b_muon_n] = muon_pt_precalib;
      m_b_muon_pt[m_b_muon_n] = muon_pt;
      m_b_muon_ms_pt_precalib[m_b_muon_n] = muon_ms_pt_precalib;
      m_b_muon_ms_pt[m_b_muon_n] = muon_ms_pt;
      m_b_muon_eta[m_b_muon_n] = muon_eta;
      m_b_muon_phi[m_b_muon_n] = muon_phi;
      m_b_muon_charge[m_b_muon_n] = muon->charge ();

      // Store muon trigger matching info
      if (IsCollisions (m_dataType) && m_triggerSet.count (EventTrigger::Muon)) {
        triggerParticles.clear ();
        triggerParticles.push_back ((const xAOD::IParticle*) muon);
        for (int iTrigger = 0; iTrigger < m_muon_trig_n; iTrigger++) {
          bool matched = m_trigMatchingTool->match (triggerParticles, m_muon_trig_name[iTrigger], 0.07, false);
          m_b_muon_matched[iTrigger][m_b_muon_n] = matched;
        }
      }

      // Store muon identification info
      m_b_muon_tight[m_b_muon_n] = m_muonTightSelectorTool->accept (muon);
      m_b_muon_medium[m_b_muon_n] = m_muonMediumSelectorTool->accept (muon);
      m_b_muon_loose[m_b_muon_n] = m_muonLooseSelectorTool->accept (muon);

      // Store muon isolation info
      m_b_muon_etcone20[m_b_muon_n] = muon->auxdata<float> ("etcone20") * 1e-3;
      m_b_muon_etcone30[m_b_muon_n] = muon->auxdata<float> ("etcone30") * 1e-3;
      m_b_muon_etcone40[m_b_muon_n] = muon->auxdata<float> ("etcone40") * 1e-3;
      m_b_muon_topoetcone20[m_b_muon_n] = muon->auxdata<float> ("topoetcone20") * 1e-3;
      m_b_muon_topoetcone30[m_b_muon_n] = muon->auxdata<float> ("topoetcone30") * 1e-3;
      m_b_muon_topoetcone40[m_b_muon_n] = muon->auxdata<float> ("topoetcone40") * 1e-3;

      // Store muon inner detector track info
      const auto muon_id_track = muon->inDetTrackParticleLink ();
      if (muon_id_track.isValid ()) {
        m_b_muon_id_track_pt[m_b_muon_n] = (*muon_id_track)->pt () * 1e-3;
        m_b_muon_id_track_eta[m_b_muon_n] = (*muon_id_track)->eta ();
        m_b_muon_id_track_phi[m_b_muon_n] = (*muon_id_track)->phi ();
        m_b_muon_id_track_charge[m_b_muon_n] = (*muon_id_track)->charge ();
        m_b_muon_id_track_d0[m_b_muon_n] = (*muon_id_track)->d0 ();
        m_b_muon_id_track_d0sig[m_b_muon_n] = xAOD::TrackingHelpers::d0significance (*muon_id_track, eventInfo->beamPosSigmaX (), eventInfo->beamPosSigmaY (), eventInfo->beamPosSigmaXY());
        m_b_muon_id_track_z0[m_b_muon_n] = (*muon_id_track)->z0 ();
        try {
          m_b_muon_id_track_z0sig[m_b_muon_n] = xAOD::TrackingHelpers::z0significance (*muon_id_track);
        }
        catch (const std::runtime_error& error) {
          if (m_debug >= 1) {
            Info ("GetMuons ()", "Recovered from error in muon inner detector track z0 significance calculation.");
          }
          return EL::StatusCode::SUCCESS;
        }
        m_b_muon_id_track_theta[m_b_muon_n] = (*muon_id_track)->theta ();
        m_b_muon_id_track_vz[m_b_muon_n] = (*muon_id_track)->vz ();
        m_b_muon_id_track_Loose[m_b_muon_n] = m_trackSelectionToolLoose->accept (**muon_id_track, priVtx);
        m_b_muon_id_track_LoosePrimary[m_b_muon_n] = m_trackSelectionToolLoosePrimary->accept (**muon_id_track, priVtx);
        m_b_muon_id_track_TightPrimary[m_b_muon_n] = m_trackSelectionToolTightPrimary->accept (**muon_id_track, priVtx);
        m_b_muon_id_track_HILoose[m_b_muon_n] = m_trackSelectionToolHILoose->accept (**muon_id_track, priVtx);
        m_b_muon_id_track_HITight[m_b_muon_n] = m_trackSelectionToolHITight->accept (**muon_id_track, priVtx);
      }
      else {
        m_b_muon_id_track_pt[m_b_muon_n] = -999;
        m_b_muon_id_track_eta[m_b_muon_n] = -999;
        m_b_muon_id_track_phi[m_b_muon_n] = -999;
        m_b_muon_id_track_charge[m_b_muon_n] = -999;
        m_b_muon_id_track_d0[m_b_muon_n] = -999;
        m_b_muon_id_track_d0sig[m_b_muon_n] = -999;
        m_b_muon_id_track_z0[m_b_muon_n] = -999;
        m_b_muon_id_track_z0sig[m_b_muon_n] = -999;
        m_b_muon_id_track_theta[m_b_muon_n] = -999;
        m_b_muon_id_track_vz[m_b_muon_n] = -999;
        m_b_muon_id_track_Loose[m_b_muon_n] = false;
        m_b_muon_id_track_LoosePrimary[m_b_muon_n] = false;
        m_b_muon_id_track_TightPrimary[m_b_muon_n] = false;
        m_b_muon_id_track_HITight[m_b_muon_n] = false;
        m_b_muon_id_track_HILoose[m_b_muon_n] = false;
      }

      // Store muon spectrometer track info
      const auto muon_ms_track = muon->extrapolatedMuonSpectrometerTrackParticleLink ();
      if (muon_ms_track.isValid ()) {
        m_b_muon_ms_track_pt[m_b_muon_n] = (*muon_ms_track)->pt () * 1e-3;
        m_b_muon_ms_track_eta[m_b_muon_n] = (*muon_ms_track)->eta ();
        m_b_muon_ms_track_phi[m_b_muon_n] = (*muon_ms_track)->phi ();
        m_b_muon_ms_track_charge[m_b_muon_n] = (*muon_ms_track)->charge ();
        m_b_muon_ms_track_d0[m_b_muon_n] = (*muon_ms_track)->d0 ();
        m_b_muon_ms_track_d0sig[m_b_muon_n] = xAOD::TrackingHelpers::d0significance (*muon_ms_track, eventInfo->beamPosSigmaX (), eventInfo->beamPosSigmaY (), eventInfo->beamPosSigmaXY());
        m_b_muon_ms_track_z0[m_b_muon_n] = (*muon_ms_track)->z0 ();
        try {
          m_b_muon_ms_track_z0sig[m_b_muon_n] = xAOD::TrackingHelpers::z0significance (*muon_ms_track);
        }
        catch (const std::runtime_error& error) {
          if (m_debug >= 1) {
            Info ("GetMuons ()", "Recovered from error in muon spectrometer track z0 significance calculation.");
          }
          return EL::StatusCode::SUCCESS;
        }
        m_b_muon_ms_track_vz[m_b_muon_n] = (*muon_ms_track)->vz ();
        m_b_muon_ms_track_theta[m_b_muon_n] = (*muon_ms_track)->theta ();
      }
      else {
        m_b_muon_ms_track_pt[m_b_muon_n] = -999;
        m_b_muon_ms_track_eta[m_b_muon_n] = -999;
        m_b_muon_ms_track_phi[m_b_muon_n] = -999;
        m_b_muon_ms_track_charge[m_b_muon_n] = -999;
        m_b_muon_ms_track_d0[m_b_muon_n] = -999;
        m_b_muon_ms_track_d0sig[m_b_muon_n] = -999;
        m_b_muon_ms_track_z0[m_b_muon_n] = -999;
        m_b_muon_ms_track_z0sig[m_b_muon_n] = -999;
        m_b_muon_ms_track_theta[m_b_muon_n] = -999;
        m_b_muon_ms_track_vz[m_b_muon_n] = -999;
      }

      // Store muon energy scale systematic info
      m_b_muon_pt_sys.push_back (std::vector <float> (0));
      m_b_muon_eta_sys.push_back (std::vector <float> (0));
      m_b_muon_phi_sys.push_back (std::vector <float> (0));
      for (auto& systematic : muonSysList) {
        m_muonCalibrationAndSmearingTool->applySystematicVariation (systematic);

        xAOD::Muon* muonSysVar = nullptr;
        if (m_muonCalibrationAndSmearingTool->correctedCopy (*init_muon, muonSysVar)  != CP::CorrectionCode::Ok) {
          Info ("GetMuons ()", "Cannot apply systematic variation to muon energy scale. Continuing.");
          continue;
        }

        m_b_muon_pt_sys[m_b_muon_n].push_back (muonSysVar->pt () * 1e-3);
        m_b_muon_eta_sys[m_b_muon_n].push_back (muonSysVar->eta ());
        m_b_muon_phi_sys[m_b_muon_n].push_back (muonSysVar->phi ());

        if (muonSysVar) {
          delete muonSysVar;
        }
      }

      m_b_muon_n++;
      if (muon) {
        delete muon;
      }
    } // end muon loop
    if (m_debug >= 1) Info ("GetMuons ()", "Finished retrieving muons.");
  } // end muon scope



  //----------------------------------------------------------------------
  // Get truth tracks if MC 
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Tracks) > 0 && !IsCollisions (m_dataType)) {
    if (m_debug >= 1) {
      Info ("GetTruthTracks ()", "Retrieving truth tracks...");
    }

    m_b_truth_trk_n = 0;

    const xAOD::TruthParticleContainer* truthParticleContainer = 0;
    if (!evtStore ()->retrieve (truthParticleContainer, "TruthParticles").isSuccess ()) {
      Error ("GetTruthTracks ()", "Failed to retrieve TruthParticles container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetTruthTracks ()", "Successfully retrieved TruthParticles container.");
    }

    for (const auto* truthParticle : *truthParticleContainer) {
    
      if (truthParticle->status() != 1) {
        continue; // if not final state continue
      }
      //if (!truthParticle->isCharged ()) {
      //  continue; // require charged particles
      //}
      if (truthParticle->pt() * 1e-3 < m_min_truth_trk_pt) {
        continue; // pT cut
      }
      if (std::fabs (truthParticle->eta()) > 5) {
        continue; // require particles inside detector
      }

      // Check for array overflow
      if (m_b_truth_trk_n >= m_max_truth_trk_n) {
        Error ("GetTruthTracks ()", "Tried to overflow truth track arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetTruthTracks ()", Form ("Checked truth track array length, found %i truth tracks so far.", m_b_truth_trk_n));
      }

      // Store truth track info
      m_b_truth_trk_pt[m_b_truth_trk_n] = truthParticle->pt () * 1e-3;
      m_b_truth_trk_eta[m_b_truth_trk_n] = truthParticle->eta ();
      m_b_truth_trk_phi[m_b_truth_trk_n] = truthParticle->phi ();
      m_b_truth_trk_charge[m_b_truth_trk_n] = truthParticle->charge ();
      m_b_truth_trk_pdgid[m_b_truth_trk_n] = truthParticle->pdgId ();
      m_b_truth_trk_barcode[m_b_truth_trk_n] = truthParticle->barcode ();
      m_b_truth_trk_isHadron[m_b_truth_trk_n] = truthParticle->isHadron ();
      m_b_truth_trk_n++;
      
    } // end truth particles loop
    if (m_debug >= 1) {
      Info ("GetTruthTracks ()", "Finished retrieving truth tracks.");
    }
  } // end truth tracks scope



  //----------------------------------------------------------------------
  // Get tracks
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Tracks) > 0) {
    if (m_debug >= 1) {
      Info ("GetInDetTrackParticles ()", "Retrieving inner detector tracks...");
    }

    m_b_ntrk = 0;

    const xAOD::TrackParticleContainer* trackContainer = 0;
    if (!evtStore ()->retrieve (trackContainer, "InDetTrackParticles").isSuccess ()) {
      Error ("GetInDetTrackParticles()", "Failed to retrieve InDetTrackParticles container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetInDetTrackParticles ()", "Successfully retrieved InDetTrackParticles container.");
    }

    std::pair<unsigned int, unsigned int> res;

    for (const auto* track : *trackContainer) {
      // Check for array overflow
      if (m_b_ntrk >= m_max_ntrk) {
        Error ("GetInDetTrackParticles ()", "Tried to overflow track arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetInDetTrackParticles ()", Form ("Checked track array length, found %i tracks so far.", m_b_ntrk));
      }

      // Track kinematic info
      const float trk_pt = track->pt () * 1e-3;
      const float trk_eta = track->eta ();
      const float trk_phi = track->phi ();

      // Track identification info
      const bool passLoose = m_trackSelectionToolLoose->accept (*track, priVtx);
      const bool passLoosePrimary = m_trackSelectionToolLoosePrimary->accept (*track, priVtx);
      const bool passTightPrimary = m_trackSelectionToolTightPrimary->accept (*track, priVtx);
      const bool passHITight = m_trackSelectionToolHITight->accept (*track, priVtx);
      const bool passHILoose = m_trackSelectionToolHILoose->accept (*track, priVtx);

      // Add track to vector for UPC gap study
      if (track->pt () * 1e-3 > 0.4 && passHILoose) {
        vec_trk_eta.push_back (track->eta ());
      }

      // Track kinematic cut
      if (trk_pt < m_min_trk_pt) {
        continue;
      }

      // Store track kinematic info
      m_b_trk_pt[m_b_ntrk] = trk_pt;
      m_b_trk_eta[m_b_ntrk] = trk_eta;
      m_b_trk_phi[m_b_ntrk] = trk_phi;
      m_b_trk_charge[m_b_ntrk] = track->charge ();

      // Store track identification info
      m_b_trk_Loose[m_b_ntrk] = passLoose;
      m_b_trk_LoosePrimary[m_b_ntrk] = passLoosePrimary;
      m_b_trk_TightPrimary[m_b_ntrk] = passTightPrimary;
      m_b_trk_HITight[m_b_ntrk] = passHITight;
      m_b_trk_HILoose[m_b_ntrk] = passHILoose;

      // Store track vertexing info
      m_b_trk_d0[m_b_ntrk] = track->d0 ();
      m_b_trk_d0sig[m_b_ntrk] = xAOD::TrackingHelpers::d0significance (track, eventInfo->beamPosSigmaX (), eventInfo->beamPosSigmaY (), eventInfo->beamPosSigmaXY());
      m_b_trk_z0[m_b_ntrk] = track->z0 ();
      try {
        m_b_trk_z0sig[m_b_ntrk] = xAOD::TrackingHelpers::z0significance (track);
      }
      catch (const std::runtime_error& error) {
        if (m_debug >= 1) {
          Info ("GetInDetTrackParticles ()", "Recovered from error in inner detector track z0 significance calculation.");
        }
        return EL::StatusCode::SUCCESS;
      }
      m_b_trk_theta[m_b_ntrk] = track->theta ();
      m_b_trk_vz[m_b_ntrk] = track->vz ();

      // Store track hits info
      m_b_trk_nBLayerHits[m_b_ntrk]       = (char) getSumInt (*track, xAOD::numberOfBLayerHits);
      m_b_trk_nBLayerSharedHits[m_b_ntrk] = (char) getSumInt (*track, xAOD::numberOfBLayerSharedHits);
      m_b_trk_nPixelHits[m_b_ntrk]        = (char) getSumInt (*track, xAOD::numberOfPixelHits);
      m_b_trk_nPixelHoles[m_b_ntrk]       = (char) getSumInt (*track, xAOD::numberOfPixelHoles);
      m_b_trk_nPixelSharedHits[m_b_ntrk]  = (char) getSumInt (*track, xAOD::numberOfPixelSharedHits);
      m_b_trk_nPixelDeadSensors[m_b_ntrk] = (char) getSumInt (*track, xAOD::numberOfPixelDeadSensors);
      m_b_trk_nSCTHits[m_b_ntrk]          = (char) getSumInt (*track, xAOD::numberOfSCTHits);
      m_b_trk_nSCTHoles[m_b_ntrk]         = (char) getSumInt (*track, xAOD::numberOfSCTHoles);
      m_b_trk_nSCTDoubleHoles[m_b_ntrk]   = (char) getSumInt (*track, xAOD::numberOfSCTDoubleHoles);
      m_b_trk_nSCTSharedHits[m_b_ntrk]    = (char) getSumInt (*track, xAOD::numberOfSCTSharedHits);
      m_b_trk_nSCTDeadSensors[m_b_ntrk]   = (char) getSumInt (*track, xAOD::numberOfSCTDeadSensors);
      m_b_trk_pixeldEdx[m_b_ntrk]         = (char) getSumFloat (*track, xAOD::pixeldEdx);
    
      // Store track truth partner info (MC only)
      if (!IsCollisions (m_dataType)) {
        res = m_mcTruthClassifier->particleTruthClassifier (track);
        //m_b_trk_prob_truth[m_b_ntrk] = m_mcTruthClassifier->getProbTrktoTruth ());

        if (track->isAvailable <ElementLink <xAOD::TruthParticleContainer>> ("truthParticleLink")) {
          ElementLink <xAOD::TruthParticleContainer> link = track->auxdata <ElementLink<xAOD::TruthParticleContainer>> ("truthParticleLink");

          if (link.isValid ()) {
            m_b_trk_prob_truth[m_b_ntrk] = m_mcTruthClassifier->getProbTrktoTruth ();

            const xAOD::TruthParticle* thePart = xAOD::TruthHelpers::getTruthParticle (*(track));

            if (thePart) {
              m_b_trk_truth_pt[m_b_ntrk] = thePart->pt () * 1e-3;
              m_b_trk_truth_eta[m_b_ntrk] = thePart->eta ();
              m_b_trk_truth_phi[m_b_ntrk] = thePart->phi ();
              m_b_trk_truth_charge[m_b_ntrk] = thePart->charge ();

              res = m_mcTruthClassifier->particleTruthClassifier (track);
              m_b_trk_truth_type[m_b_ntrk] = res.first;
              m_b_trk_truth_orig[m_b_ntrk] = res.second;
              m_b_trk_truth_pdgid[m_b_ntrk] = thePart->pdgId ();
              m_b_trk_truth_barcode[m_b_ntrk] = thePart->barcode ();

              if (thePart->hasProdVtx ()) {
                const xAOD::TruthVertex* vtx = thePart->prodVtx ();
                m_b_trk_truth_vz[m_b_ntrk] = vtx->z ();
                m_b_trk_truth_nIn[m_b_ntrk] = vtx->nIncomingParticles ();
              }
              else {
                m_b_trk_truth_vz[m_b_ntrk] = -999;
                m_b_trk_truth_nIn[m_b_ntrk] = -999;
              }
              m_b_trk_truth_isHadron[m_b_ntrk] = thePart->isHadron ();

            }

            else {
              m_b_trk_truth_pt[m_b_ntrk] = -999;
              m_b_trk_truth_eta[m_b_ntrk] = -999;
              m_b_trk_truth_phi[m_b_ntrk] = -999;
              m_b_trk_truth_charge[m_b_ntrk] = -999;

              m_b_trk_truth_type[m_b_ntrk] = -999;
              m_b_trk_truth_orig[m_b_ntrk] = -999;
              m_b_trk_truth_pdgid[m_b_ntrk] = -999;
              m_b_trk_truth_barcode[m_b_ntrk] = -999;

              m_b_trk_truth_vz[m_b_ntrk] = -999;
              m_b_trk_truth_nIn[m_b_ntrk] = -999;
              m_b_trk_truth_isHadron[m_b_ntrk] = false;
            }
          }

          else {
            m_b_trk_prob_truth[m_b_ntrk] = 0;

            m_b_trk_truth_pt[m_b_ntrk] = -999;
            m_b_trk_truth_eta[m_b_ntrk] = -999;
            m_b_trk_truth_phi[m_b_ntrk] = -999;
            m_b_trk_truth_charge[m_b_ntrk] = -999;

            m_b_trk_truth_type[m_b_ntrk] = -999;
            m_b_trk_truth_orig[m_b_ntrk] = -999;
            m_b_trk_truth_pdgid[m_b_ntrk] = -999;
            m_b_trk_truth_barcode[m_b_ntrk] = -999;

            m_b_trk_truth_vz[m_b_ntrk] = -999;
            m_b_trk_truth_nIn[m_b_ntrk] = -999;
            m_b_trk_truth_isHadron[m_b_ntrk] = false;
          }
          
        } // end if TruthParticleLink is available (

      } // end if not collisions

      m_b_ntrk++;
    } // end tracks loop
    if (m_debug >= 1) {
      Info ("GetInDetTrackParticles ()", "Finished retrieving inner detector tracks.");
    }
  } // end tracks scope



  //----------------------------------------------------------------------
  // Gets collision gap information
  //----------------------------------------------------------------------
  {
    if (m_debug >= 1) {
      Info ("GetGaps ()", "Retrieving gap quantities...");
    }

    // Gap information
    const xAOD::CaloClusterContainer* caloClusters = 0;
    ANA_CHECK (evtStore ()->retrieve (caloClusters, "CaloCalTopoClusters"));
    std::vector <float> vec_cl_eta;
    vec_cl_eta.clear ();

    for (const auto cluster : *caloClusters) {
      if (cluster->pt () * 1e-3 < 0.2) {
        continue; // pT cut at 200 MeV
      }
      //if (!passSinfigCut (cluster->eta(), cluster->auxdata< float >("CELL_SIG_SAMPLING"), cluster->auxdata<float>("CELL_SIGNIFICANCE"))) continue;
      vec_cl_eta.push_back (cluster->eta());
    }

    m_b_sumGap_A  = -1;
    m_b_sumGap_C  = -1;
    m_b_edgeGap_A = -1;
    m_b_edgeGap_C = -1;
    GapCut (vec_trk_eta, vec_cl_eta);

    m_b_cluster_sumGap_A  = -1;
    m_b_cluster_sumGap_C  = -1;
    m_b_cluster_edgeGap_A = -1;
    m_b_cluster_edgeGap_C = -1;
    ClusterGapCut (vec_cl_eta);

    vec_cl_eta.clear ();
    vec_trk_eta.clear ();
    if (m_debug >= 1) {
      Info ("GetGaps ()", "Finished retrieving gap quantities.");
    }
  } // end gaps scope



  //----------------------------------------------------------------------
  // Get HI clusters
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Clusters) > 0) {
    if (m_debug >= 1) {
      Info ("GetHIClusters ()", "Retrieving HI clusters...");
    }

    m_b_hi_cluster_n = 0;

    const xAOD::CaloClusterContainer* clusters = 0;
    if (!evtStore ()->retrieve (clusters, "HIClusters") .isSuccess ()) {
      Error ("GetHIClusters()", "Failed to retrieve HIClusters container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetHIClusters ()", "Successfully retrieved HIClusters container.");
    }

    for (auto cluster : *clusters) {
      // Check for array overflow
      if (m_b_hi_cluster_n >= m_max_hi_cluster_n) {
        Error ("GetHIClusters ()", "Tried to overflow HI cluster arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetHIClusters ()", Form ("Checked HI cluster array length, found %i clusters so far.", m_b_hi_cluster_n));
      }

      if (cluster->pt() * 1e-3 < m_min_hi_cluster_pt) {
        continue;
      }

      // Store cluster info
      m_b_hi_cluster_pt[m_b_hi_cluster_n] = cluster->pt ()*1e-3;
      m_b_hi_cluster_et[m_b_hi_cluster_n] = cluster->et ()*1e-3;
      m_b_hi_cluster_eta[m_b_hi_cluster_n] = cluster->eta ();
      m_b_hi_cluster_phi[m_b_hi_cluster_n] = cluster->phi ();

      m_b_hi_cluster_energyBE[m_b_hi_cluster_n] = cluster->energyBE (2)*1e-3;
      m_b_hi_cluster_etaBE[m_b_hi_cluster_n] = cluster->etaBE (2);
      m_b_hi_cluster_phiBE[m_b_hi_cluster_n] = cluster->phiBE (2);
      m_b_hi_cluster_calE[m_b_hi_cluster_n] = cluster->calE ()*1e-3;
      m_b_hi_cluster_calEta[m_b_hi_cluster_n] = cluster->calEta ();
      m_b_hi_cluster_calPhi[m_b_hi_cluster_n] = cluster->calPhi ();

      m_b_hi_cluster_size[m_b_hi_cluster_n] = cluster->clusterSize ();
      m_b_hi_cluster_status[m_b_hi_cluster_n] = cluster->signalState ();
      m_b_hi_cluster_n++;
    } // end HI cluster loop
    if (m_debug >= 1) {
      Info ("GetHIClusters ()", "Finished retrieving HI clusters.");
    }
  } // end HI cluster scope



  //----------------------------------------------------------------------
  // Get R=0.2 truth jets if MC
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Jets) > 0 && !IsCollisions (m_dataType)) {
    if (m_debug >= 1) {
      Info ("GetAntiKt2TruthJets ()", "Retrieving anti-kT R=0.2 truth jets...");
    }

    m_b_akt2_truth_jet_n = 0;

    const xAOD::JetContainer* truthJet2Container = 0;
    if (!evtStore ()->retrieve (truthJet2Container, "AntiKt2TruthJets").isSuccess ()) {
      Error ("GetAntiKt2TruthJets ()", "Failed to retrieve AntiKt2TruthJets container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetAntiKt2TruthJets ()", "Successfully retrieved AntiKt2TruthJets container.");
    }

    for (const auto* truthJet : *truthJet2Container) {
      // Check for array overflow
      if (m_b_akt2_truth_jet_n >= m_max_akt2_truth_jet_n) {
        Error ("GetAntiKt2TruthJets ()", "Tried to overflow anti-kT truth R=0.2 jet arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetAntiKt2TruthJets ()", Form ("Checked anti-kT truth R=0.2 jet array length, found %i jets so far.", m_b_akt2_truth_jet_n));
      }

      // Truth jet kinematic cut
      if (truthJet->pt () * 1e-3 < m_min_truth_akt2_jet_pt) {
        continue;
      }

      // Store truth jet info
      m_b_akt2_truth_jet_pt[m_b_akt2_truth_jet_n] = truthJet->pt () * 1e-3;
      m_b_akt2_truth_jet_eta[m_b_akt2_truth_jet_n] = truthJet->eta ();
      m_b_akt2_truth_jet_phi[m_b_akt2_truth_jet_n] = truthJet->phi ();
      m_b_akt2_truth_jet_e[m_b_akt2_truth_jet_n] = truthJet->e () * 1e-3;
      m_b_akt2_truth_jet_n++;
    } // end R=0.2 truth jet loop
    if (m_debug >= 1) {
      Info ("GetAntiKt2TruthJets ()", "Finished retrieving anti-kT R=0.2 truth jets.");
    }
  } // end R=0.2 truth jet scope



  //----------------------------------------------------------------------
  // Get R=0.4 truth jets if MC
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Jets) > 0 && !IsCollisions (m_dataType)) {
    if (m_debug >= 1) {
      Info ("GetAntiKt4TruthJets ()", "Retrieving anti-kT R=0.4 truth jets...");
    }

    m_b_akt4_truth_jet_n = 0;

    const xAOD::JetContainer* truthJet4Container = 0;
    if (!evtStore ()->retrieve (truthJet4Container, "AntiKt4TruthJets").isSuccess ()) {
      Error ("GetAntiKt4TruthJets ()", "Failed to retrieve AntiKt4TruthJets container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetAntiKt4TruthJets ()", "Successfully retrieved AntiKt4TruthJets container.");
    }

    for (const auto* truthJet : *truthJet4Container) {
      // Check for array overflow
      if (m_b_akt4_truth_jet_n >= m_max_akt4_truth_jet_n) {
        Error ("GetAntiKt4TruthJets ()", "Tried to overflow anti-kT truth R=0.4 jet arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetAntiKt4TruthJets ()", Form ("Checked anti-kT truth R=0.4 jet array length, found %i jets so far.", m_b_akt4_truth_jet_n));
      }

      // Truth jet kinematic cut
      if (truthJet->pt () * 1e-3 < m_min_truth_akt4_jet_pt) {
        continue;
      }

      // Store truth jet info
      m_b_akt4_truth_jet_pt[m_b_akt4_truth_jet_n] = truthJet->pt () * 1e-3;
      m_b_akt4_truth_jet_eta[m_b_akt4_truth_jet_n] = truthJet->eta ();
      m_b_akt4_truth_jet_phi[m_b_akt4_truth_jet_n] = truthJet->phi ();
      m_b_akt4_truth_jet_e[m_b_akt4_truth_jet_n] = truthJet->e () * 1e-3;
      m_b_akt4_truth_jet_n++;
    } // end R=0.4 truth jet loop
    if (m_debug >= 1) {
      Info ("GetAntiKt4TruthJets ()", "Finished retrieving anti-kT R=0.4 truth jets.");
    }
  } // end R=0.4 truth jet scope



  //----------------------------------------------------------------------
  // Get R=0.4 EMTopo jets (in pp collisions)
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Jets) > 0 && Ispp (m_collisionSystem)) {
    if (m_debug >= 1) {
      Info ("GetAntiKt4EMTopoJets ()", "Retrieving anti-kT R=0.4 EMTopo jets...");
    }

    m_b_akt4_emtopo_jet_n = 0;

    const xAOD::JetContainer* jetContainer = 0;
    if (!evtStore ()->retrieve (jetContainer, "AntiKt4EMTopoJets").isSuccess ()) {
      Error ("GetAntiKt4EMTopoJets ()", "Failed to retrieve AntiKt4EMTopoJets container. Exiting." );
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetAntiKt4EMTopoJets ()", "Successfully retrieved AntiKt4EMTopoJets container.");
    }

    for (const auto* jet : *jetContainer) {
      // Check for array overflow
      if (m_b_akt4_emtopo_jet_n >= m_max_akt4_emtopo_jet_n) {
        Info ("GetAntiKt4EMTopoJets ()", "Tried to overflow jets arrays, continuing.");
        continue;
      }
      else if (m_debug >= 3) {
        Info ("GetAntiKt4EMTopoJets ()", Form ("Checked anti-kT EMTopo R=0.4 jet array length, found %i jets so far.", m_b_akt4_emtopo_jet_n));
      }

      // Jet quality check
      const bool isLooseBad = m_jetCleaningTool->keep (*jet); // implements jet cleaning at the "LooseBad" selection level

      // Jet kinematic info
      const xAOD::JetFourMom_t jet_4mom = jet->jetP4 ();
      const float jet_pt = 1e-3 * jet_4mom.pt ();
      const float jet_eta = jet_4mom.eta ();
      const float jet_phi = jet_4mom.phi ();
      const float jet_e = 1e-3 * jet_4mom.e ();

      // Jet kinematic cut
      if (jet_pt < m_min_akt4_jet_pt) {
        continue;
      }

      // Store jet info
      m_b_akt4_emtopo_jet_pt[m_b_akt4_emtopo_jet_n] = jet_pt;
      m_b_akt4_emtopo_jet_eta[m_b_akt4_emtopo_jet_n] = jet_eta;
      m_b_akt4_emtopo_jet_phi[m_b_akt4_emtopo_jet_n] = jet_phi;
      m_b_akt4_emtopo_jet_e[m_b_akt4_emtopo_jet_n] = jet_e;
      m_b_akt4_emtopo_jet_LooseBad[m_b_akt4_emtopo_jet_n] = isLooseBad;
      m_b_akt4_emtopo_jet_n++;
    } // end R=0.4 EMTopo jet loop
    if (m_debug >= 1) {
      Info ("GetAntiKt4EMTopoJets ()", "Finished retrieving anti-kT R=0.4 EMTopo jets.");
    }
  } // end R=0.4 EMTopo jet scope



  //----------------------------------------------------------------------
  // Get R=0.2 HI jets
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Jets) > 0) {
    if (m_debug >= 1) {
      Info ("GetAntiKt2HIJets ()", "Retrieving anti-kT R=0.2 HI jets...");
    }

    m_b_akt2_hi_jet_n = 0;

    // Accessor for jet timing info
    static SG::AuxElement::Accessor <Float_t> accTiming ("Timing");

    const xAOD::JetContainer* jet2Container = 0;
    if (!evtStore ()->retrieve (jet2Container, "AntiKt2HIJets").isSuccess ()) {
      Error ("GetAntiKt2HIJets ()", "Failed to retrieve AntiKt2HIJets container. Exiting." );
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetAntiKt2HIJets ()", "Successfully retrieved AntiKt2HIJets container.");
    }

    for (const auto* init_jet : *jet2Container){
      // Check for array overflow
      if (m_b_akt2_hi_jet_n >= m_max_akt2_hi_jet_n) {
        Error ("GetAntiKt2HIJets ()", "Tried to overflow anti-kT HI R=0.2 jet arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetAntiKt2HIJets ()", Form ("Checked anti-kT HI R=0.2 jet array length, found %i jets so far.", m_b_akt2_hi_jet_n));
      }


      // HI jet subtracted energy
      const xAOD::JetFourMom_t sub_4mom = init_jet->jetP4 ("JetSubtractedScaleMomentum") - init_jet->jetP4 ("JetUnsubtractedScaleMomentum");
      const float sub_et = sub_4mom.Et () * 1e-3;
      const float sub_e = sub_4mom.E () * 1e-3;


      // Jet quality check
      const bool isLooseBad = m_jetCleaningTool->keep (*init_jet); // implements jet cleaning at the "LooseBad" selection level


      // Uncalibrated jet kinematic info
      const xAOD::JetFourMom_t init_p4 = init_jet->jetP4 ("JetSubtractedScaleMomentum");
      const float init_pt = init_p4.Pt () * 1e-3;
      const float init_eta = init_p4.Eta ();
      const float init_e = init_p4.E () * 1e-3;

    
      xAOD::Jet* jet = new xAOD::Jet ();

      jet->makePrivateStore (*init_jet); 
      jet->setJetP4 ("JetConstitScaleMomentum", init_p4);
      jet->setJetP4 ("JetEMScaleMomentum", init_p4);


      // Apply EtaJES calibration
      if (m_Akt2HI_EtaJES_CalibTool->applyCorrection (*jet) != CP::CorrectionCode::Ok || !jet) {
        Error ("GetAntiKt2HIJets ()", "EtaJES correction applied unsuccessfully to jet. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 2) {
        Info ("GetAntiKt2HIJets ()", "EtaJES correction applied to jet successfully.");
      }

      // EtaJES calibrated jet kinematic info
      const xAOD::JetFourMom_t etajes_p4 = jet->jetP4 ("JetEtaJESScaleMomentum");
      const float etajes_pt = etajes_p4.Pt () * 1e-3;
      const float etajes_eta = etajes_p4.Eta ();
      const float etajes_e = etajes_p4.E () * 1e-3;


      // Apply cross-calibration & in situ calibration to data
      if (IsCollisions (m_dataType) || IsDataOverlay (m_dataType)) {
        if (m_Akt2HI_EtaJES_Insitu_CalibTool->applyCorrection (*jet) != CP::CorrectionCode::Ok) {
          Error ("GetAntiKt2HIJets ()", "In situ & eta inter-calibrations applied unsuccessfully to jet. Exiting.");
          return EL::StatusCode::FAILURE;
        }
        else if (m_debug >= 2) {
          Info ("GetAntiKt2HIJets ()", "In situ & eta inter-calibrations applied to jet successfully.");
        }
      }
      else {
        jet->setJetP4 ("JetInsituScaleMomentum", etajes_p4);
        if (m_debug >= 2) {
          Info ("GetAntiKt2HIJets ()", "Setting in situ scale momentum to EtaJES in MC without data overlay.");
        }
      }

      // Cross-calibrated jet kinematic info
      const xAOD::JetFourMom_t xcalib_p4 = jet->jetP4 ("JetInsituScaleMomentum");
      const float xcalib_pt = xcalib_p4.Pt () * 1e-3;
      const float xcalib_eta = xcalib_p4.Eta ();
      const float xcalib_e = xcalib_p4.E () * 1e-3;


      // Additional jet kinematic info
      const float jet_phi = jet->jetP4 ().Phi ();


      // Jet kinematic cut
      if (xcalib_pt < m_min_akt2_jet_pt) {
        if (jet) {
          delete jet;
        }
        continue;
      }


      // Systematic uncertainties
      //if (!IsCollisions (m_dataType)) {
      // JES systematics
      // EMTopo-based uncertainties
      m_b_akt2_hi_jet_pt_sys_JES_ALL[0][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (0, *jet); // JET_EtaIntercalibration_Modelling
      m_b_akt2_hi_jet_pt_sys_JES_ALL[1][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (1, *jet); // JET_EtaIntercalibration_TotalStat
      m_b_akt2_hi_jet_pt_sys_JES_ALL[2][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (2, *jet); // JET_EtaIntercalibration_NonClosure_highE
      m_b_akt2_hi_jet_pt_sys_JES_ALL[3][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (3, *jet); // JET_EtaIntercalibration_NonClosure_negEta
      m_b_akt2_hi_jet_pt_sys_JES_ALL[4][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (4, *jet); // JET_EtaIntercalibration_NonClosure_posEta
      m_b_akt2_hi_jet_pt_sys_JES_ALL[5][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (5, *jet); // JET_PunchThrough_MC16
      m_b_akt2_hi_jet_pt_sys_JES_ALL[6][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (6, *jet); // JET_EffectiveNP_1
      m_b_akt2_hi_jet_pt_sys_JES_ALL[7][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (7, *jet); // JET_EffectiveNP_2
      m_b_akt2_hi_jet_pt_sys_JES_ALL[8][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (8, *jet);
      m_b_akt2_hi_jet_pt_sys_JES_ALL[9][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (9, *jet);
      m_b_akt2_hi_jet_pt_sys_JES_ALL[10][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (10, *jet);
      m_b_akt2_hi_jet_pt_sys_JES_ALL[11][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (11, *jet);
      m_b_akt2_hi_jet_pt_sys_JES_ALL[12][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (12, *jet); // JET_EffectiveNP_7
      m_b_akt2_hi_jet_pt_sys_JES_ALL[13][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (13, *jet); // JET_EffectiveNP_8restTerm
      m_b_akt2_hi_jet_pt_sys_JES_ALL[14][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (14, *jet); // JET_Crosscalibration
      m_b_akt2_hi_jet_pt_sys_JES_ALL[15][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (15, *jet); // JET_RScan_JES
      m_b_akt2_hi_jet_pt_sys_JES_ALL[16][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (16, *jet); // JET_SingleParticle_HighPt
      //m_b_akt2_hi_jet_pt_sys_JES_ALL[17][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (17, *jet); // JET_BJES_Response
      //m_b_akt2_hi_jet_pt_sys_JES_ALL[18][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (18, *jet); // JET_EtaIntercalibration_NonClosure_2018data
      
      // Cross-calibration uncertainty
      m_b_akt2_hi_jet_pt_sys_JES_ALL[17][m_b_akt2_hi_jet_n] = m_h2CC_JES->GetBinContent (m_h2CC_JES->FindBin (xcalib_pt, std::fabs (xcalib_eta)));

      // Flavour uncertainties
      const double fu_eta = (IspPb16 (m_collisionSystem) ? xcalib_eta : std::fabs (xcalib_eta));
      m_b_akt2_hi_jet_pt_sys_JES_ALL[18][m_b_akt2_hi_jet_n] = m_f_res_02->GetBinContent (m_f_res_02->FindBin (xcalib_pt, fu_eta));
      m_b_akt2_hi_jet_pt_sys_JES_ALL[19][m_b_akt2_hi_jet_n] = m_f_fra_02->GetBinContent (m_f_fra_02->FindBin (xcalib_pt, fu_eta));

      // R-scan uncertainty
      m_b_akt2_hi_jet_pt_sys_JES_ALL[20][m_b_akt2_hi_jet_n] = m_RS_JER->Eval (m_pt_mapping->Eval (xcalib_pt));

      
      // JER systematics
      // EMTopo-based uncertainties
      m_b_akt2_hi_jet_pt_sys_JER_ALL[0][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (17, *jet); // JET_JER_DataVsMC_MC16
      m_b_akt2_hi_jet_pt_sys_JER_ALL[1][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (18, *jet); // JET_JER_EffectiveNP_1
      m_b_akt2_hi_jet_pt_sys_JER_ALL[2][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (19, *jet); // JET_JER_EffectiveNP_2
      m_b_akt2_hi_jet_pt_sys_JER_ALL[3][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (20, *jet);
      m_b_akt2_hi_jet_pt_sys_JER_ALL[4][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (21, *jet);
      m_b_akt2_hi_jet_pt_sys_JER_ALL[5][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (22, *jet);
      m_b_akt2_hi_jet_pt_sys_JER_ALL[6][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (23, *jet); // JET_JER_EffectiveNP_6
      m_b_akt2_hi_jet_pt_sys_JER_ALL[7][m_b_akt2_hi_jet_n] = m_Akt2HI_UncertTool->getUncertainty (24, *jet); // JET_JER_EffectiveNP_7restTerm

      // Cross-calibration uncertainty
      const double R04_JER = m_h204_JER->GetBinContent (m_h204_JER->FindBin (xcalib_pt, std::fabs (xcalib_eta)));
      const double CC2_smearing = m_h2CC_JER->GetBinContent (m_h2CC_JER->FindBin (xcalib_pt, std::fabs (xcalib_eta)));
      m_b_akt2_hi_jet_pt_sys_JER_ALL[8][m_b_akt2_hi_jet_n] = m_rnd->Gaus (0., std::sqrt (std::pow (R04_JER+CC2_smearing, 2) - std::pow (R04_JER, 2))); // JER cross calibration

      // R-scan uncertainty
      m_b_akt2_hi_jet_pt_sys_JER_ALL[9][m_b_akt2_hi_jet_n] = m_rnd->Gaus (0., m_RS_JER->Eval (m_pt_mapping->Eval (xcalib_pt))); // JER R-scan
      //}


      // Store jet info
      m_b_akt2_hi_jet_pt_precalib[m_b_akt2_hi_jet_n] = init_pt;
      m_b_akt2_hi_jet_eta_precalib[m_b_akt2_hi_jet_n] = init_eta;
      m_b_akt2_hi_jet_e_precalib[m_b_akt2_hi_jet_n] = init_e;
      m_b_akt2_hi_jet_pt_etajes[m_b_akt2_hi_jet_n] = etajes_pt;
      m_b_akt2_hi_jet_eta_etajes[m_b_akt2_hi_jet_n] = etajes_eta;
      m_b_akt2_hi_jet_e_etajes[m_b_akt2_hi_jet_n] = etajes_e;
      m_b_akt2_hi_jet_pt_xcalib[m_b_akt2_hi_jet_n] = xcalib_pt;
      m_b_akt2_hi_jet_eta_xcalib[m_b_akt2_hi_jet_n] = xcalib_eta;
      m_b_akt2_hi_jet_e_xcalib[m_b_akt2_hi_jet_n] = xcalib_e;
      m_b_akt2_hi_jet_phi[m_b_akt2_hi_jet_n] = jet_phi;
      m_b_akt2_hi_jet_sub_et[m_b_akt2_hi_jet_n] = sub_et;
      m_b_akt2_hi_jet_sub_e[m_b_akt2_hi_jet_n] = sub_e;
      m_b_akt2_hi_jet_LooseBad[m_b_akt2_hi_jet_n] = isLooseBad;
      m_b_akt2_hi_jet_timing[m_b_akt2_hi_jet_n] = accTiming (*jet);
      m_b_akt2_hi_jet_n++;
    
      if (jet) {
        delete jet;
      }
    } // end R=0.2 HI jet loop
    if (m_debug >= 1) {
      Info ("GetAntiKt2HIJets ()", "Finished retrieving anti-kT R=0.2 HI jets.");
    }
  } // end R=0.2 HI jet scope



  //----------------------------------------------------------------------
  // Get R=0.4 HI jets
  //----------------------------------------------------------------------
  if (m_saveObjects.count (SaveObject::Jets) > 0) {
    if (m_debug >= 1) {
      Info ("GetAntiKt4HIJets ()", "Retrieving anti-kT R=0.4 HI jets...");
    }

    m_b_akt4_hi_jet_n = 0;

    // Accessor for jet timing info
    static SG::AuxElement::Accessor <Float_t> accTiming ("Timing");

    const xAOD::JetContainer* jet4Container = 0;
    if (!evtStore ()->retrieve (jet4Container, "AntiKt4HIJets").isSuccess ()) {
      Error ("GetAntiKt4HIJets ()", "Failed to retrieve AntiKt4HIJets container. Exiting." );
      return EL::StatusCode::FAILURE;
    }
    else if (m_debug >= 2) {
      Info ("GetAntiKt4HIJets ()", "Successfully retrieved AntiKt4HIJets container.");
    }

    for (const auto* init_jet : *jet4Container){
      // Check for array overflow
      if (m_b_akt4_hi_jet_n >= m_max_akt4_hi_jet_n) {
        Error ("GetAntiKt4HIJets ()", "Tried to overflow anti-kT HI R=0.4 jet arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 3) {
        Info ("GetAntiKt4HIJets ()", Form ("Checked anti-kT HI R=0.4 jet array length, found %i jets so far.", m_b_akt4_hi_jet_n));
      }


      // HI jet subtracted energy
      const xAOD::JetFourMom_t sub_4mom = init_jet->jetP4 ("JetSubtractedScaleMomentum") - init_jet->jetP4 ("JetUnsubtractedScaleMomentum");
      const float sub_et = sub_4mom.Et () * 1e-3;
      const float sub_e = sub_4mom.E () * 1e-3;


      // Jet quality check
      const bool isLooseBad = m_jetCleaningTool->keep (*init_jet); // implements jet cleaning at the "LooseBad" selection level


      // Uncalibrated jet kinematic info
      const xAOD::JetFourMom_t init_p4 = init_jet->jetP4 ("JetSubtractedScaleMomentum");
      const float init_pt = init_p4.Pt () * 1e-3;
      const float init_eta = init_p4.Eta ();
      const float init_e = init_p4.E () * 1e-3;

    
      xAOD::Jet* jet = new xAOD::Jet ();

      jet->makePrivateStore (*init_jet); 
      jet->setJetP4 ("JetConstitScaleMomentum", init_p4);
      jet->setJetP4 ("JetEMScaleMomentum", init_p4);

      // Apply EtaJES calibration
      if (m_Akt4HI_EtaJES_CalibTool->applyCorrection (*jet) != CP::CorrectionCode::Ok || !jet) {
        Error ("GetAntiKt4HIJets ()", "EtaJES correction applied unsuccessfully to jet. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      else if (m_debug >= 2) {
        Info ("GetAntiKt4HIJets ()", "EtaJES correction applied to jet successfully.");
      }

      // EtaJES calibrated jet kinematic info
      const xAOD::JetFourMom_t etajes_p4 = jet->jetP4 ("JetEtaJESScaleMomentum");
      const float etajes_pt = etajes_p4.Pt () * 1e-3;
      const float etajes_eta = etajes_p4.Eta ();
      const float etajes_e = etajes_p4.E () * 1e-3;


      // Apply cross-calibration & in situ calibration to data
      if (IsCollisions (m_dataType) || IsDataOverlay (m_dataType)) {
        if (m_Akt4HI_EtaJES_Insitu_CalibTool->applyCorrection (*jet) != CP::CorrectionCode::Ok) {
          Error ("GetAntiKt4HIJets ()", "In situ & eta inter-calibrations applied unsuccessfully to jet. Exiting.");
          return EL::StatusCode::FAILURE;
        }
        else if (m_debug >= 2) {
          Info ("GetAntiKt4HIJets ()", "In situ & eta inter-calibrations applied to jet successfully.");
        }
      }
      else {
        jet->setJetP4 ("JetInsituScaleMomentum", etajes_p4);
        if (m_debug >= 2) {
          Info ("GetAntiKt4HIJets ()", "Setting in situ scale momentum to EtaJES in MC without data overlay.");
        }
      }

      // Cross-calibrated jet kinematic info
      const xAOD::JetFourMom_t xcalib_p4 = jet->jetP4 ("JetInsituScaleMomentum");
      const float xcalib_pt = xcalib_p4.Pt () * 1e-3;
      const float xcalib_eta = xcalib_p4.Eta ();
      const float xcalib_e = xcalib_p4.E () * 1e-3;


      // Additional jet kinematic info
      const float jet_phi = jet->jetP4 ().Phi ();


      // Jet kinematic cut
      if (xcalib_pt < m_min_akt4_jet_pt) {
        if (jet) {
          delete jet;
        }
        continue;
      }


      // Systematic uncertainties
      //if (!IsCollisions (m_dataType)) {
      // JES systematics
      // EMTopo-based uncertainties
      m_b_akt4_hi_jet_pt_sys_JES_ALL[0][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (0, *jet); // JET_EtaIntercalibration_Modelling
      m_b_akt4_hi_jet_pt_sys_JES_ALL[1][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (1, *jet); // JET_EtaIntercalibration_TotalStat
      m_b_akt4_hi_jet_pt_sys_JES_ALL[2][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (2, *jet); // JET_EtaIntercalibration_NonClosure_highE
      m_b_akt4_hi_jet_pt_sys_JES_ALL[3][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (3, *jet); // JET_EtaIntercalibration_NonClosure_negEta
      m_b_akt4_hi_jet_pt_sys_JES_ALL[4][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (4, *jet); // JET_EtaIntercalibration_NonClosure_posEta
      m_b_akt4_hi_jet_pt_sys_JES_ALL[5][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (5, *jet); // JET_PunchThrough_MC16
      m_b_akt4_hi_jet_pt_sys_JES_ALL[6][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (6, *jet); // JET_EffectiveNP_1
      m_b_akt4_hi_jet_pt_sys_JES_ALL[7][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (7, *jet); // JET_EffectiveNP_2
      m_b_akt4_hi_jet_pt_sys_JES_ALL[8][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (8, *jet); // 3
      m_b_akt4_hi_jet_pt_sys_JES_ALL[9][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (9, *jet); // 4
      m_b_akt4_hi_jet_pt_sys_JES_ALL[10][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (10, *jet); // 5
      m_b_akt4_hi_jet_pt_sys_JES_ALL[11][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (11, *jet); // 6
      m_b_akt4_hi_jet_pt_sys_JES_ALL[12][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (12, *jet); // JET_EffectiveNP_7
      m_b_akt4_hi_jet_pt_sys_JES_ALL[13][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (13, *jet); // JET_EffectiveNP_8restTerm
      m_b_akt4_hi_jet_pt_sys_JES_ALL[14][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (14, *jet); // JET_Crosscalibration
      m_b_akt4_hi_jet_pt_sys_JES_ALL[15][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (15, *jet); // JET_SingleParticle_HighPt
      m_b_akt4_hi_jet_pt_sys_JES_ALL[16][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (16, *jet); // JET_BJES_Response

      // Cross-calibration uncertainty
      m_b_akt4_hi_jet_pt_sys_JES_ALL[17][m_b_akt4_hi_jet_n] = m_h2CC_JES->GetBinContent (m_h2CC_JES->FindBin (xcalib_pt, std::fabs (xcalib_eta)));

      // Flavour uncertainties
      const double fu_eta = (IspPb16 (m_collisionSystem) ? xcalib_eta : std::fabs (xcalib_eta));
      m_b_akt4_hi_jet_pt_sys_JES_ALL[18][m_b_akt4_hi_jet_n] = m_f_res_04->GetBinContent (m_f_res_04->FindBin (xcalib_pt, fu_eta));
      m_b_akt4_hi_jet_pt_sys_JES_ALL[19][m_b_akt4_hi_jet_n] = m_f_fra_04->GetBinContent (m_f_fra_04->FindBin (xcalib_pt, fu_eta));

      // R-scan uncertainty
      //m_b_akt4_hi_jet_pt_sys_JES_ALL[20][m_b_akt4_hi_jet_n] = m_RS_JER->Eval (m_pt_mapping->Eval (xcalib_pt)); // Why did I put this here??? R-scan does not apply to R=0.4... ignore me
      m_b_akt4_hi_jet_pt_sys_JES_ALL[20][m_b_akt4_hi_jet_n] = 0; // dummy protection -- set to 0

      
      // JER systematics
      // EMTopo-based uncertainties
      m_b_akt4_hi_jet_pt_sys_JER_ALL[0][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (17, *jet); // JET_JER_DataVsMC_MC16
      m_b_akt4_hi_jet_pt_sys_JER_ALL[1][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (18, *jet); // JET_JER_EffectiveNP_1
      m_b_akt4_hi_jet_pt_sys_JER_ALL[2][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (19, *jet); // JET_JER_EffectiveNP_2
      m_b_akt4_hi_jet_pt_sys_JER_ALL[3][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (20, *jet);
      m_b_akt4_hi_jet_pt_sys_JER_ALL[4][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (21, *jet);
      m_b_akt4_hi_jet_pt_sys_JER_ALL[5][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (22, *jet);
      m_b_akt4_hi_jet_pt_sys_JER_ALL[6][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (23, *jet); // JET_JER_EffectiveNP_6
      m_b_akt4_hi_jet_pt_sys_JER_ALL[7][m_b_akt4_hi_jet_n] = m_Akt4HI_UncertTool->getUncertainty (24, *jet); // JET_JER_EffectiveNP_7restTerm

      // Cross-calibration uncertainty
      const double R04_JER = m_h204_JER->GetBinContent (m_h204_JER->FindBin (xcalib_pt, std::fabs (xcalib_eta)));
      const double CC2_smearing = m_h2CC_JER->GetBinContent (m_h2CC_JER->FindBin (xcalib_pt, std::fabs (xcalib_eta)));
      m_b_akt4_hi_jet_pt_sys_JER_ALL[8][m_b_akt4_hi_jet_n] = m_rnd->Gaus (0., std::sqrt (std::pow (R04_JER+CC2_smearing, 2) - std::pow (R04_JER, 2))); // JER cross calibration
      //}


      // Store jet info
      m_b_akt4_hi_jet_pt_precalib[m_b_akt4_hi_jet_n] = init_pt;
      m_b_akt4_hi_jet_eta_precalib[m_b_akt4_hi_jet_n] = init_eta;
      m_b_akt4_hi_jet_e_precalib[m_b_akt4_hi_jet_n] = init_e;
      m_b_akt4_hi_jet_pt_etajes[m_b_akt4_hi_jet_n] = etajes_pt;
      m_b_akt4_hi_jet_eta_etajes[m_b_akt4_hi_jet_n] = etajes_eta;
      m_b_akt4_hi_jet_e_etajes[m_b_akt4_hi_jet_n] = etajes_e;
      m_b_akt4_hi_jet_pt_xcalib[m_b_akt4_hi_jet_n] = xcalib_pt;
      m_b_akt4_hi_jet_eta_xcalib[m_b_akt4_hi_jet_n] = xcalib_eta;
      m_b_akt4_hi_jet_e_xcalib[m_b_akt4_hi_jet_n] = xcalib_e;
      m_b_akt4_hi_jet_phi[m_b_akt4_hi_jet_n] = jet_phi;
      m_b_akt4_hi_jet_sub_et[m_b_akt4_hi_jet_n] = sub_et;
      m_b_akt4_hi_jet_sub_e[m_b_akt4_hi_jet_n] = sub_e;
      m_b_akt4_hi_jet_LooseBad[m_b_akt4_hi_jet_n] = isLooseBad;
      m_b_akt4_hi_jet_timing[m_b_akt4_hi_jet_n] = accTiming (*jet);
      m_b_akt4_hi_jet_n++;
    
      if (jet) {
        delete jet;
      }
    } // end R=0.4 HI jet loop
    if (m_debug >= 1) {
      Info ("GetAntiKt4HIJets ()", "Finished retrieving anti-kT R=0.4 HI jets.");
    }
  } // end R=0.4 HI jet scope


  if (m_debug >= 1) {
    Info ("execute ()", "Reached end of event processing, now filling tree.");
  }

  m_tree->Fill();

  if (m_debug >= 1) {
    Info ("execute ()", "Finished processing event, continuing to the next event.");
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: postExecute () {
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: finalize () {
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  Info ("finalize ()", "Deleting tool handles...");

  // Delete trigger tools
  if (m_triggerSet.count (EventTrigger::MinBias)) {
    delete [] m_b_minbias_trig_decision;
    m_b_minbias_trig_decision = nullptr;
    delete [] m_b_minbias_trig_prescale;
    m_b_minbias_trig_prescale = nullptr;
  }
  if (m_triggerSet.count (EventTrigger::Photon)) {
    delete [] m_b_photon_trig_decision;
    m_b_photon_trig_decision = nullptr;
    delete [] m_b_photon_trig_prescale;
    m_b_photon_trig_prescale = nullptr;
  }
  if (m_triggerSet.count (EventTrigger::Electron)) {
    delete [] m_b_electron_trig_decision;
    m_b_electron_trig_decision = nullptr;
    delete [] m_b_electron_trig_prescale;
    m_b_electron_trig_prescale = nullptr;
  }
  if (m_triggerSet.count (EventTrigger::Muon)) {
    delete [] m_b_muon_trig_decision;
    m_b_muon_trig_decision = nullptr;
    delete [] m_b_muon_trig_prescale;
    m_b_muon_trig_prescale = nullptr;
  }
  if (m_triggerSet.count (EventTrigger::Jet)) {
    delete [] m_b_jet_trig_decision;
    m_b_jet_trig_decision = nullptr;
    delete [] m_b_jet_trig_prescale;
    m_b_jet_trig_prescale = nullptr;
  }
  if (m_triggerSet.count (EventTrigger::ZDC)) {
    delete [] m_b_zdc_L1_trig_decision;
    m_b_zdc_L1_trig_decision = nullptr;
    delete [] m_b_zdc_L1_trig_tbp;
    m_b_zdc_L1_trig_tbp = nullptr;
    delete [] m_b_zdc_L1_trig_tap;
    m_b_zdc_L1_trig_tap = nullptr;
    delete [] m_b_zdc_L1_trig_tav;
    m_b_zdc_L1_trig_tav = nullptr;
    delete [] m_b_zdc_L1_trig_prescale;
    m_b_zdc_L1_trig_prescale = nullptr;
  }
  

  if (m_trigConfigTool) {
    delete m_trigConfigTool;
    m_trigConfigTool = nullptr;
  }
  if (m_trigDecisionTool) {
    delete m_trigDecisionTool;
    m_trigDecisionTool = nullptr;
  }
  if (m_trigMatchingTool) {
    delete m_trigMatchingTool;
    m_trigMatchingTool = nullptr;
  }


  // Delete GRL tools
  if (m_goodRunsList) {
    delete m_goodRunsList;
    m_goodRunsList = nullptr;
  }
  if (m_goodRunsList_ignoreToroid) {
    delete m_goodRunsList_ignoreToroid;
    m_goodRunsList_ignoreToroid = nullptr;
  }


  // Delete ZDC analysis tool
  if (m_zdcAnalysisTool) {
    delete m_zdcAnalysisTool;
    m_zdcAnalysisTool = nullptr;
  }


  // Delete out-of-time pileup histograms
  if (m_oop_hMean) {
    delete m_oop_hMean;
    m_oop_hMean = nullptr;
  }
  if (m_oop_hSigma) {
    delete m_oop_hSigma;
    m_oop_hSigma = nullptr;
  }


  // Delete egamma calibration tool
  if (m_egammaCalibrationAndSmearingTool) {
    delete m_egammaCalibrationAndSmearingTool;
    m_egammaCalibrationAndSmearingTool = nullptr;
  }


  // Delete photon identification tools
  if (m_photonLooseIsEMSelector) {
    delete m_photonLooseIsEMSelector;
    m_photonLooseIsEMSelector = nullptr;
  }
  if (m_photonMediumIsEMSelector) {
    delete m_photonMediumIsEMSelector;
    m_photonMediumIsEMSelector = nullptr;
  }
  if (m_photonTightIsEMSelector) {
    delete m_photonTightIsEMSelector;
    m_photonTightIsEMSelector = nullptr;
  }


  // Delete electron identification tools
  if (m_electronLHTightSelectorTool) {
    delete m_electronLHTightSelectorTool;
    m_electronLHTightSelectorTool = nullptr;
  }
  if (m_electronLHMediumSelectorTool) {
    delete m_electronLHMediumSelectorTool;
    m_electronLHMediumSelectorTool = nullptr;
  }
  if (m_electronLHLooseSelectorTool) {
    delete m_electronLHLooseSelectorTool;
    m_electronLHLooseSelectorTool = nullptr;
  }
  if (m_electronLHMediumHISelectorTool) {
    delete m_electronLHMediumHISelectorTool;
    m_electronLHMediumHISelectorTool = nullptr;
  }
  if (m_electronLHLooseHISelectorTool) {
    delete m_electronLHLooseHISelectorTool;
    m_electronLHLooseHISelectorTool = nullptr;
  }


  // Delete muon calibration tool
  if (m_muonCalibrationAndSmearingTool) {
    delete m_muonCalibrationAndSmearingTool;
    m_muonCalibrationAndSmearingTool = nullptr;
  }


  // Delete muon identifaction tool
  if (m_muonTightSelectorTool) {
    delete m_muonTightSelectorTool;
    m_muonTightSelectorTool = nullptr;
  }
  if (m_muonMediumSelectorTool) {
    delete m_muonMediumSelectorTool;
    m_muonMediumSelectorTool = nullptr;
  }
  if (m_muonLooseSelectorTool) {
    delete m_muonLooseSelectorTool;
    m_muonLooseSelectorTool = nullptr;
  }


  // Delete track selection tools
  if (m_trackSelectionToolLoose) {
    delete m_trackSelectionToolLoose;
    m_trackSelectionToolLoose = nullptr;
  }
  if (m_trackSelectionToolLoosePrimary) {
    delete m_trackSelectionToolLoosePrimary;
    m_trackSelectionToolLoosePrimary = nullptr;
  }
  if (m_trackSelectionToolTightPrimary) {
    delete m_trackSelectionToolTightPrimary;
    m_trackSelectionToolTightPrimary = nullptr;
  }
  if (m_trackSelectionToolHITight) {
    delete m_trackSelectionToolHITight;
    m_trackSelectionToolHITight = nullptr;
  }
  if (m_trackSelectionToolHILoose) {
    delete m_trackSelectionToolHILoose;
    m_trackSelectionToolHILoose = nullptr;
  }


  // Delete MC truth classifier tool
  if (m_mcTruthClassifier) {
    delete m_mcTruthClassifier;
    m_mcTruthClassifier = nullptr;
  }


  // Delete jet cleaning tool
  if (m_jetCleaningTool) {
    delete m_jetCleaningTool;
    m_jetCleaningTool = nullptr;
  }


  // Delete jet calibration tools
  if (m_Akt2HI_EtaJES_CalibTool) {
    delete m_Akt2HI_EtaJES_CalibTool;
    m_Akt2HI_EtaJES_CalibTool = nullptr;
  }
  if (m_Akt2HI_EtaJES_Insitu_CalibTool) {
    delete m_Akt2HI_EtaJES_Insitu_CalibTool;
    m_Akt2HI_EtaJES_Insitu_CalibTool = nullptr;
  }
  if (m_Akt4HI_EtaJES_CalibTool) {
    delete m_Akt4HI_EtaJES_CalibTool;
    m_Akt4HI_EtaJES_CalibTool = nullptr;
  }
  if (m_Akt4HI_EtaJES_Insitu_CalibTool) {
    delete m_Akt4HI_EtaJES_Insitu_CalibTool;
    m_Akt4HI_EtaJES_Insitu_CalibTool = nullptr;
  }

  // Delete jet uncertainty tools
  if (m_Akt2HI_UncertTool) {
    delete m_Akt2HI_UncertTool;
    m_Akt2HI_UncertTool = nullptr;
  }
  if (m_Akt4HI_UncertTool) {
    delete m_Akt4HI_UncertTool;
    m_Akt4HI_UncertTool = nullptr;
  }
  if (m_h204_JER) {
    delete m_h204_JER;
    m_h204_JER = nullptr;
  }
  if (m_h2CC_JES) {
    delete m_h2CC_JES;
    m_h2CC_JES = nullptr;
  }
  if (m_h2CC_JER) {
    delete m_h2CC_JER;
    m_h2CC_JER = nullptr;
  }
  if (m_rnd) {
    delete m_rnd;
    m_rnd = nullptr;
  }
  if (m_f_res_02) {
    delete m_f_res_02;
    m_f_res_02 = nullptr;
  }
  if (m_f_fra_02) {
    delete m_f_fra_02;
    m_f_fra_02 = nullptr;
  }
  if (m_f_res_04) {
    delete m_f_res_04;
    m_f_res_04 = nullptr;
  }
  if (m_f_fra_04) {
    delete m_f_fra_04;
    m_f_fra_04 = nullptr;
  }
  if (m_pt_mapping) {
    delete m_pt_mapping;
    m_pt_mapping = nullptr;
  }
  if (m_RS_JES) {
    delete m_RS_JES;
    m_RS_JES = nullptr;
  }
  if (m_RS_JER) {
    delete m_RS_JER;
    m_RS_JER = nullptr;
  }


  Info ("finalize ()", "Finished deleting tool handles, all done now.");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: histFinalize () {
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}



/////////////////////////////////////////////////////////////////////
// 2018 tracker misalignment
/////////////////////////////////////////////////////////////////////
bool TreeMaker :: isDesynEvent (const int _runNumber, const int _lumiBlock) {
  switch (_runNumber) {
    case 366627: { if (_lumiBlock >= 139  && _lumiBlock <= 250) return true; return false; }
    case 366691: { if (_lumiBlock >= 236  && _lumiBlock <= 360) return true; return false; }
    case 366754: { if (_lumiBlock >= 208  && _lumiBlock <= 340) return true; return false; }
    case 366805: { if (_lumiBlock >= 81   && _lumiBlock <= 200) return true; return false; }
    case 366860: { if (_lumiBlock >= 147  && _lumiBlock <= 210) return true; return false; }
    case 366878: { if (_lumiBlock >= 87   && _lumiBlock <= 160) return true; return false; }
    case 366919: { if (_lumiBlock >= 110  && _lumiBlock <= 200) return true; return false; }
    case 366931: { if (_lumiBlock >= 139  && _lumiBlock <= 200) return true; return false; }
    case 367023: { if (_lumiBlock >= 140  && _lumiBlock <= 220) return true; return false; }
    case 367099: { if (_lumiBlock >= 200  && _lumiBlock <= 260) return true; return false; }
    case 367134: { if (_lumiBlock >= 140  && _lumiBlock <= 170) return true; return false; }
    case 367233: { if (_lumiBlock >= 180  && _lumiBlock <= 250) return true; return false; }
    default: return false;
  }
}



/////////////////////////////////////////////////////////////////////
// 2018 out-of-time pile-up removal
/////////////////////////////////////////////////////////////////////
bool TreeMaker :: is_Outpileup(const xAOD::HIEventShapeContainer& evShCont, const int nTrack) {

  if (nTrack > 3000) { // The selection is only for [0, 3000]
    return 0;
  }
  
  float Fcal_Et = 0.0;
  float Tot_Et = 0.0;
  float oop_Et = 0.0;
  Fcal_Et = evShCont.at(5)->et()*1e-6;
  Tot_Et = evShCont.at(0)->et()*1e-6;
  oop_Et = Tot_Et - Fcal_Et;// Barrel + Endcap calo
  
  int nBin{m_oop_hMean->GetXaxis()->FindFixBin(nTrack)};
  double mean{m_oop_hMean->GetBinContent(nBin)};
  double sigma{m_oop_hSigma->GetBinContent(nBin)};

  ANA_MSG_DEBUG (" oop_Et = " << oop_Et << "TeV"  );
  
  if (m_nside == 1) { // one side cut
    if (oop_Et - mean > -4 * sigma) { // 4 sigma cut
      return 0;
    }
  }

  if (m_nside == 2) { // two side cut
    if (abs(oop_Et - mean) < 4 * sigma) { // 4 sigma cut
      return 0;
    }
  }

  return 1;
}



/////////////////////////////////////////////////////////////////////
// Helper function for gap cuts
// Author: Blair Seidlitz
/////////////////////////////////////////////////////////////////////
bool TreeMaker :: passSinfigCut (float eta, int cellsigsamp, float cellsig) {
  bool Use_cluster = false;
  float sig_cut = CellSigCut (eta);
  //Check if cell sig is above threshold
  if (cellsig > sig_cut) {
    Use_cluster = 1;
  }
  (void) cellsigsamp;
  //Tile cut off!!!!!!!!!
  //Check if significant cell is in tile calorimeter
  //if (cellsigsamp < 21 && cellsigsamp > 11)
  //Use_cluster = 0;
  if (std::fabs (eta) > 4.9) {
    Use_cluster = 0;
  }
  return Use_cluster;
}



/////////////////////////////////////////////////////////////////////
// Helper function for gap cuts
// Author: Blair Seidlitz
/////////////////////////////////////////////////////////////////////
float TreeMaker :: CellSigCut (float x) {
  float eta[101] = {-5, -4.9, -4.8, -4.7, -4.6, -4.5, -4.4, -4.3, -4.2, -4.1, -4, -3.9, -3.8, -3.7, -3.6, -3.5, -3.4, -3.3, -3.2, -3.1, -3, -2.9, -2.8, -2.7, -2.6, -2.5, -2.4, -2.3, -2.2, -2.1, -2.0, -1.9, -1.8, -1.7, -1.6, -1.5, -1.4, -1.3, -1.2, -1.1, -1, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9};
  float sig[101] = {0, 4.7426, 5.11018, 5.07498, 5.0969, 5.10695, 5.04098, 5.07106, 4.98087, 5.11647, 5.08988, 5.16267, 5.17202, 5.23803, 5.25314, 5.29551, 5.35092, 5.40863, 5.44375, 5.38075, 5.25022, 5.37933, 5.25459, 5.37719, 5.25169, 5.73985, 5.79174, 5.79266, 5.79588, 5.7963, 5.81949, 5.82273, 5.85658, 5.85442, 5.84779, 5.77679, 5.83323, 5.84524, 5.84439, 5.84488, 5.84744, 5.84683, 5.84524, 5.84594, 5.84656, 5.84639, 5.84461, 5.84515, 5.84206, 5.8396, 5.84497, 5.84801, 5.84608, 5.84608, 5.84783, 5.84726, 5.84844, 5.8477, 5.84796, 5.84757, 5.84822, 5.84814, 5.84617, 5.83451, 5.77658, 5.84309, 5.85496, 5.85761, 5.82555, 5.82206, 5.78982, 5.78482, 5.7778, 5.78327, 5.74898, 5.25459, 5.37503, 5.25459, 5.37283, 5.25169, 5.37862, 5.44473, 5.41041, 5.34498, 5.29551, 5.25602, 5.2283, 5.17428, 5.14504, 5.09342, 5.12256, 4.98721, 5.07106, 5.02642, 5.10031, 5.11018, 5.05447, 5.10031, 4.7426, 0};
  float sig_cut = 0;
  for (int i = 0; i < 100; i++) {
    if (x < eta[i]) {
      sig_cut = sig[i];
      break;
    }
  }
  return sig_cut;
}



/////////////////////////////////////////////////////////////////////
// Calculate gaps using only clusters
// Author: Blair Seidlitz
/////////////////////////////////////////////////////////////////////
void TreeMaker :: ClusterGapCut (std::vector <float>& clus) {
  //Define a c++ Set for organiZed list eta vals fro gap calc
  //C++ Set automatically organizes inserted vals
  //Intialized with with tracks and then clusters are added.
  std::set<float> eta_vals (clus.begin (), clus.end ());

  //add goast particles to define edge of detector
  eta_vals.insert (4.9);
  eta_vals.insert (-4.9);
  //----------------------------------------------------
  //Edge gap calculations
  // A side eta > 0 
  // C side eta < 0 
  float edgeGap_A = 0;
  float edgeGap_C = 0;

  //Edge gap calc (gap between detector edge and closest particle)
  std::set<float>::iterator neg_itr1 = ++eta_vals.begin ();
  std::set<float>::iterator neg_itr2 = eta_vals.begin ();
  edgeGap_C = *neg_itr1 - *neg_itr2;

  std::set<float>::iterator pos_itr1 = ----eta_vals.end ();
  std::set<float>::iterator pos_itr2 = --eta_vals.end ();
  edgeGap_A = *pos_itr2 - *pos_itr1;

  /*  
  int MedianEta = round(eta_vals.size()/2);
  for (set<float>::iterator itr1 = ++eta_vals.begin(), itr2 = eta_vals.begin(); itr1 != next(eta_vals.begin(),MedianEta); itr1++,itr2++) {
    float delta_eta=*itr1-*itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (delta_eta > eta_cut) sum_gap_a+=delta_eta;
  }

  for ( set<float>::iterator itr1= next(eta_vals.begin(),MedianEta), itr2 = next(eta_vals.begin(),MedianEta-1); itr1 != eta_vals.end(); itr1++,itr2++) {
    float delta_eta=*itr1-*itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (delta_eta > eta_cut) sum_gap_p+=delta_eta;
  }
  */

  //insert goast particle at eta=0.
  //This breaks up the gaps makeing the max gap=4.9
  //This also splits the detector into two separate sides
  //the photon side and the nucleus side
  //this causes a pile up effect in gap quantities at 4.9
  //because this is a synthetic particle.
  eta_vals.insert (0);

  //Sum of gaps to zero rapidity for both sides using gap cut
  float sumGap_A = 0;
  float sumGap_C = 0;
  float eta_cut = 0.5;

  for (std::set<float>::iterator itr1 = ++eta_vals.begin (), itr2 = eta_vals.begin (); itr1!=eta_vals.end (); itr1++, itr2++) {
    float delta_eta = *itr1 - *itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (*itr2 < 0) {
      if (delta_eta > eta_cut) {
        sumGap_C += delta_eta;
      }
    }
    //define the photon side of the detector
    if (*itr1 > 0) {
      if (delta_eta > eta_cut) {
        sumGap_A += delta_eta;
      }
    }
  }

  //Store gap info
  m_b_cluster_sumGap_A  = sumGap_A;
  m_b_cluster_sumGap_C  = sumGap_C;
  m_b_cluster_edgeGap_A = edgeGap_A;
  m_b_cluster_edgeGap_C = edgeGap_C;

  eta_vals.erase (0.0);//erase goast particle
}



/////////////////////////////////////////////////////////////////////
// Calculate gaps using only clusters
// Author: Blair Seidlitz
/////////////////////////////////////////////////////////////////////
void TreeMaker :: GapCut (std::vector <float>& trks, std::vector <float>& clus) {
  //Define a c++ Set for organiZed list eta vals fro gap calc
  //C++ Set automatically organizes inserted vals
  //Intialized with with tracks and then clusters are added.
  std::set<float> eta_vals (clus.begin (), clus.end ());

  //add goast particles to define edge of detector
  eta_vals.insert (4.9);
  eta_vals.insert (-4.9);

  std::vector <float>::iterator itrE = trks.begin ();
  for(; itrE!=trks.end (); itrE++) {
    eta_vals.insert (*itrE);
  }

  //----------------------------------------------------
  //Edge gap calculations
  // A side eta > 0 
  // C side eta < 0 
  float edgeGap_A = 0;
  float edgeGap_C = 0;

  //Edge gap calc (gap between detector edge and closest particle)
  std::set<float>::iterator neg_itr1 = ++eta_vals.begin ();
  std::set<float>::iterator neg_itr2 = eta_vals.begin ();
  edgeGap_C = *neg_itr1 - *neg_itr2;

  std::set<float>::iterator pos_itr1 = ----eta_vals.end ();
  std::set<float>::iterator pos_itr2 = --eta_vals.end ();
  edgeGap_A = *pos_itr2 - *pos_itr1;
  /*  
  int MedianEta = round(eta_vals.size()/2);
  for (set<float>::iterator itr1 = ++eta_vals.begin(), itr2 = eta_vals.begin(); itr1 != next(eta_vals.begin(),MedianEta); itr1++,itr2++) {
    float delta_eta=*itr1-*itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (delta_eta > eta_cut) sum_gap_a+=delta_eta;
  }

  for ( set<float>::iterator itr1= next(eta_vals.begin(),MedianEta), itr2 = next(eta_vals.begin(),MedianEta-1); itr1 != eta_vals.end(); itr1++,itr2++) {
    float delta_eta=*itr1-*itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (delta_eta > eta_cut) sum_gap_p+=delta_eta;
  }
  */

  //insert goast particle at eta=0.
  //This breaks up the gaps makeing the max gap=4.9
  //This also splits the detector into two separate sides
  //the photon side and the nucleus side
  //this causes a pile up effect in gap quantities at 4.9
  //because this is a synthetic particle.
  eta_vals.insert (0);

  //Sum of gaps to zero rapidity for both sides using gap cut
  float sumGap_A = 0;
  float sumGap_C = 0;
  float eta_cut = 0.5;

  for (std::set<float>::iterator itr1=++eta_vals.begin (), itr2=eta_vals.begin (); itr1!=eta_vals.end (); itr1++, itr2++) {
    float delta_eta = *itr1 - *itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (*itr2 < 0) {
      if (delta_eta > eta_cut) {
        sumGap_C += delta_eta;
      }
    }
    //define the photon side of the detector
    if (*itr1 > 0) {
      if (delta_eta > eta_cut) {
        sumGap_A += delta_eta;
      }
    }
  }

  //Store gap info
  m_b_sumGap_A  = sumGap_A;
  m_b_sumGap_C  = sumGap_C;
  m_b_edgeGap_A = edgeGap_A;
  m_b_edgeGap_C = edgeGap_C;

  eta_vals.erase (0.0);//erase goast particle
}




/////////////////////////////////////////////////////////////////////
// Checks the summary value for some value.
/////////////////////////////////////////////////////////////////////
uint8_t TreeMaker :: getSumInt (const xAOD::TrackParticle& trk, xAOD::SummaryType sumType) {
  uint8_t sumVal=0;
  if (!trk.summaryValue (sumVal, sumType)) {
    Error ("getSumInt ()", "Could not get summary type %i", sumType);
  }
  return sumVal;
}

float TreeMaker :: getSumFloat (const xAOD::TrackParticle& trk, xAOD::SummaryType sumType) {
  float sumVal = 0;
  if (!trk.summaryValue (sumVal, sumType)) {
    Error ("getSumFloat ()", "Could not get summary type %i", sumType);
  }
  return sumVal;
}
