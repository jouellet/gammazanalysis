#ifndef HadronYieldsAnalysis_TreeMaker_H
#define HadronYieldsAnalysis_TreeMaker_H

#include <HadronYieldsAnalysis/Useful.h>

#include <TTree.h>
#include <TH1.h>
#include <TRandom3.h>

#include <EventLoop/Algorithm.h>
#include <AsgTools/AnaToolHandle.h>

// GRL tool
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

// Trigger tools
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include "TrigConfxAOD/xAODConfigTool.h"
#include <TrigDecisionTool/TrigDecisionTool.h>
#include "TriggerMatchingTool/MatchingTool.h"

// HI event shape access
#include "xAODHIEvent/HIEventShapeContainer.h"

// ZDC sums access
#include "ZdcAnalysis/ZdcAnalysisTool.h"

// EGamma calibration tool
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
// EGamma identification tools
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"

// Muon tools
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"

// Track selection tool
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

// MC truth classification
#include "MCTruthClassifier/MCTruthClassifier.h"

// Jet cleaning tool
#include "JetSelectorTools/JetCleaningTool.h"

// Jet calibration tool
#include "JetCalibTools/JetCalibrationTool.h"

// Jet uncertainty tool                                                                 
#include "JetUncertainties/JetUncertaintiesTool.h"

// in the header
#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicCode.h"

#include <vector>
#include <string>
#include <set>


class TreeMaker : public EL::Algorithm
{
private:
  const short m_debug = 0;

  const float m_min_truth_photon_pt = 9.;
  const float m_min_photon_pt = 9.;
  const float m_min_truth_electron_pt = 9;
  const float m_min_electron_pt = 9;
  const float m_min_em_cluster_pt = 9;
  const float m_min_truth_muon_pt = 9;
  const float m_min_muon_pt = 9;
  const float m_min_truth_trk_pt = 0.0; // prev: 0.4
  const float m_min_trk_pt = 0.4; // prev: 0.9
  const float m_min_hi_cluster_pt = 2;
  const float m_min_truth_akt4_jet_pt = 0;
  const float m_min_truth_akt2_jet_pt = 0;
  const float m_min_akt4_jet_pt = 0;
  const float m_min_akt2_jet_pt = 0;

  const static int nJESSys = 21;
  const static int nJERSys = 10;

  const std::string m_pp15_grl_name = "data15_5TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_HeavyIonP_All_Good.xml";
  const std::string m_PbPb15_grl_name = "data15_hi.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_HeavyIonP_All_Good.xml";
  const std::string m_pPb16s5TeV_grl_name = "data16_hip5TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_HeavyIonP_All_Good.xml";
  const std::string m_pPb16_grl_name = "data16_hip8TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_HeavyIonP_All_Good.xml";
  const std::string m_pp17_grl_name = "data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml";
  const std::string m_PbPb18_grl_name = "data18_hi.periodAllYear_DetStatus-v106-pro22-14_Unknown_PHYS_HeavyIonP_All_Good.xml";
  const std::string m_PbPb18_grl_ignoreToroid_name = "data18_hi.periodAllYear_DetStatus-v106-pro22-14_Unknown_PHYS_HeavyIonP_All_Good_ignore_TOROIDSTATUS.xml";

  const int m_nside = 1;
  const std::string m_oop_fname = "all_fit.root";
  const std::string filePath = "$UserAnalysis_DIR/data/HadronYieldsAnalysis";

  std::vector<CP::SystematicSet> muonSysList; // list of systematics associated with muons
  std::vector<CP::SystematicSet> electronSysList; // list of systematics associated with electrons
  std::vector<CP::SystematicSet> photonSysList; // list of systematics associated with photons

  TH1D* m_oop_hMean = nullptr; //!
  TH1D* m_oop_hSigma = nullptr; //!

public:
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.

  // configuration variables
  CollisionSystem m_collisionSystem;
  DataType m_dataType;
  std::set<EventTrigger> m_triggerSet; // set of triggers for this tree
  std::set<SaveObject> m_saveObjects; // set of physics objects to save for this tree
  std::string m_outputName;

  TTree* m_tree; //!

  // event info 
  unsigned int m_b_runNum; //!
  unsigned int m_b_lbn; //!
  unsigned int m_b_evtNum; //!
  bool m_b_passesToroid; //! 
  bool m_b_isOOTPU; //!
  bool m_b_BlayerDesyn; //!

  // collision info
  float m_b_actualInteractionsPerCrossing; //!
  float m_b_averageInteractionsPerCrossing; //!

  std::vector<float> m_b_mcEventWeights; //!

  // truth event info
  const static int m_max_truth_event_n = 5;
  int m_b_truth_event_n; //!
  int m_b_nPart1[m_max_truth_event_n]; //!
  int m_b_nPart2[m_max_truth_event_n]; //!
  float m_b_impactParameter[m_max_truth_event_n]; //!
  int m_b_nColl[m_max_truth_event_n]; //!
  float m_b_sigmaInelasticNN[m_max_truth_event_n]; //!
  int m_b_nSpectatorNeutrons[m_max_truth_event_n]; //!
  int m_b_nSpectatorProtons[m_max_truth_event_n]; //!
  float m_b_eccentricity[m_max_truth_event_n]; //!
  float m_b_eventPlaneAngle[m_max_truth_event_n]; //!


  // Minimum bias trigger names
  int m_minbias_trig_n; //!
  const std::string* m_minbias_trig_name; //!

  const static int m_minbias_trig_n_PbPb18 = 3;
  const std::string m_minbias_trig_name_PbPb18 [m_minbias_trig_n_PbPb18] = {
    "HLT_noalg_cc_L1TE600.0ETA49",
    "HLT_mb_sptrk_L1ZDC_A_C_VTE50",
    "HLT_noalg_pc_L1TE50_VTE600.0ETA49"
  }; //!

  const static int m_minbias_trig_n_pp17 = 1;
  const std::string m_minbias_trig_name_pp17 [m_minbias_trig_n_pp17] = {
    "HLT_mb_sptrk"
  };

  const static int m_minbias_trig_n_pPb16 = 2;
  const std::string m_minbias_trig_name_pPb16 [m_minbias_trig_n_pPb16] = {
    "HLT_mb_sptrk_L1MBTS_1",
    "HLT_mb_sptrk"
  };

  const static int m_minbias_trig_n_PbPb15 = 2;
  const std::string m_minbias_trig_name_PbPb15 [m_minbias_trig_n_PbPb15] = {
    "HLT_noalg_mb_L1TE50",
    "HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50"
  }; //!

  const static int m_minbias_trig_n_pp15 = 1;
  const std::string m_minbias_trig_name_pp15 [m_minbias_trig_n_pp15] = {
    "HLT_mb_sptrk"
  };

  const static int m_max_minbias_trig_n = std::max (m_minbias_trig_n_PbPb18, std::max (m_minbias_trig_n_pp17, std::max (m_minbias_trig_n_pPb16, std::max (m_minbias_trig_n_PbPb15, m_minbias_trig_n_pp15))));


  // Electron trigger names
  int m_electron_trig_n; //!
  const std::string* m_electron_trig_name; //!

  const static int m_electron_trig_n_PbPb18 = 1;
  const std::string m_electron_trig_name_PbPb18 [m_electron_trig_n_PbPb18] = {
    "HLT_e15_lhloose_ion_L1EM12"
  }; //!

  const static int m_electron_trig_n_pp17 = 1;
  const std::string m_electron_trig_name_pp17 [m_electron_trig_n_pp17] = {
    "HLT_e15_lhloose_L1EM12"
  }; //!

  const static int m_electron_trig_n_pPb16 = 1;
  const std::string m_electron_trig_name_pPb16 [m_electron_trig_n_pPb16] = {
    "HLT_e15_lhloose_nod0"
  }; //!

  const static int m_electron_trig_n_PbPb15 = 1;
  const std::string m_electron_trig_name_PbPb15 [m_electron_trig_n_PbPb15] = {
    "HLT_e15_loose_ion_L1EM12"
  }; //!

  const static int m_electron_trig_n_pp15 = 1;
  const std::string m_electron_trig_name_pp15 [m_electron_trig_n_pp15] = {
    "HLT_e15_lhloose_L1EM13VH"
  }; //!

  const static int m_max_electron_trig_n = std::max (m_electron_trig_n_PbPb18, std::max (m_electron_trig_n_pp17, std::max (m_electron_trig_n_pPb16, std::max (m_electron_trig_n_PbPb15, m_electron_trig_n_pp15))));


  // Muon trigger names
  int m_muon_trig_n; //!
  const std::string* m_muon_trig_name; //!

  const static int m_muon_trig_n_PbPb18 = 1;
  const std::string m_muon_trig_name_PbPb18 [m_muon_trig_n_PbPb18] = {
    "HLT_mu14"
  }; //!

  const static int m_muon_trig_n_pp17 = 1;
  const std::string m_muon_trig_name_pp17 [m_muon_trig_n_pp17] = {
    "HLT_mu14"
  }; //!

  const static int m_muon_trig_n_pPb16 = 1;
  const std::string m_muon_trig_name_pPb16 [m_muon_trig_n_pPb16] = {
    "HLT_mu15"
  }; //!

  const static int m_muon_trig_n_PbPb15 = 1;
  const std::string m_muon_trig_name_PbPb15 [m_muon_trig_n_PbPb15] = {
    "HLT_mu14"
  }; //!

  const static int m_muon_trig_n_pp15 = 1;
  const std::string m_muon_trig_name_pp15 [m_muon_trig_n_pp15] = {
    "HLT_mu14"
  }; //!

  const static int m_max_muon_trig_n = std::max (m_muon_trig_n_PbPb18, std::max (m_muon_trig_n_pp17, std::max (m_muon_trig_n_pPb16, std::max (m_muon_trig_n_PbPb15, m_muon_trig_n_pp15))));


  // Photon trigger names
  int m_photon_trig_n; //!
  const std::string* m_photon_trig_name; //!

  const static int m_photon_trig_n_PbPb18 = 2;
  const std::string m_photon_trig_name_PbPb18 [m_photon_trig_n_PbPb18] = {
    "HLT_g30_loose_ion",
    "HLT_g50_loose_ion"
  }; //!

  const static int m_photon_trig_n_pp17 = 2;
  const std::string m_photon_trig_name_pp17 [m_photon_trig_n_pp17] = {
    //"HLT_g15_loose_L1EM7",
    //"HLT_g20_loose_L1EM15",
    //"HLT_g25_loose_L1EM15",
    "HLT_g30_loose_L1EM15",
    "HLT_g35_loose_L1EM15"
  }; //!

  const static int m_photon_trig_n_pPb16 = 2;
  const std::string m_photon_trig_name_pPb16 [m_photon_trig_n_pPb16] = {
    //"HLT_g10_loose",
    //"HLT_g15_loose",
    //"HLT_g20_loose",
    //"HLT_g25_loose",
    "HLT_g30_loose",
    "HLT_g35_loose"
  }; //!

  const static int m_photon_trig_n_PbPb15 = 1;
  const std::string m_photon_trig_name_PbPb15 [m_photon_trig_n_PbPb15] = {
    "HLT_g20_loose_ion"
  }; //!

  const static int m_photon_trig_n_pp15 = 1;
  const std::string m_photon_trig_name_pp15 [m_photon_trig_n_pp15] = {
    "HLT_g30_loose"
  }; //!

  const static int m_max_photon_trig_n = std::max (m_photon_trig_n_PbPb18, std::max (m_photon_trig_n_pp17, std::max (m_photon_trig_n_pPb16, m_photon_trig_n_PbPb15)));


  // Jet trigger names
  int m_jet_trig_n; //!
  const std::string* m_jet_trig_name; //!

  const static int m_jet_trig_n_PbPb18 = 1;
  const std::string m_jet_trig_name_PbPb18 [m_jet_trig_n_PbPb18] = {
    "HLT_j100_ion_L1J20"
  }; //!

  const static int m_jet_trig_n_pp17 = 5;
  const std::string m_jet_trig_name_pp17 [m_jet_trig_n_pp17] = {
    //"HLT_j110",
    "HLT_j30_0eta490_L1TE20",
    "HLT_j40_0eta490_L1TE20",
    "HLT_j50_L1J15", // to match 5 TeV p+Pb trigger
    "HLT_j75_L1J20",
    "HLT_j100_L1J20" // unprescaled
  }; //!

  const static int m_jet_trig_n_pPb16s5TeV = 2;
  const std::string m_jet_trig_name_pPb16s5TeV [m_jet_trig_n_pPb16s5TeV] = {
    "HLT_j40_ion_0eta490_L1TE10",
    "HLT_j50_ion_L1J10" // unprescaled
  }; //!

  const static int m_jet_trig_n_pPb16 = 1;
  const std::string m_jet_trig_name_pPb16 [m_jet_trig_n_pPb16] = {
    "HLT_j100_ion_L1J20"
  }; //!

  const static int m_jet_trig_n_Pbp16 = 1;
  const std::string m_jet_trig_name_Pbp16 [m_jet_trig_n_Pbp16] = {
    "HLT_j100_L1J20"
  }; //!

  const static int m_jet_trig_n_PbPb15 = 1;
  const std::string m_jet_trig_name_PbPb15 [m_jet_trig_n_PbPb15] = {
    "HLT_j75_ion_L1TE50"
  }; //!

  const static int m_jet_trig_n_pp15 = 1;
  const std::string m_jet_trig_name_pp15 [m_jet_trig_n_pp15] = {
    "HLT_j75_L1J20"
  }; //!


  // ZDC L1 trigger names
  const static int m_zdc_L1_trig_n = 2;
  const std::string m_zdc_L1_trig_name [m_zdc_L1_trig_n] = {
    "L1_ZDC_A",
    "L1_ZDC_C"
  }; //!


  // Minimum bias trigger info
  bool* m_b_minbias_trig_decision; //!
  float* m_b_minbias_trig_prescale; //!

  // Electron trigger info
  bool* m_b_electron_trig_decision; //!
  float* m_b_electron_trig_prescale; //!

  // Muon trigger info
  bool* m_b_muon_trig_decision; //!
  float* m_b_muon_trig_prescale; //!

  // Photon trigger info
  bool* m_b_photon_trig_decision; //!
  float* m_b_photon_trig_prescale; //!

  // Jet trigger info
  bool* m_b_jet_trig_decision; //!
  float* m_b_jet_trig_prescale; //!

  // ZDC L1 trigger info
  bool* m_b_zdc_L1_trig_decision; //!
  bool* m_b_zdc_L1_trig_tbp; //!
  bool* m_b_zdc_L1_trig_tap; //!
  bool* m_b_zdc_L1_trig_tav; //!
  float* m_b_zdc_L1_trig_prescale; //!

  // vertex info
  const static int m_max_nvert = 40;
  int m_b_nvert; //!
  float m_b_vert_x[m_max_nvert]; //!
  float m_b_vert_y[m_max_nvert]; //!
  float m_b_vert_z[m_max_nvert]; //!
  int m_b_vert_ntrk[m_max_nvert]; //!
  int m_b_vert_type[m_max_nvert]; //!
  float m_b_vert_sumpt2[m_max_nvert]; //!

  // truth vertex info
  const static int m_max_truth_vert_n = 40;
  int m_b_truth_vert_n; //!
  float m_b_truth_vert_x[m_max_truth_vert_n]; //!
  float m_b_truth_vert_y[m_max_truth_vert_n]; //!
  float m_b_truth_vert_z[m_max_truth_vert_n]; //!
  int m_b_truth_vert_ntrk[m_max_truth_vert_n]; //!
  float m_b_truth_vert_sumpt2[m_max_truth_vert_n]; //!

  // FCal Et info
  float m_b_fcalA_et; //!
  float m_b_fcalC_et; //!
  float m_b_fcalA_et_Cos2; //!
  float m_b_fcalC_et_Cos2; //!
  float m_b_fcalA_et_Sin2; //!
  float m_b_fcalC_et_Sin2; //!
  float m_b_fcalA_et_Cos3; //!
  float m_b_fcalC_et_Cos3; //!
  float m_b_fcalA_et_Sin3; //!
  float m_b_fcalC_et_Sin3; //!
  float m_b_fcalA_et_Cos4; //!
  float m_b_fcalC_et_Cos4; //!
  float m_b_fcalA_et_Sin4; //!
  float m_b_fcalC_et_Sin4; //!

  // ZDC energies
  float  m_b_ZdcRawEnergy_A; //!
  float  m_b_ZdcRawEnergy_C; //!
  float  m_b_ZdcCalibEnergy_A; //!
  float  m_b_ZdcCalibEnergy_C; //!

  // Sum of gaps and edge gaps for UPC background
  float m_b_cluster_sumGap_A; //!
  float m_b_cluster_sumGap_C; //!
  float m_b_cluster_edgeGap_A; //!
  float m_b_cluster_edgeGap_C; //!
  float m_b_sumGap_A; //!
  float m_b_sumGap_C; //!
  float m_b_edgeGap_A; //!
  float m_b_edgeGap_C; //!

  // Photon info
  const static int m_max_photon_n = 1000;
  int m_b_photon_n; //!
  float m_b_photon_pt_precalib[m_max_photon_n]; //!
  float m_b_photon_pt[m_max_photon_n]; //!
  float m_b_photon_eta[m_max_photon_n]; //!
  float m_b_photon_etaBE[m_max_photon_n]; //!
  float m_b_photon_phi[m_max_photon_n]; //!
  bool m_b_photon_matched[m_max_photon_trig_n][m_max_photon_n]; //!
  bool m_b_photon_tight[m_max_photon_n]; //!
  bool m_b_photon_medium[m_max_photon_n]; //!
  bool m_b_photon_loose[m_max_photon_n]; //!
  unsigned int m_b_photon_isem[m_max_photon_n]; //!
  int m_b_photon_convFlag[m_max_photon_n]; //!
  float m_b_photon_Rconv[m_max_photon_n]; //!
  float m_b_photon_etcone20[m_max_photon_n]; //!
  float m_b_photon_etcone30[m_max_photon_n]; //!
  float m_b_photon_etcone40[m_max_photon_n]; //!
  float m_b_photon_topoetcone20[m_max_photon_n]; //!
  float m_b_photon_topoetcone30[m_max_photon_n]; //!
  float m_b_photon_topoetcone40[m_max_photon_n]; //!
  float m_b_photon_Rhad1[m_max_photon_n]; //!
  float m_b_photon_Rhad[m_max_photon_n]; //!
  float m_b_photon_e277[m_max_photon_n]; //!
  float m_b_photon_Reta[m_max_photon_n]; //!
  float m_b_photon_Rphi[m_max_photon_n]; //!
  float m_b_photon_weta1[m_max_photon_n]; //!
  float m_b_photon_weta2[m_max_photon_n]; //!
  float m_b_photon_wtots1[m_max_photon_n]; //!
  float m_b_photon_f1[m_max_photon_n]; //!
  float m_b_photon_f3[m_max_photon_n]; //!
  float m_b_photon_fracs1[m_max_photon_n]; //!
  float m_b_photon_DeltaE[m_max_photon_n]; //!
  float m_b_photon_Eratio[m_max_photon_n]; //!
  float m_b_photon_pt_sys[m_max_photon_n]; //!
  float m_b_photon_eta_sys[m_max_photon_n]; //!
  float m_b_photon_phi_sys[m_max_photon_n]; //!

  // Truth photon info
  const static int m_max_truth_photon_n = 1000;
  int m_b_truth_photon_n; //!
  float m_b_truth_photon_pt[m_max_truth_photon_n]; //!
  float m_b_truth_photon_eta[m_max_truth_photon_n]; //!
  float m_b_truth_photon_phi[m_max_truth_photon_n]; //!
  int m_b_truth_photon_barcode[m_max_truth_photon_n]; //!

  // Electrons info 
  const static int m_max_electron_n = 1000;
  int m_b_electron_n; //!
  float m_b_electron_pt_precalib[m_max_electron_n]; //!
  float m_b_electron_pt[m_max_electron_n]; //!
  float m_b_electron_eta[m_max_electron_n]; //!
  float m_b_electron_etaBE[m_max_electron_n]; //!
  float m_b_electron_phi[m_max_electron_n]; //!
  int m_b_electron_charge[m_max_electron_n]; //!
  bool m_b_electron_lhtight[m_max_electron_n]; //!
  bool m_b_electron_lhmedium[m_max_electron_n]; //!
  bool m_b_electron_lhloose[m_max_electron_n]; //!
  bool m_b_electron_lhmedium_hi[m_max_electron_n]; //!
  bool m_b_electron_lhloose_hi[m_max_electron_n]; //!
  bool m_b_electron_matched[m_max_electron_trig_n][m_max_electron_n]; //!
  float m_b_electron_etcone20[m_max_electron_n]; //!
  float m_b_electron_etcone30[m_max_electron_n]; //!
  float m_b_electron_etcone40[m_max_electron_n]; //!
  float m_b_electron_topoetcone20[m_max_electron_n]; //!
  float m_b_electron_topoetcone30[m_max_electron_n]; //!
  float m_b_electron_topoetcone40[m_max_electron_n]; //!
  int m_b_electron_ntrk[m_max_electron_n]; //!
  float m_b_electron_id_track_pt[m_max_electron_n]; //!
  float m_b_electron_id_track_eta[m_max_electron_n]; //!
  float m_b_electron_id_track_phi[m_max_electron_n]; //!
  float m_b_electron_id_track_charge[m_max_electron_n]; //!
  float m_b_electron_id_track_d0[m_max_electron_n]; //!
  float m_b_electron_id_track_d0sig[m_max_electron_n]; //!
  float m_b_electron_id_track_z0[m_max_electron_n]; //!
  float m_b_electron_id_track_z0sig[m_max_electron_n]; //!
  float m_b_electron_id_track_theta[m_max_electron_n]; //!
  float m_b_electron_id_track_vz[m_max_electron_n]; //!
  bool m_b_electron_id_track_Loose[m_max_electron_n]; //!
  bool m_b_electron_id_track_LoosePrimary[m_max_electron_n]; //!
  bool m_b_electron_id_track_TightPrimary[m_max_electron_n]; //!
  bool m_b_electron_id_track_HITight[m_max_electron_n]; //!
  bool m_b_electron_id_track_HILoose[m_max_electron_n]; //!
  float m_b_electron_Rhad1[m_max_electron_n]; //!
  float m_b_electron_Rhad[m_max_electron_n]; //!
  float m_b_electron_e277[m_max_electron_n]; //!
  float m_b_electron_Reta[m_max_electron_n]; //!
  float m_b_electron_Rphi[m_max_electron_n]; //!
  float m_b_electron_weta1[m_max_electron_n]; //!
  float m_b_electron_weta2[m_max_electron_n]; //!
  float m_b_electron_wtots1[m_max_electron_n]; //!
  float m_b_electron_f1[m_max_electron_n]; //!
  float m_b_electron_f3[m_max_electron_n]; //!
  float m_b_electron_fracs1[m_max_electron_n]; //!
  float m_b_electron_DeltaE[m_max_electron_n]; //!
  float m_b_electron_Eratio[m_max_electron_n]; //!
  std::vector <std::vector <float>> m_b_electron_pt_sys; //!
  std::vector <std::vector <float>> m_b_electron_eta_sys; //!
  std::vector <std::vector <float>> m_b_electron_phi_sys; //!

  // Truth electrons info
  const static int m_max_truth_electron_n = 1000;
  int m_b_truth_electron_n; //!
  float m_b_truth_electron_pt[m_max_truth_electron_n]; //!
  float m_b_truth_electron_eta[m_max_truth_electron_n]; //!
  float m_b_truth_electron_phi[m_max_truth_electron_n]; //!
  int m_b_truth_electron_charge[m_max_truth_electron_n]; //!
  int m_b_truth_electron_barcode[m_max_truth_electron_n]; //!

  // EGamma calorimeter cluster info, see https://ucatlas.github.io/RootCoreDocumentation/2.4.28/dc/d4b/CaloCluster__v1_8h_source.html
  const static int m_max_em_cluster_n = 250;
  int m_b_em_cluster_n; //!
  float m_b_em_cluster_pt[m_max_em_cluster_n]; //!
  float m_b_em_cluster_et[m_max_em_cluster_n]; //!
  float m_b_em_cluster_eta[m_max_em_cluster_n]; //!
  float m_b_em_cluster_phi[m_max_em_cluster_n]; //!
  float m_b_em_cluster_energyBE[m_max_em_cluster_n]; //!
  float m_b_em_cluster_etaBE[m_max_em_cluster_n]; //!
  float m_b_em_cluster_phiBE[m_max_em_cluster_n]; //!
  float m_b_em_cluster_calE[m_max_em_cluster_n]; //!
  float m_b_em_cluster_calEta[m_max_em_cluster_n]; //!
  float m_b_em_cluster_calPhi[m_max_em_cluster_n]; //!
  int m_b_em_cluster_size[m_max_em_cluster_n]; //!
  int m_b_em_cluster_status[m_max_em_cluster_n]; //!

  // Truth muons info
  const static int m_max_truth_muon_n = 1000;
  int m_b_truth_muon_n; //!
  float m_b_truth_muon_pt[m_max_truth_muon_n]; //!
  float m_b_truth_muon_eta[m_max_truth_muon_n]; //!
  float m_b_truth_muon_phi[m_max_truth_muon_n]; //!
  int m_b_truth_muon_charge[m_max_truth_muon_n]; //!
  int m_b_truth_muon_barcode[m_max_truth_muon_n]; //!

  // Muons info
  const static int m_max_muon_n = 40;
  int m_b_muon_n; //!
  float m_b_muon_pt_precalib[m_max_muon_n]; //!
  float m_b_muon_pt[m_max_muon_n]; //!
  float m_b_muon_ms_pt_precalib[m_max_muon_n]; //!
  float m_b_muon_ms_pt[m_max_muon_n]; //!
  float m_b_muon_eta[m_max_muon_n]; //!
  float m_b_muon_phi[m_max_muon_n]; //!
  int m_b_muon_charge[m_max_muon_n]; //!
  bool m_b_muon_tight[m_max_muon_n]; //!
  bool m_b_muon_medium[m_max_muon_n]; //!
  bool m_b_muon_loose[m_max_muon_n]; //!
  bool m_b_muon_matched[m_max_muon_trig_n][m_max_muon_n]; //!
  float m_b_muon_etcone20[m_max_muon_n]; //!
  float m_b_muon_etcone30[m_max_muon_n]; //!
  float m_b_muon_etcone40[m_max_muon_n]; //!
  float m_b_muon_topoetcone20[m_max_muon_n]; //!
  float m_b_muon_topoetcone30[m_max_muon_n]; //!
  float m_b_muon_topoetcone40[m_max_muon_n]; //!
  float m_b_muon_id_track_pt[m_max_muon_n]; //!
  float m_b_muon_id_track_eta[m_max_muon_n]; //!
  float m_b_muon_id_track_phi[m_max_muon_n]; //!
  float m_b_muon_id_track_charge[m_max_muon_n]; //!
  float m_b_muon_id_track_d0[m_max_muon_n]; //!
  float m_b_muon_id_track_d0sig[m_max_muon_n]; //!
  float m_b_muon_id_track_z0[m_max_muon_n]; //!
  float m_b_muon_id_track_z0sig[m_max_muon_n]; //!
  float m_b_muon_id_track_theta[m_max_muon_n]; //!
  float m_b_muon_id_track_vz[m_max_muon_n]; //!
  bool m_b_muon_id_track_Loose[m_max_muon_n]; //!
  bool m_b_muon_id_track_LoosePrimary[m_max_muon_n]; //!
  bool m_b_muon_id_track_TightPrimary[m_max_muon_n]; //!
  bool m_b_muon_id_track_HITight[m_max_muon_n]; //!
  bool m_b_muon_id_track_HILoose[m_max_muon_n]; //!
  float m_b_muon_ms_track_pt[m_max_muon_n]; //!
  float m_b_muon_ms_track_eta[m_max_muon_n]; //!
  float m_b_muon_ms_track_phi[m_max_muon_n]; //!
  float m_b_muon_ms_track_charge[m_max_muon_n]; //!
  float m_b_muon_ms_track_d0[m_max_muon_n]; //!
  float m_b_muon_ms_track_d0sig[m_max_muon_n]; //!
  float m_b_muon_ms_track_z0[m_max_muon_n]; //!
  float m_b_muon_ms_track_z0sig[m_max_muon_n]; //!
  float m_b_muon_ms_track_theta[m_max_muon_n]; //!
  float m_b_muon_ms_track_vz[m_max_muon_n]; //!
  std::vector <std::vector <float>> m_b_muon_pt_sys; //!
  std::vector <std::vector <float>> m_b_muon_eta_sys; //!
  std::vector <std::vector <float>> m_b_muon_phi_sys; //!

  // Truth tracks info
  const static int m_max_truth_trk_n = 10000;
  int m_b_truth_trk_n; //!
  float m_b_truth_trk_pt[m_max_truth_trk_n]; //!
  float m_b_truth_trk_eta[m_max_truth_trk_n]; //!
  float m_b_truth_trk_phi[m_max_truth_trk_n]; //!
  float m_b_truth_trk_charge[m_max_truth_trk_n]; //!
  int m_b_truth_trk_pdgid[m_max_truth_trk_n]; //!
  int m_b_truth_trk_barcode[m_max_truth_trk_n]; //!
  bool m_b_truth_trk_isHadron[m_max_truth_trk_n]; //!

  // Tracks info
  const static int m_max_ntrk = 10000;
  int m_b_ntrk; //!
  float m_b_trk_pt[m_max_ntrk]; //!
  float m_b_trk_eta[m_max_ntrk]; //!
  float m_b_trk_phi[m_max_ntrk]; //!
  float m_b_trk_charge[m_max_ntrk]; //!
  bool m_b_trk_Loose[m_max_ntrk]; //!
  bool m_b_trk_LoosePrimary[m_max_ntrk]; //!
  bool m_b_trk_TightPrimary[m_max_ntrk]; //!
  bool m_b_trk_HITight[m_max_ntrk]; //!
  bool m_b_trk_HILoose[m_max_ntrk]; //!
  float m_b_trk_d0[m_max_ntrk]; //!
  float m_b_trk_d0sig[m_max_ntrk]; //!
  float m_b_trk_z0[m_max_ntrk]; //!
  float m_b_trk_z0sig[m_max_ntrk]; //!
  float m_b_trk_theta[m_max_ntrk]; //!
  float m_b_trk_vz[m_max_ntrk]; //!
  char m_b_trk_nBLayerHits[m_max_ntrk]; //!
  char m_b_trk_nBLayerSharedHits[m_max_ntrk]; //!
  char m_b_trk_nPixelHits[m_max_ntrk]; //!
  char m_b_trk_nPixelHoles[m_max_ntrk]; //!
  char m_b_trk_nPixelSharedHits[m_max_ntrk]; //!
  char m_b_trk_nPixelDeadSensors[m_max_ntrk]; //!
  char m_b_trk_nSCTHits[m_max_ntrk]; //!
  char m_b_trk_nSCTHoles[m_max_ntrk]; //!
  char m_b_trk_nSCTDoubleHoles[m_max_ntrk]; //!
  char m_b_trk_nSCTSharedHits[m_max_ntrk]; //!
  char m_b_trk_nSCTDeadSensors[m_max_ntrk]; //!
  float m_b_trk_pixeldEdx[m_max_ntrk]; //!
  float m_b_trk_prob_truth[m_max_ntrk]; //!
  float m_b_trk_truth_pt[m_max_ntrk]; //!
  float m_b_trk_truth_eta[m_max_ntrk]; //!
  float m_b_trk_truth_phi[m_max_ntrk]; //!
  float m_b_trk_truth_charge[m_max_ntrk]; //!
  int m_b_trk_truth_type[m_max_ntrk]; //!
  int m_b_trk_truth_orig[m_max_ntrk]; //!
  int m_b_trk_truth_barcode[m_max_ntrk]; //!
  int m_b_trk_truth_pdgid[m_max_ntrk]; //!
  float m_b_trk_truth_vz[m_max_ntrk]; //!
  int m_b_trk_truth_nIn[m_max_ntrk]; //!
  bool m_b_trk_truth_isHadron[m_max_ntrk]; //!

  // HI calorimeter cluster info, see https://ucatlas.github.io/RootCoreDocumentation/2.4.28/dc/d4b/CaloCluster__v1_8h_source.html
  // These are the primary inputs to the HI jet reconstruction
  const static int m_max_hi_cluster_n = 250;
  int m_b_hi_cluster_n; //!
  float m_b_hi_cluster_pt[m_max_hi_cluster_n]; //!
  float m_b_hi_cluster_et[m_max_hi_cluster_n]; //!
  float m_b_hi_cluster_eta[m_max_hi_cluster_n]; //!
  float m_b_hi_cluster_phi[m_max_hi_cluster_n]; //!
  float m_b_hi_cluster_energyBE[m_max_hi_cluster_n]; //!
  float m_b_hi_cluster_etaBE[m_max_hi_cluster_n]; //!
  float m_b_hi_cluster_phiBE[m_max_hi_cluster_n]; //!
  float m_b_hi_cluster_calE[m_max_hi_cluster_n]; //!
  float m_b_hi_cluster_calEta[m_max_hi_cluster_n]; //!
  float m_b_hi_cluster_calPhi[m_max_hi_cluster_n]; //!
  int m_b_hi_cluster_size[m_max_hi_cluster_n]; //!
  int m_b_hi_cluster_status[m_max_hi_cluster_n]; //!

  // Anti-kT R=0.2 truth Jets info
  const static int m_max_akt2_truth_jet_n = 200;
  int m_b_akt2_truth_jet_n; //!
  float m_b_akt2_truth_jet_pt[m_max_akt2_truth_jet_n]; //!
  float m_b_akt2_truth_jet_eta[m_max_akt2_truth_jet_n]; //!
  float m_b_akt2_truth_jet_phi[m_max_akt2_truth_jet_n]; //!
  float m_b_akt2_truth_jet_e[m_max_akt2_truth_jet_n]; //!

  // Anti-kT R=0.4 truth Jets info
  const static int m_max_akt4_truth_jet_n = 200;
  int m_b_akt4_truth_jet_n; //!
  float m_b_akt4_truth_jet_pt[m_max_akt4_truth_jet_n]; //!
  float m_b_akt4_truth_jet_eta[m_max_akt4_truth_jet_n]; //!
  float m_b_akt4_truth_jet_phi[m_max_akt4_truth_jet_n]; //!
  float m_b_akt4_truth_jet_e[m_max_akt4_truth_jet_n]; //!

  // Anti-kT R=0.2 EMTopo Jets info
  const static int m_max_akt2_emtopo_jet_n = 40;
  int m_b_akt2_emtopo_jet_n; //!
  float m_b_akt2_emtopo_jet_pt[m_max_akt2_emtopo_jet_n]; //!
  float m_b_akt2_emtopo_jet_eta[m_max_akt2_emtopo_jet_n]; //!
  float m_b_akt2_emtopo_jet_phi[m_max_akt2_emtopo_jet_n]; //!
  float m_b_akt2_emtopo_jet_e[m_max_akt2_emtopo_jet_n]; //!
  bool m_b_akt2_emtopo_jet_LooseBad[m_max_akt2_emtopo_jet_n]; //!

  // Anti-kT R=0.4 EMTopo Jets info
  const static int m_max_akt4_emtopo_jet_n = 40;
  int m_b_akt4_emtopo_jet_n; //!
  float m_b_akt4_emtopo_jet_pt[m_max_akt4_emtopo_jet_n]; //!
  float m_b_akt4_emtopo_jet_eta[m_max_akt4_emtopo_jet_n]; //!
  float m_b_akt4_emtopo_jet_phi[m_max_akt4_emtopo_jet_n]; //!
  float m_b_akt4_emtopo_jet_e[m_max_akt4_emtopo_jet_n]; //!
  bool m_b_akt4_emtopo_jet_LooseBad[m_max_akt4_emtopo_jet_n]; //!

  // Anti-kT R=0.2 HI Jets info
  const static int m_max_akt2_hi_jet_n = 40;
  int m_b_akt2_hi_jet_n; //!
  float m_b_akt2_hi_jet_pt_precalib[m_max_akt2_hi_jet_n]; //!
  float m_b_akt2_hi_jet_eta_precalib[m_max_akt2_hi_jet_n]; //!
  float m_b_akt2_hi_jet_e_precalib[m_max_akt2_hi_jet_n]; //!
  float m_b_akt2_hi_jet_pt_etajes[m_max_akt2_hi_jet_n]; //!
  float m_b_akt2_hi_jet_eta_etajes[m_max_akt2_hi_jet_n]; //!
  float m_b_akt2_hi_jet_e_etajes[m_max_akt2_hi_jet_n]; //!
  float m_b_akt2_hi_jet_pt_xcalib[m_max_akt2_hi_jet_n]; //!
  float m_b_akt2_hi_jet_eta_xcalib[m_max_akt2_hi_jet_n]; //!
  float m_b_akt2_hi_jet_e_xcalib[m_max_akt2_hi_jet_n]; //!
  float m_b_akt2_hi_jet_phi[m_max_akt2_hi_jet_n]; //!
  float m_b_akt2_hi_jet_sub_et[m_max_akt2_hi_jet_n]; //!
  float m_b_akt2_hi_jet_sub_e[m_max_akt2_hi_jet_n]; //!
  bool m_b_akt2_hi_jet_LooseBad[m_max_akt2_hi_jet_n]; //!
  float* m_b_akt2_hi_jet_pt_sys_JES_ALL[nJESSys]; //!
  float* m_b_akt2_hi_jet_pt_sys_JER_ALL[nJERSys]; //!
  float m_b_akt2_hi_jet_timing[m_max_akt2_hi_jet_n]; //!

  // Anti-kT R=0.4 HI Jets info
  const static int m_max_akt4_hi_jet_n = 40;
  int m_b_akt4_hi_jet_n; //!
  float m_b_akt4_hi_jet_pt_precalib[m_max_akt4_hi_jet_n]; //!
  float m_b_akt4_hi_jet_eta_precalib[m_max_akt4_hi_jet_n]; //!
  float m_b_akt4_hi_jet_e_precalib[m_max_akt4_hi_jet_n]; //!
  float m_b_akt4_hi_jet_pt_etajes[m_max_akt4_hi_jet_n]; //!
  float m_b_akt4_hi_jet_eta_etajes[m_max_akt4_hi_jet_n]; //!
  float m_b_akt4_hi_jet_e_etajes[m_max_akt4_hi_jet_n]; //!
  float m_b_akt4_hi_jet_pt_xcalib[m_max_akt4_hi_jet_n]; //!
  float m_b_akt4_hi_jet_eta_xcalib[m_max_akt4_hi_jet_n]; //!
  float m_b_akt4_hi_jet_e_xcalib[m_max_akt4_hi_jet_n]; //!
  float m_b_akt4_hi_jet_phi[m_max_akt4_hi_jet_n]; //!
  float m_b_akt4_hi_jet_sub_et[m_max_akt4_hi_jet_n]; //!
  float m_b_akt4_hi_jet_sub_e[m_max_akt4_hi_jet_n]; //!
  bool m_b_akt4_hi_jet_LooseBad[m_max_akt4_hi_jet_n]; //!
  float* m_b_akt4_hi_jet_pt_sys_JES_ALL[nJESSys]; //!
  float* m_b_akt4_hi_jet_pt_sys_JER_ALL[nJERSys]; //!
  float m_b_akt4_hi_jet_timing[m_max_akt4_hi_jet_n]; //!



  // GRL tools
  GoodRunsListSelectionTool* m_goodRunsList; //!
  GoodRunsListSelectionTool* m_goodRunsList_ignoreToroid; //!

  // Zdc Tool
  ZDC::ZdcAnalysisTool* m_zdcAnalysisTool; //!

  // Trigger tools
  Trig::TrigDecisionTool* m_trigDecisionTool; //!
  TrigConf::xAODConfigTool* m_trigConfigTool; //!
  Trig::MatchingTool* m_trigMatchingTool; //!

  // EGamma calibration tool
  CP::EgammaCalibrationAndSmearingTool* m_egammaCalibrationAndSmearingTool; //!

  // Photon identification tools
  AsgPhotonIsEMSelector* m_photonTightIsEMSelector; //!
  AsgPhotonIsEMSelector* m_photonMediumIsEMSelector; //!
  AsgPhotonIsEMSelector* m_photonLooseIsEMSelector; //!

  // Electron identification tools
  AsgElectronLikelihoodTool* m_electronLHTightSelectorTool; //!
  AsgElectronLikelihoodTool* m_electronLHMediumSelectorTool; //!
  AsgElectronLikelihoodTool* m_electronLHLooseSelectorTool; //!
  AsgElectronLikelihoodTool* m_electronLHMediumHISelectorTool; //!
  AsgElectronLikelihoodTool* m_electronLHLooseHISelectorTool; //!

  // Muon calibration tool
  CP::MuonCalibrationAndSmearingTool* m_muonCalibrationAndSmearingTool; //!

  // Muon identification tools
  CP::MuonSelectionTool* m_muonTightSelectorTool; //!
  CP::MuonSelectionTool* m_muonMediumSelectorTool; //!
  CP::MuonSelectionTool* m_muonLooseSelectorTool; //!

  // Track selection tool
  InDet::InDetTrackSelectionTool* m_trackSelectionToolLoose; //!
  InDet::InDetTrackSelectionTool* m_trackSelectionToolLoosePrimary; //!
  InDet::InDetTrackSelectionTool* m_trackSelectionToolTightPrimary; //!
  InDet::InDetTrackSelectionTool* m_trackSelectionToolHITight; //!
  InDet::InDetTrackSelectionTool* m_trackSelectionToolHILoose; //!

  // MC Truth Classifier
  MCTruthClassifier* m_mcTruthClassifier; //!

  // Jet cleaning tool
  JetCleaningTool* m_jetCleaningTool; //!

  // EtaJES calibration tool
  JetCalibrationTool* m_Akt2HI_EtaJES_CalibTool; //!
  JetCalibrationTool* m_Akt4HI_EtaJES_CalibTool; //!
  // EtaJES and In situ + cross calibration tool
  JetCalibrationTool* m_Akt2HI_EtaJES_Insitu_CalibTool; //!
  JetCalibrationTool* m_Akt4HI_EtaJES_Insitu_CalibTool; //!

  // Jet systematic uncertainty tools
  std::string m_configFile_uncer02; //!
  std::string m_configFile_uncer04; //!
  JetUncertaintiesTool* m_Akt2HI_UncertTool; //!
  JetUncertaintiesTool* m_Akt4HI_UncertTool; //!

  // Jet cross calibration                                                              
  std::string m_CC_fname; //!
  TH2D* m_h204_JER; //!
  TH2D* m_h2CC_JES; //!
  TH2D* m_h2CC_JER; //!
  TRandom3* m_rnd; //!

  // Flavour uncertainties
  std::string m_FU_02_fname; //!
  TH2D* m_f_res_02; //!
  TH2D* m_f_fra_02; //!
  std::string m_FU_04_fname; //!
  TH2D* m_f_res_04; //!
  TH2D* m_f_fra_04; //!

  // R-scan uncertainties
  std::string m_RS_fname; //!
  TF1* m_pt_mapping; //!
  TF1* m_RS_JES; //!
  TF1* m_RS_JER; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:

  // this is a standard constructor
  TreeMaker ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  bool isDesynEvent (const int _runNumber, const int _lumiBlock);
  bool is_Outpileup (const xAOD::HIEventShapeContainer& evShCont, const int nTrack);
  bool passSinfigCut (float eta, int cellsigsamp, float cellsig);
  float CellSigCut (float x);
  void ClusterGapCut (std::vector <float>& clus);
  void GapCut (std::vector <float>& trks, std::vector <float>& clus);
  uint8_t getSumInt (const xAOD::TrackParticle& trk, xAOD::SummaryType sumType);
  float getSumFloat (const xAOD::TrackParticle& trk, xAOD::SummaryType sumType);

  // this is needed to distribute the algorithm to the workers
  ClassDef(TreeMaker, 1);
};

#endif
