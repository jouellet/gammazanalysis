#ifndef HadronYieldsAnalysis_Useful_H
#define HadronYieldsAnalysis_Useful_H


#include <string>

#include <EventLoop/Algorithm.h>



enum class CollisionSystem  { pp15, PbPb15, pPb16s5TeV, pPb16, Pbp16, XeXe17, pp17, PbPb18 }; // pPb = period A, Pbp = period B
enum class DataType         { Collisions, MCSignal, MCDataOverlay, MCHijing, MCHijingOverlay };
enum class EventTrigger     { None, MinBias, Photon, Electron, Muon, Jet, ZDC };
enum class SaveObject       { None, Photons, Electrons, Clusters, Muons, Jets, Tracks };


std::string ToString (const CollisionSystem collSys);
std::string ToString (const DataType dType);
std::string ToString (const EventTrigger evtTrig);


bool IsIons (const CollisionSystem collSys);
bool IsPbPb (const CollisionSystem collSys);
bool IsPbPb18 (const CollisionSystem collSys);
bool IsPbPb15 (const CollisionSystem collSys);
bool IsXeXe (const CollisionSystem collSys);
bool IspPb (const CollisionSystem collSys);
bool IspPb16 (const CollisionSystem collSys);
bool Ispp (const CollisionSystem collSys);
bool Ispp15 (const CollisionSystem collSys);
bool Ispp17 (const CollisionSystem collSys);
bool IsPeriodA (const CollisionSystem collSys);
bool Is5TeV (const CollisionSystem collSys);
bool Is8TeV (const CollisionSystem collSys);
bool Is2018 (const CollisionSystem collSys);
bool Is2017 (const CollisionSystem collSys);
bool Is2016 (const CollisionSystem collSys);
bool Is2015 (const CollisionSystem collSys);

bool IsCollisions (const DataType dType);
bool IsDataOverlay (const DataType dType);
bool IsHijing (const DataType dType);

#endif
