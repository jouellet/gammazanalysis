
##############################################################################
#
#  JESProvider Input Settings
#    Complete (un-reduced) set of NPs from winter 2021 HI JES recommendations based on Fall 2018 EMTopo JES.
#
#    Full JER treatment (MC and data smearing) from the Summer 2018 with addtional HI modifications
#    recommendations iteration of the JER, with all NPs retained
#
#    February, 2021
#
#  M. Rybar
# 
##############################################################################

UncertaintyRelease:                 rel21_Winter2021
SupportedJetDefs:                   AntiKt2HI
SupportedMCTypes:                   MC16
UncertaintyRootFile:                HIJetUncertainties/Winter2021/HI_AllComponents.root
#UncertaintyRootFile:                $UserAnalysis_DIR/data/HadronYieldsAnalysis/HIJetUncertainties/HI_AllComponents.root
AnalysisRootFile:                   analysisInputs/UnknownFlavourComp.root

##############################################################################
#
#   Settings for JES Components
#
##############################################################################

JESComponent.1.Name:                EffectiveNP_1
JESComponent.1.Desc:                Effective JES Uncertainty Component 1
JESComponent.1.Type:                Effective
JESComponent.1.Param:               Pt
JESComponent.1.Hists:               EffectiveNP_1

JESComponent.2.Name:                EffectiveNP_2
JESComponent.2.Desc:                Effective JES Uncertainty Component 2
JESComponent.2.Type:                Effective
JESComponent.2.Param:               Pt
JESComponent.2.Hists:               EffectiveNP_2

JESComponent.3.Name:                EffectiveNP_3
JESComponent.3.Desc:                Effective JES Uncertainty Component 3
JESComponent.3.Type:                Effective
JESComponent.3.Param:               Pt
JESComponent.3.Hists:               EffectiveNP_3

JESComponent.4.Name:                EffectiveNP_4
JESComponent.4.Desc:                Effective JES Uncertainty Component 4
JESComponent.4.Type:                Effective
JESComponent.4.Param:               Pt
JESComponent.4.Hists:               EffectiveNP_4

JESComponent.5.Name:                EffectiveNP_5
JESComponent.5.Desc:                Effective JES Uncertainty Component 5
JESComponent.5.Type:                Effective
JESComponent.5.Param:               Pt
JESComponent.5.Hists:               EffectiveNP_5

JESComponent.6.Name:                EffectiveNP_6
JESComponent.6.Desc:                Effective JES Uncertainty Component 6
JESComponent.6.Type:                Effective
JESComponent.6.Param:               Pt
JESComponent.6.Hists:               EffectiveNP_6

JESComponent.7.Name:                EffectiveNP_7
JESComponent.7.Desc:                Effective JES Uncertainty Component 7
JESComponent.7.Type:                Effective
JESComponent.7.Param:               Pt
JESComponent.7.Hists:               EffectiveNP_7

JESComponent.8.Name:                EffectiveNP_8restTerm
JESComponent.8.Desc:                Effective JES Uncertainty Component 8
JESComponent.8.Type:                Effective
JESComponent.8.Param:               Pt
JESComponent.8.Hists:               EffectiveNP_8restTerm

##############################################################################
# 
#   HI cross-calibration component components
#
##############################################################################

JESComponent.9.Name:                Crosscalibration
JESComponent.9.Desc:                EMTopo to HI jet crosscalibration
JESComponent.9.Type:                Other
JESComponent.9.Param:               PtAbsEta
JESComponent.9.Hists:               Crosscalib_JES

JESComponent.10.Name:                RScan_JES
JESComponent.10.Desc:                HI RScan crosscalibration
JESComponent.10.Type:                Other
JESComponent.10.Param:               Pt
JESComponent.10.Hists:               RScan_JES

##############################################################################
# 
#   Special components
#
##############################################################################

# Eta intercalibration combines 2D components with standard 1D components.
# Use explicit groups

# Eta intercalibration: theory uncertainty
JESGroup.1.Name:                    EtaIntercalibration_Modelling
JESGroup.1.Desc:                    Eta intercalibration: MC generator modelling uncertainty
JESGroup.1.Type:                    Modelling
JESGroup.1.Corr:                    Correlated
JESGroup.1.Group:                   1
JESGroup.1.Reducible:               False

JESComponent.99.Name:               EtaIntercalibration_Modelling_orig
JESComponent.99.Desc:               Eta intercalibration: MC generator modelling uncertainty
JESComponent.99.Type:               Modelling
JESComponent.99.Param:              PtEta
JESComponent.99.Hists:              EtaIntercalibration_Modelling
JESComponent.99.Group:              1
JESComponent.99.Reducible:          False

JESComponent.100.Name:               EtaIntercalibration_Modelling_prop
JESComponent.100.Desc:               Eta intercalibration: MC generator modelling uncertainty
JESComponent.100.Type:               Modelling
JESComponent.100.Param:              Pt
JESComponent.100.Hists:              InSituProp_EtaIntercalModelling
JESComponent.100.Group:              1
JESComponent.100.Reducible:          False

# Eta intercalibration: total statistical and method uncertainty
JESGroup.2.Name:                    EtaIntercalibration_TotalStat
JESGroup.2.Desc:                    Eta intercalibration: statistical uncertainty
JESGroup.2.Type:                    Statistical
JESGroup.2.Corr:                    Correlated
JESGroup.2.Group:                   2
JESGroup.2.Reducible:               False

JESComponent.101.Name:               EtaIntercalibration_TotalStat_orig
JESComponent.101.Desc:               Eta intercalibration: statistical uncertainty
JESComponent.101.Type:               Statistical
JESComponent.101.Param:              PtEta
JESComponent.101.Hists:              EtaIntercalibration_TotalStat
JESComponent.101.Group:              2
JESComponent.101.Reducible:          False

JESComponent.102.Name:               EtaIntercalibration_TotalStat_prop
JESComponent.102.Desc:               Eta intercalibration: statistical uncertainty
JESComponent.102.Type:               Statistical
JESComponent.102.Param:              Pt
JESComponent.102.Hists:              InSituProp_EtaIntercalStat
JESComponent.102.Group:              2
JESComponent.102.Reducible:          False

# Eta intercalibration: non-closure at high energy
JESGroup.3.Name:                    EtaIntercalibration_NonClosure_highE
JESGroup.3.Desc:                    Eta intercalibration: non-closure uncertainty, high E
JESGroup.3.Type:                    Statistical
JESGroup.3.Corr:                    Correlated
JESGroup.3.Group:                   3
JESGroup.3.Reducible:               False

JESComponent.103.Name:               EtaIntercalibration_NonClosure_highE_orig
JESComponent.103.Desc:               Eta intercalibration: non-closure uncertainty, high E
JESComponent.103.Type:               Other
JESComponent.103.Param:              PtEta
JESComponent.103.Interp:             False
JESComponent.103.Hists:              EtaIntercalibration_NonClosure_highE
JESComponent.103.Group:              3
JESComponent.103.Reducible:          False

JESComponent.104.Name:               EtaIntercalibration_NonClosure_highE_prop
JESComponent.104.Desc:               Eta intercalibration: non-closure uncertainty, high E
JESComponent.104.Type:               Other
JESComponent.104.Param:              Pt
JESComponent.104.Interp:             False
JESComponent.104.Hists:              InSituProp_EtaIntercalNonClosureHighE
JESComponent.104.Group:              3
JESComponent.104.Reducible:          False

# Eta intercalibration: non-closure at negative eta
JESGroup.4.Name:                    EtaIntercalibration_NonClosure_negEta
JESGroup.4.Desc:                    Eta intercalibration: non-closure uncertainty, negative eta
JESGroup.4.Type:                    Statistical
JESGroup.4.Corr:                    Correlated
JESGroup.4.Group:                   4
JESGroup.4.Reducible:               False

JESComponent.105.Name:               EtaIntercalibration_NonClosure_negEta_orig
JESComponent.105.Desc:               Eta intercalibration: non-closure uncertainty, negative eta
JESComponent.105.Type:               Other
JESComponent.105.Param:              PtEta
JESComponent.105.Interp:             False
JESComponent.105.Hists:              EtaIntercalibration_NonClosure_negEta
JESComponent.105.Group:              4
JESComponent.105.Reducible:          False

JESComponent.106.Name:               EtaIntercalibration_NonClosure_negEta_prop
JESComponent.106.Desc:               Eta intercalibration: non-closure uncertainty, negative eta
JESComponent.106.Type:               Other
JESComponent.106.Param:              Pt
JESComponent.106.Interp:             False
JESComponent.106.Hists:              InSituProp_EtaIntercalNonClosureNegEta
JESComponent.106.Group:              4
JESComponent.106.Reducible:          False

# Eta intercalibration: non-closure at positive eta
JESGroup.5.Name:                    EtaIntercalibration_NonClosure_posEta
JESGroup.5.Desc:                    Eta intercalibration: non-closure uncertainty, positive eta
JESGroup.5.Type:                    Statistical
JESGroup.5.Corr:                    Correlated
JESGroup.5.Group:                   5
JESGroup.5.Reducible:               False

JESComponent.107.Name:               EtaIntercalibration_NonClosure_posEta_orig
JESComponent.107.Desc:               Eta intercalibration: non-closure uncertainty, positive eta
JESComponent.107.Type:               Other
JESComponent.107.Param:              PtEta
JESComponent.107.Interp:             False
JESComponent.107.Hists:              EtaIntercalibration_NonClosure_posEta
JESComponent.107.Group:              5
JESComponent.107.Reducible:          False

JESComponent.108.Name:               EtaIntercalibration_NonClosure_posEta_prop
JESComponent.108.Desc:               Eta intercalibration: non-closure uncertainty, positive eta
JESComponent.108.Type:               Other
JESComponent.108.Param:              Pt
JESComponent.108.Interp:             False
JESComponent.108.Hists:              InSituProp_EtaIntercalNonClosurePosEta
JESComponent.108.Group:              5
JESComponent.108.Reducible:          False

# High pT term
JESComponent.109.Name:               SingleParticle_HighPt
JESComponent.109.Desc:               High pT term (r20.7 version)
JESComponent.109.Type:               Detector
JESComponent.109.Param:              Pt
JESComponent.109.Reducible:          False

# bJES uncertainty
JESComponent.123.Name:               BJES_Response
JESComponent.123.Desc:               JES uncertainty for b jets
JESComponent.123.Type:               Modelling
JESComponent.123.Param:              PtEta
JESComponent.123.Special:            True
JESComponent.123.Hists:              bJES
JESComponent.123.Reducible:          False

# Make one more explicit group for the punchthrough
JESGroup.12.Name:                    PunchThrough_MCTYPE
JESGroup.12.Desc:                    Punch-through correction uncertainty
JESGroup.12.Type:                    Detector
JESGroup.12.Corr:                    Correlated
JESGroup.12.Group:                   12
JESGroup.12.Reducible:               False

# Original punch-through uncertainty
JESComponent.124.Name:               PunchThrough_MCTYPE_orig
JESComponent.124.Desc:               Punch-through correction uncertainty, orig
JESComponent.124.Type:               Detector
JESComponent.124.Param:              PtAbsEta
JESComponent.124.Interp:             False
JESComponent.124.Special:            True
JESComponent.124.Hists:              PunchThrough_MCTYPE
JESComponent.124.Group               12
JESComponent.124.Reducible:          False

# Propagated punch-through uncertainty
JESComponent.125.Name:               PunchThrough_MCTYPE_prop
JESComponent.125.Desc:               Punch-through correction uncertainty, prop
JESComponent.125.Type:               Detector
JESComponent.125.Param:              Pt
JESComponent.125.Hists:              InSituProp_PunchThroughMC
JESComponent.125.Group               12
JESComponent.125.Reducible:          False

# 2018 eta-intercalibration non-closure
JESComponent.126.Name:               EtaIntercalibration_NonClosure_2018data
JESComponent.126.Desc:               Eta intercalibration: non-closure uncertainty, 2018 difference
JESComponent.126.Type:               Other
JESComponent.126.Param:              PtEta
JESComponent.126.Reducible:          False

##############################################################################
# 
#   Settings for JER Components
#
##############################################################################

# Resolution control
ResolutionSmearOnlyMC:      True

# Nominal resolutions
NominalFourVecResData:      JER_Nominal_data,PtAbsEta,OnlyX
NominalFourVecResMC:        JER_Nominal_MCTYPE,PtAbsEta,OnlyX

# Resolution uncertainties
JESComponent.201.Name:      JER_DataVsMC_MCTYPE
JESComponent.201.Desc:      NP covering when JER(data) < JER(MC)
JESComponent.201.Type:      Other
JESComponent.201.Param:     PtAbsEta
JESComponent.201.Scale:     FourVecResAbs
JESComponent.201.Interp:    OnlyX

JESComponent.202.Name:      JER_EffectiveNP_1
JESComponent.202.Desc:      Effective JER Uncertainty Component 1
JESComponent.202.Type:      Effective
JESComponent.202.Param:     PtAbsEta
JESComponent.202.Scale:     FourVecResAbs
JESComponent.202.Interp:    OnlyX

JESComponent.203.Name:      JER_EffectiveNP_2
JESComponent.203.Desc:      Effective JER Uncertainty Component 2
JESComponent.203.Type:      Effective
JESComponent.203.Param:     PtAbsEta
JESComponent.203.Scale:     FourVecResAbs
JESComponent.203.Interp:    OnlyX

JESComponent.204.Name:      JER_EffectiveNP_3
JESComponent.204.Desc:      Effective JER Uncertainty Component 3
JESComponent.204.Type:      Effective
JESComponent.204.Param:     PtAbsEta
JESComponent.204.Scale:     FourVecResAbs
JESComponent.204.Interp:    OnlyX

JESComponent.205.Name:      JER_EffectiveNP_4
JESComponent.205.Desc:      Effective JER Uncertainty Component 4
JESComponent.205.Type:      Effective
JESComponent.205.Param:     PtAbsEta
JESComponent.205.Scale:     FourVecResAbs
JESComponent.205.Interp:    OnlyX

JESComponent.206.Name:      JER_EffectiveNP_5
JESComponent.206.Desc:      Effective JER Uncertainty Component 5
JESComponent.206.Type:      Effective
JESComponent.206.Param:     PtAbsEta
JESComponent.206.Scale:     FourVecResAbs
JESComponent.206.Interp:    OnlyX

JESComponent.207.Name:      JER_EffectiveNP_6
JESComponent.207.Desc:      Effective JER Uncertainty Component 6
JESComponent.207.Type:      Effective
JESComponent.207.Param:     PtAbsEta
JESComponent.207.Scale:     FourVecResAbs
JESComponent.207.Interp:    OnlyX

JESComponent.208.Name:      JER_EffectiveNP_7restTerm
JESComponent.208.Desc:      Effective JER Uncertainty Component 7
JESComponent.208.Type:      Effective
JESComponent.208.Param:     PtAbsEta
JESComponent.208.Scale:     FourVecResAbs
JESComponent.208.Interp:    OnlyX

JESComponent.209.Name:      JER_Crosscalib
JESComponent.209.Desc:      EMTopo to HI jet crosscalibration
JESComponent.209.Type:      Effective
JESComponent.209.Param:     PtAbsEta
JESComponent.209.Scale:     FourVecResAbs
JESComponent.209.Interp:    OnlyX

JESComponent.210.Name:      JER_RScan
JESComponent.210.Desc:      RScan Crosscalibration
JESComponent.210.Type:      Effective
JESComponent.210.Param:     PtAbsEta
JESComponent.210.Scale:     FourVecResAbs
JESComponent.210.Interp:    OnlyX

##############################################################################

