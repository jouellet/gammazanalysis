#! /bin/bash

# Script that submits jobs from a .dat file
# $1: dat file

for job in $(seq 0 $((`wc -l $1 | awk '{print $1}'`-5))); do

  line=`sed "$((job+6))q;d" $1`

  # the following skips empty lines and lines that start with '#'
  if [[ "${line}" == \#* ]]; then continue; fi
  if [[ `echo "${line}" | wc -w | awk '{print $1}'` -eq 0 ]]; then continue; fi

  log=`echo "${line}" | awk '{print $8}'` # gets the line with the job description then gets the .log file at the end
  err=`echo "${line}" | awk '{print $9}'` # gets the line with the job description then gets the .err file at the end

  (set -x; root -l -b -q '$ROOTCOREDIR/scripts/load_packages.C' '../source/SubmitGridJob.cxx("'${1}'", '${job}')' > logs/${log} 2> errors/${err} &)
  

  # wait for every 8 jobs you start submitting to avoid cpu overload
  if [[ $((${job} % 8)) -eq 0 ]]; then wait; fi

done
